# 1 "mcc_generated_files/TCPIPLibrary/mac_address.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/opt/microchip/xc8/v2.20/pic/include/language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "mcc_generated_files/TCPIPLibrary/mac_address.c" 2
# 39 "mcc_generated_files/TCPIPLibrary/mac_address.c"
# 1 "mcc_generated_files/TCPIPLibrary/tcpip_config.h" 1
# 39 "mcc_generated_files/TCPIPLibrary/mac_address.c" 2

# 1 "mcc_generated_files/TCPIPLibrary/mac_address.h" 1
# 43 "mcc_generated_files/TCPIPLibrary/mac_address.h"
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 1 3



# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/musl_xc8.h" 1 3
# 5 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3
# 22 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 127 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long uintptr_t;
# 142 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long intptr_t;
# 158 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef signed char int8_t;




typedef short int16_t;




typedef __int24 int24_t;




typedef long int32_t;





typedef long long int64_t;
# 188 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long long intmax_t;





typedef unsigned char uint8_t;




typedef unsigned short uint16_t;




typedef __uint24 uint24_t;




typedef unsigned long uint32_t;





typedef unsigned long long uint64_t;
# 229 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long long uintmax_t;
# 23 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3

typedef int8_t int_fast8_t;

typedef int64_t int_fast64_t;


typedef int8_t int_least8_t;
typedef int16_t int_least16_t;

typedef int24_t int_least24_t;

typedef int32_t int_least32_t;

typedef int64_t int_least64_t;


typedef uint8_t uint_fast8_t;

typedef uint64_t uint_fast64_t;


typedef uint8_t uint_least8_t;
typedef uint16_t uint_least16_t;

typedef uint24_t uint_least24_t;

typedef uint32_t uint_least32_t;

typedef uint64_t uint_least64_t;
# 139 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/stdint.h" 1 3
typedef int32_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef uint32_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
# 140 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3
# 43 "mcc_generated_files/TCPIPLibrary/mac_address.h" 2








typedef union
{
    uint8_t mac_array[6];
    struct { uint8_t byte1,byte2,byte3,byte4,byte5,byte6; } s;
} mac48Address_t;

extern const mac48Address_t broadcastMAC;
extern mac48Address_t macAddress;
extern mac48Address_t hostMacAddress;

const mac48Address_t *MAC_getAddress(void);
# 40 "mcc_generated_files/TCPIPLibrary/mac_address.c" 2


mac48Address_t macAddress = {0x02,0x00,0x00,0x00,0x00,0x01};
const mac48Address_t broadcastMAC = {0xff,0xff,0xff,0xff,0xff,0xff};


const mac48Address_t *MAC_getAddress(void)
{
    return &macAddress;
}
