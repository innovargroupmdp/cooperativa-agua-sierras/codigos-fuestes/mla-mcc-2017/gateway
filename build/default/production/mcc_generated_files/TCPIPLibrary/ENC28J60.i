# 1 "mcc_generated_files/TCPIPLibrary/ENC28J60.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/opt/microchip/xc8/v2.20/pic/include/language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "mcc_generated_files/TCPIPLibrary/ENC28J60.c" 2
# 42 "mcc_generated_files/TCPIPLibrary/ENC28J60.c"
# 1 "/opt/microchip/xc8/v2.20/pic/include/xc.h" 1 3
# 18 "/opt/microchip/xc8/v2.20/pic/include/xc.h" 3
extern const char __xc8_OPTIM_SPEED;

extern double __fpnormalize(double);



# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/xc8debug.h" 1 3



# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/stdlib.h" 1 3



# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/musl_xc8.h" 1 3
# 5 "/opt/microchip/xc8/v2.20/pic/include/c99/stdlib.h" 2 3





# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/features.h" 1 3
# 11 "/opt/microchip/xc8/v2.20/pic/include/c99/stdlib.h" 2 3
# 21 "/opt/microchip/xc8/v2.20/pic/include/c99/stdlib.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 18 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long int wchar_t;
# 122 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned size_t;
# 168 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef __int24 int24_t;
# 204 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef __uint24 uint24_t;
# 22 "/opt/microchip/xc8/v2.20/pic/include/c99/stdlib.h" 2 3

int atoi (const char *);
long atol (const char *);
long long atoll (const char *);
double atof (const char *);

float strtof (const char *restrict, char **restrict);
double strtod (const char *restrict, char **restrict);
long double strtold (const char *restrict, char **restrict);



long strtol (const char *restrict, char **restrict, int);
unsigned long strtoul (const char *restrict, char **restrict, int);
long long strtoll (const char *restrict, char **restrict, int);
unsigned long long strtoull (const char *restrict, char **restrict, int);

int rand (void);
void srand (unsigned);

          void abort (void);
int atexit (void (*) (void));
          void exit (int);
          void _Exit (int);

void *bsearch (const void *, const void *, size_t, size_t, int (*)(const void *, const void *));

__attribute__((nonreentrant)) void qsort (void *, size_t, size_t, int (*)(const void *, const void *));

int abs (int);
long labs (long);
long long llabs (long long);

typedef struct { int quot, rem; } div_t;
typedef struct { long quot, rem; } ldiv_t;
typedef struct { long long quot, rem; } lldiv_t;

div_t div (int, int);
ldiv_t ldiv (long, long);
lldiv_t lldiv (long long, long long);

typedef struct { unsigned int quot, rem; } udiv_t;
typedef struct { unsigned long quot, rem; } uldiv_t;
udiv_t udiv (unsigned int, unsigned int);
uldiv_t uldiv (unsigned long, unsigned long);





size_t __ctype_get_mb_cur_max(void);
# 5 "/opt/microchip/xc8/v2.20/pic/include/c99/xc8debug.h" 2 3







#pragma intrinsic(__builtin_software_breakpoint)
extern void __builtin_software_breakpoint(void);
# 24 "/opt/microchip/xc8/v2.20/pic/include/xc.h" 2 3








# 1 "/opt/microchip/xc8/v2.20/pic/include/pic18.h" 1 3




# 1 "/opt/microchip/xc8/v2.20/pic/include/htc.h" 1 3



# 1 "/opt/microchip/xc8/v2.20/pic/include/xc.h" 1 3
# 5 "/opt/microchip/xc8/v2.20/pic/include/htc.h" 2 3
# 6 "/opt/microchip/xc8/v2.20/pic/include/pic18.h" 2 3


# 1 "/opt/microchip/xc8/v2.20/pic/include/pic18_chip_select.h" 1 3
# 424 "/opt/microchip/xc8/v2.20/pic/include/pic18_chip_select.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 1 3
# 44 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/__at.h" 1 3
# 45 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 2 3







extern volatile unsigned char SSPMSK __attribute__((address(0xF77)));

__asm("SSPMSK equ 0F77h");


typedef union {
    struct {
        unsigned MSK0 :1;
        unsigned MSK1 :1;
        unsigned MSK2 :1;
        unsigned MSK3 :1;
        unsigned MSK4 :1;
        unsigned MSK5 :1;
        unsigned MSK6 :1;
        unsigned MSK7 :1;
    };
    struct {
        unsigned MSK :8;
    };
} SSPMSKbits_t;
extern volatile SSPMSKbits_t SSPMSKbits __attribute__((address(0xF77)));
# 122 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char SLRCON __attribute__((address(0xF78)));

__asm("SLRCON equ 0F78h");


typedef union {
    struct {
        unsigned SLRA :1;
        unsigned SLRB :1;
        unsigned SLRC :1;
    };
} SLRCONbits_t;
extern volatile SLRCONbits_t SLRCONbits __attribute__((address(0xF78)));
# 154 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char CM2CON1 __attribute__((address(0xF79)));

__asm("CM2CON1 equ 0F79h");


typedef union {
    struct {
        unsigned :4;
        unsigned C2RSEL :1;
        unsigned C1RSEL :1;
        unsigned MC2OUT :1;
        unsigned MC1OUT :1;
    };
} CM2CON1bits_t;
extern volatile CM2CON1bits_t CM2CON1bits __attribute__((address(0xF79)));
# 193 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char CM2CON0 __attribute__((address(0xF7A)));

__asm("CM2CON0 equ 0F7Ah");


typedef union {
    struct {
        unsigned C2CH :2;
        unsigned C2R :1;
        unsigned C2SP :1;
        unsigned C2POL :1;
        unsigned C2OE :1;
        unsigned C2OUT :1;
        unsigned C2ON :1;
    };
    struct {
        unsigned C2CH0 :1;
        unsigned C2CH1 :1;
    };
} CM2CON0bits_t;
extern volatile CM2CON0bits_t CM2CON0bits __attribute__((address(0xF7A)));
# 263 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char CM1CON0 __attribute__((address(0xF7B)));

__asm("CM1CON0 equ 0F7Bh");


typedef union {
    struct {
        unsigned C1CH :2;
        unsigned C1R :1;
        unsigned C1SP :1;
        unsigned C1POL :1;
        unsigned C1OE :1;
        unsigned C1OUT :1;
        unsigned C1ON :1;
    };
    struct {
        unsigned C1CH0 :1;
        unsigned C1CH1 :1;
    };
} CM1CON0bits_t;
extern volatile CM1CON0bits_t CM1CON0bits __attribute__((address(0xF7B)));
# 333 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char WPUB __attribute__((address(0xF7C)));

__asm("WPUB equ 0F7Ch");


typedef union {
    struct {
        unsigned WPUB0 :1;
        unsigned WPUB1 :1;
        unsigned WPUB2 :1;
        unsigned WPUB3 :1;
        unsigned WPUB4 :1;
        unsigned WPUB5 :1;
        unsigned WPUB6 :1;
        unsigned WPUB7 :1;
    };
} WPUBbits_t;
extern volatile WPUBbits_t WPUBbits __attribute__((address(0xF7C)));
# 395 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char IOCB __attribute__((address(0xF7D)));

__asm("IOCB equ 0F7Dh");


typedef union {
    struct {
        unsigned :4;
        unsigned IOCB4 :1;
        unsigned IOCB5 :1;
        unsigned IOCB6 :1;
        unsigned IOCB7 :1;
    };
} IOCBbits_t;
extern volatile IOCBbits_t IOCBbits __attribute__((address(0xF7D)));
# 434 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char ANSEL __attribute__((address(0xF7E)));

__asm("ANSEL equ 0F7Eh");


typedef union {
    struct {
        unsigned ANS0 :1;
        unsigned ANS1 :1;
        unsigned ANS2 :1;
        unsigned ANS3 :1;
        unsigned ANS4 :1;
    };
} ANSELbits_t;
extern volatile ANSELbits_t ANSELbits __attribute__((address(0xF7E)));
# 478 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char ANSELH __attribute__((address(0xF7F)));

__asm("ANSELH equ 0F7Fh");


typedef union {
    struct {
        unsigned ANS8 :1;
        unsigned ANS9 :1;
        unsigned ANS10 :1;
        unsigned ANS11 :1;
        unsigned ANS12 :1;
    };
} ANSELHbits_t;
extern volatile ANSELHbits_t ANSELHbits __attribute__((address(0xF7F)));
# 522 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PORTA __attribute__((address(0xF80)));

__asm("PORTA equ 0F80h");


typedef union {
    struct {
        unsigned RA0 :1;
        unsigned RA1 :1;
        unsigned RA2 :1;
        unsigned RA3 :1;
        unsigned RA4 :1;
        unsigned RA5 :1;
        unsigned RA6 :1;
        unsigned RA7 :1;
    };
    struct {
        unsigned AN0 :1;
        unsigned AN1 :1;
        unsigned AN2 :1;
        unsigned AN3 :1;
        unsigned :1;
        unsigned AN4 :1;
    };
    struct {
        unsigned C12IN0M :1;
        unsigned C12IN1M :1;
        unsigned C2INP :1;
        unsigned C1INP :1;
        unsigned C1OUT :1;
        unsigned C2OUT :1;
    };
    struct {
        unsigned C12IN0N :1;
        unsigned C12IN1N :1;
        unsigned VREFM :1;
        unsigned VREFP :1;
        unsigned T0CKI :1;
        unsigned SS :1;
    };
    struct {
        unsigned :5;
        unsigned NOT_SS :1;
    };
    struct {
        unsigned :2;
        unsigned VREFN :1;
        unsigned :2;
        unsigned nSS :1;
    };
    struct {
        unsigned :2;
        unsigned CVREF :1;
        unsigned :2;
        unsigned LVDIN :1;
    };
    struct {
        unsigned :5;
        unsigned HLVDIN :1;
    };
    struct {
        unsigned ULPWUIN :1;
        unsigned :6;
        unsigned RJPU :1;
    };
} PORTAbits_t;
extern volatile PORTAbits_t PORTAbits __attribute__((address(0xF80)));
# 758 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PORTB __attribute__((address(0xF81)));

__asm("PORTB equ 0F81h");


typedef union {
    struct {
        unsigned RB0 :1;
        unsigned RB1 :1;
        unsigned RB2 :1;
        unsigned RB3 :1;
        unsigned RB4 :1;
        unsigned RB5 :1;
        unsigned RB6 :1;
        unsigned RB7 :1;
    };
    struct {
        unsigned INT0 :1;
        unsigned INT1 :1;
        unsigned INT2 :1;
        unsigned CCP2 :1;
        unsigned KBI0 :1;
        unsigned KBI1 :1;
        unsigned KBI2 :1;
        unsigned KBI3 :1;
    };
    struct {
        unsigned AN12 :1;
        unsigned AN10 :1;
        unsigned AN8 :1;
        unsigned AN9 :1;
        unsigned AN11 :1;
        unsigned PGM :1;
        unsigned PGC :1;
        unsigned PGD :1;
    };
    struct {
        unsigned FLT0 :1;
        unsigned C12IN3M :1;
        unsigned :1;
        unsigned C12IN2M :1;
    };
    struct {
        unsigned :1;
        unsigned C12IN3N :1;
        unsigned :1;
        unsigned C12IN2N :1;
    };
    struct {
        unsigned :1;
        unsigned P1C :1;
        unsigned P1B :1;
        unsigned :1;
        unsigned P1D :1;
    };
    struct {
        unsigned :3;
        unsigned CCP2_PA2 :1;
    };
} PORTBbits_t;
extern volatile PORTBbits_t PORTBbits __attribute__((address(0xF81)));
# 988 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PORTC __attribute__((address(0xF82)));

__asm("PORTC equ 0F82h");


typedef union {
    struct {
        unsigned RC0 :1;
        unsigned RC1 :1;
        unsigned RC2 :1;
        unsigned RC3 :1;
        unsigned RC4 :1;
        unsigned RC5 :1;
        unsigned RC6 :1;
        unsigned RC7 :1;
    };
    struct {
        unsigned T1OSO :1;
        unsigned T1OSI :1;
        unsigned CCP1 :1;
        unsigned SCK :1;
        unsigned SDI :1;
        unsigned SDO :1;
        unsigned TX :1;
        unsigned RX :1;
    };
    struct {
        unsigned T13CKI :1;
        unsigned CCP2 :1;
        unsigned P1A :1;
        unsigned SCL :1;
        unsigned SDA :1;
        unsigned :1;
        unsigned CK :1;
        unsigned DT :1;
    };
    struct {
        unsigned T1CKI :1;
    };
    struct {
        unsigned T3CKI :1;
    };
    struct {
        unsigned :1;
        unsigned PA2 :1;
        unsigned PA1 :1;
    };
} PORTCbits_t;
extern volatile PORTCbits_t PORTCbits __attribute__((address(0xF82)));
# 1176 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PORTE __attribute__((address(0xF84)));

__asm("PORTE equ 0F84h");


typedef union {
    struct {
        unsigned :3;
        unsigned RE3 :1;
    };
    struct {
        unsigned :3;
        unsigned MCLR :1;
    };
    struct {
        unsigned :3;
        unsigned NOT_MCLR :1;
    };
    struct {
        unsigned :3;
        unsigned nMCLR :1;
    };
    struct {
        unsigned :3;
        unsigned VPP :1;
    };
    struct {
        unsigned :3;
        unsigned CCP9E :1;
    };
    struct {
        unsigned :3;
        unsigned PC3E :1;
    };
} PORTEbits_t;
extern volatile PORTEbits_t PORTEbits __attribute__((address(0xF84)));
# 1251 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char LATA __attribute__((address(0xF89)));

__asm("LATA equ 0F89h");


typedef union {
    struct {
        unsigned LATA0 :1;
        unsigned LATA1 :1;
        unsigned LATA2 :1;
        unsigned LATA3 :1;
        unsigned LATA4 :1;
        unsigned LATA5 :1;
        unsigned LATA6 :1;
        unsigned LATA7 :1;
    };
    struct {
        unsigned LA0 :1;
        unsigned LA1 :1;
        unsigned LA2 :1;
        unsigned LA3 :1;
        unsigned LA4 :1;
        unsigned LA5 :1;
        unsigned LA6 :1;
        unsigned LA7 :1;
    };
} LATAbits_t;
extern volatile LATAbits_t LATAbits __attribute__((address(0xF89)));
# 1363 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char LATB __attribute__((address(0xF8A)));

__asm("LATB equ 0F8Ah");


typedef union {
    struct {
        unsigned LATB0 :1;
        unsigned LATB1 :1;
        unsigned LATB2 :1;
        unsigned LATB3 :1;
        unsigned LATB4 :1;
        unsigned LATB5 :1;
        unsigned LATB6 :1;
        unsigned LATB7 :1;
    };
    struct {
        unsigned LB0 :1;
        unsigned LB1 :1;
        unsigned LB2 :1;
        unsigned LB3 :1;
        unsigned LB4 :1;
        unsigned LB5 :1;
        unsigned LB6 :1;
        unsigned LB7 :1;
    };
} LATBbits_t;
extern volatile LATBbits_t LATBbits __attribute__((address(0xF8A)));
# 1475 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char LATC __attribute__((address(0xF8B)));

__asm("LATC equ 0F8Bh");


typedef union {
    struct {
        unsigned LATC0 :1;
        unsigned LATC1 :1;
        unsigned LATC2 :1;
        unsigned LATC3 :1;
        unsigned LATC4 :1;
        unsigned LATC5 :1;
        unsigned LATC6 :1;
        unsigned LATC7 :1;
    };
    struct {
        unsigned LC0 :1;
        unsigned LC1 :1;
        unsigned LC2 :1;
        unsigned LC3 :1;
        unsigned LC4 :1;
        unsigned LC5 :1;
        unsigned LC6 :1;
        unsigned LC7 :1;
    };
} LATCbits_t;
extern volatile LATCbits_t LATCbits __attribute__((address(0xF8B)));
# 1587 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char TRISA __attribute__((address(0xF92)));

__asm("TRISA equ 0F92h");


extern volatile unsigned char DDRA __attribute__((address(0xF92)));

__asm("DDRA equ 0F92h");


typedef union {
    struct {
        unsigned TRISA0 :1;
        unsigned TRISA1 :1;
        unsigned TRISA2 :1;
        unsigned TRISA3 :1;
        unsigned TRISA4 :1;
        unsigned TRISA5 :1;
        unsigned TRISA6 :1;
        unsigned TRISA7 :1;
    };
    struct {
        unsigned RA0 :1;
        unsigned RA1 :1;
        unsigned RA2 :1;
        unsigned RA3 :1;
        unsigned RA4 :1;
        unsigned RA5 :1;
        unsigned RA6 :1;
        unsigned RA7 :1;
    };
} TRISAbits_t;
extern volatile TRISAbits_t TRISAbits __attribute__((address(0xF92)));
# 1702 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
typedef union {
    struct {
        unsigned TRISA0 :1;
        unsigned TRISA1 :1;
        unsigned TRISA2 :1;
        unsigned TRISA3 :1;
        unsigned TRISA4 :1;
        unsigned TRISA5 :1;
        unsigned TRISA6 :1;
        unsigned TRISA7 :1;
    };
    struct {
        unsigned RA0 :1;
        unsigned RA1 :1;
        unsigned RA2 :1;
        unsigned RA3 :1;
        unsigned RA4 :1;
        unsigned RA5 :1;
        unsigned RA6 :1;
        unsigned RA7 :1;
    };
} DDRAbits_t;
extern volatile DDRAbits_t DDRAbits __attribute__((address(0xF92)));
# 1809 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char TRISB __attribute__((address(0xF93)));

__asm("TRISB equ 0F93h");


extern volatile unsigned char DDRB __attribute__((address(0xF93)));

__asm("DDRB equ 0F93h");


typedef union {
    struct {
        unsigned TRISB0 :1;
        unsigned TRISB1 :1;
        unsigned TRISB2 :1;
        unsigned TRISB3 :1;
        unsigned TRISB4 :1;
        unsigned TRISB5 :1;
        unsigned TRISB6 :1;
        unsigned TRISB7 :1;
    };
    struct {
        unsigned RB0 :1;
        unsigned RB1 :1;
        unsigned RB2 :1;
        unsigned RB3 :1;
        unsigned RB4 :1;
        unsigned RB5 :1;
        unsigned RB6 :1;
        unsigned RB7 :1;
    };
} TRISBbits_t;
extern volatile TRISBbits_t TRISBbits __attribute__((address(0xF93)));
# 1924 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
typedef union {
    struct {
        unsigned TRISB0 :1;
        unsigned TRISB1 :1;
        unsigned TRISB2 :1;
        unsigned TRISB3 :1;
        unsigned TRISB4 :1;
        unsigned TRISB5 :1;
        unsigned TRISB6 :1;
        unsigned TRISB7 :1;
    };
    struct {
        unsigned RB0 :1;
        unsigned RB1 :1;
        unsigned RB2 :1;
        unsigned RB3 :1;
        unsigned RB4 :1;
        unsigned RB5 :1;
        unsigned RB6 :1;
        unsigned RB7 :1;
    };
} DDRBbits_t;
extern volatile DDRBbits_t DDRBbits __attribute__((address(0xF93)));
# 2031 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char TRISC __attribute__((address(0xF94)));

__asm("TRISC equ 0F94h");


extern volatile unsigned char DDRC __attribute__((address(0xF94)));

__asm("DDRC equ 0F94h");


typedef union {
    struct {
        unsigned TRISC0 :1;
        unsigned TRISC1 :1;
        unsigned TRISC2 :1;
        unsigned TRISC3 :1;
        unsigned TRISC4 :1;
        unsigned TRISC5 :1;
        unsigned TRISC6 :1;
        unsigned TRISC7 :1;
    };
    struct {
        unsigned RC0 :1;
        unsigned RC1 :1;
        unsigned RC2 :1;
        unsigned RC3 :1;
        unsigned RC4 :1;
        unsigned RC5 :1;
        unsigned RC6 :1;
        unsigned RC7 :1;
    };
} TRISCbits_t;
extern volatile TRISCbits_t TRISCbits __attribute__((address(0xF94)));
# 2146 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
typedef union {
    struct {
        unsigned TRISC0 :1;
        unsigned TRISC1 :1;
        unsigned TRISC2 :1;
        unsigned TRISC3 :1;
        unsigned TRISC4 :1;
        unsigned TRISC5 :1;
        unsigned TRISC6 :1;
        unsigned TRISC7 :1;
    };
    struct {
        unsigned RC0 :1;
        unsigned RC1 :1;
        unsigned RC2 :1;
        unsigned RC3 :1;
        unsigned RC4 :1;
        unsigned RC5 :1;
        unsigned RC6 :1;
        unsigned RC7 :1;
    };
} DDRCbits_t;
extern volatile DDRCbits_t DDRCbits __attribute__((address(0xF94)));
# 2253 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char OSCTUNE __attribute__((address(0xF9B)));

__asm("OSCTUNE equ 0F9Bh");


typedef union {
    struct {
        unsigned TUN :6;
        unsigned PLLEN :1;
        unsigned INTSRC :1;
    };
    struct {
        unsigned TUN0 :1;
        unsigned TUN1 :1;
        unsigned TUN2 :1;
        unsigned TUN3 :1;
        unsigned TUN4 :1;
        unsigned TUN5 :1;
    };
} OSCTUNEbits_t;
extern volatile OSCTUNEbits_t OSCTUNEbits __attribute__((address(0xF9B)));
# 2323 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PIE1 __attribute__((address(0xF9D)));

__asm("PIE1 equ 0F9Dh");


typedef union {
    struct {
        unsigned TMR1IE :1;
        unsigned TMR2IE :1;
        unsigned CCP1IE :1;
        unsigned SSPIE :1;
        unsigned TXIE :1;
        unsigned RCIE :1;
        unsigned ADIE :1;
    };
    struct {
        unsigned :4;
        unsigned TX1IE :1;
        unsigned RC1IE :1;
    };
} PIE1bits_t;
extern volatile PIE1bits_t PIE1bits __attribute__((address(0xF9D)));
# 2394 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PIR1 __attribute__((address(0xF9E)));

__asm("PIR1 equ 0F9Eh");


typedef union {
    struct {
        unsigned TMR1IF :1;
        unsigned TMR2IF :1;
        unsigned CCP1IF :1;
        unsigned SSPIF :1;
        unsigned TXIF :1;
        unsigned RCIF :1;
        unsigned ADIF :1;
    };
    struct {
        unsigned :4;
        unsigned TX1IF :1;
        unsigned RC1IF :1;
    };
} PIR1bits_t;
extern volatile PIR1bits_t PIR1bits __attribute__((address(0xF9E)));
# 2465 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char IPR1 __attribute__((address(0xF9F)));

__asm("IPR1 equ 0F9Fh");


typedef union {
    struct {
        unsigned TMR1IP :1;
        unsigned TMR2IP :1;
        unsigned CCP1IP :1;
        unsigned SSPIP :1;
        unsigned TXIP :1;
        unsigned RCIP :1;
        unsigned ADIP :1;
    };
    struct {
        unsigned :4;
        unsigned TX1IP :1;
        unsigned RC1IP :1;
    };
} IPR1bits_t;
extern volatile IPR1bits_t IPR1bits __attribute__((address(0xF9F)));
# 2536 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PIE2 __attribute__((address(0xFA0)));

__asm("PIE2 equ 0FA0h");


typedef union {
    struct {
        unsigned CCP2IE :1;
        unsigned TMR3IE :1;
        unsigned HLVDIE :1;
        unsigned BCLIE :1;
        unsigned EEIE :1;
        unsigned C2IE :1;
        unsigned C1IE :1;
        unsigned OSCFIE :1;
    };
    struct {
        unsigned :2;
        unsigned LVDIE :1;
    };
    struct {
        unsigned :6;
        unsigned CMIE :1;
    };
} PIE2bits_t;
extern volatile PIE2bits_t PIE2bits __attribute__((address(0xFA0)));
# 2616 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PIR2 __attribute__((address(0xFA1)));

__asm("PIR2 equ 0FA1h");


typedef union {
    struct {
        unsigned CCP2IF :1;
        unsigned TMR3IF :1;
        unsigned HLVDIF :1;
        unsigned BCLIF :1;
        unsigned EEIF :1;
        unsigned C2IF :1;
        unsigned C1IF :1;
        unsigned OSCFIF :1;
    };
    struct {
        unsigned :2;
        unsigned LVDIF :1;
    };
    struct {
        unsigned :6;
        unsigned CMIF :1;
    };
} PIR2bits_t;
extern volatile PIR2bits_t PIR2bits __attribute__((address(0xFA1)));
# 2696 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char IPR2 __attribute__((address(0xFA2)));

__asm("IPR2 equ 0FA2h");


typedef union {
    struct {
        unsigned CCP2IP :1;
        unsigned TMR3IP :1;
        unsigned HLVDIP :1;
        unsigned BCLIP :1;
        unsigned EEIP :1;
        unsigned C2IP :1;
        unsigned C1IP :1;
        unsigned OSCFIP :1;
    };
    struct {
        unsigned :2;
        unsigned LVDIP :1;
    };
    struct {
        unsigned :6;
        unsigned CMIP :1;
    };
} IPR2bits_t;
extern volatile IPR2bits_t IPR2bits __attribute__((address(0xFA2)));
# 2776 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char EECON1 __attribute__((address(0xFA6)));

__asm("EECON1 equ 0FA6h");


typedef union {
    struct {
        unsigned RD :1;
        unsigned WR :1;
        unsigned WREN :1;
        unsigned WRERR :1;
        unsigned FREE :1;
        unsigned :1;
        unsigned CFGS :1;
        unsigned EEPGD :1;
    };
    struct {
        unsigned :6;
        unsigned EEFS :1;
    };
} EECON1bits_t;
extern volatile EECON1bits_t EECON1bits __attribute__((address(0xFA6)));
# 2842 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char EECON2 __attribute__((address(0xFA7)));

__asm("EECON2 equ 0FA7h");




extern volatile unsigned char EEDATA __attribute__((address(0xFA8)));

__asm("EEDATA equ 0FA8h");




extern volatile unsigned char EEADR __attribute__((address(0xFA9)));

__asm("EEADR equ 0FA9h");


typedef union {
    struct {
        unsigned EEADR0 :1;
        unsigned EEADR1 :1;
        unsigned EEADR2 :1;
        unsigned EEADR3 :1;
        unsigned EEADR4 :1;
        unsigned EEADR5 :1;
        unsigned EEADR6 :1;
        unsigned EEADR7 :1;
    };
} EEADRbits_t;
extern volatile EEADRbits_t EEADRbits __attribute__((address(0xFA9)));
# 2918 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char EEADRH __attribute__((address(0xFAA)));

__asm("EEADRH equ 0FAAh");


typedef union {
    struct {
        unsigned EEADR8 :1;
        unsigned EEADR9 :1;
    };
} EEADRHbits_t;
extern volatile EEADRHbits_t EEADRHbits __attribute__((address(0xFAA)));
# 2944 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char RCSTA __attribute__((address(0xFAB)));

__asm("RCSTA equ 0FABh");


extern volatile unsigned char RCSTA1 __attribute__((address(0xFAB)));

__asm("RCSTA1 equ 0FABh");


typedef union {
    struct {
        unsigned RX9D :1;
        unsigned OERR :1;
        unsigned FERR :1;
        unsigned ADDEN :1;
        unsigned CREN :1;
        unsigned SREN :1;
        unsigned RX9 :1;
        unsigned SPEN :1;
    };
    struct {
        unsigned :3;
        unsigned ADEN :1;
    };
    struct {
        unsigned :5;
        unsigned SRENA :1;
    };
    struct {
        unsigned :6;
        unsigned RC8_9 :1;
    };
    struct {
        unsigned :6;
        unsigned RC9 :1;
    };
    struct {
        unsigned RCD8 :1;
    };
} RCSTAbits_t;
extern volatile RCSTAbits_t RCSTAbits __attribute__((address(0xFAB)));
# 3053 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
typedef union {
    struct {
        unsigned RX9D :1;
        unsigned OERR :1;
        unsigned FERR :1;
        unsigned ADDEN :1;
        unsigned CREN :1;
        unsigned SREN :1;
        unsigned RX9 :1;
        unsigned SPEN :1;
    };
    struct {
        unsigned :3;
        unsigned ADEN :1;
    };
    struct {
        unsigned :5;
        unsigned SRENA :1;
    };
    struct {
        unsigned :6;
        unsigned RC8_9 :1;
    };
    struct {
        unsigned :6;
        unsigned RC9 :1;
    };
    struct {
        unsigned RCD8 :1;
    };
} RCSTA1bits_t;
extern volatile RCSTA1bits_t RCSTA1bits __attribute__((address(0xFAB)));
# 3154 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char TXSTA __attribute__((address(0xFAC)));

__asm("TXSTA equ 0FACh");


extern volatile unsigned char TXSTA1 __attribute__((address(0xFAC)));

__asm("TXSTA1 equ 0FACh");


typedef union {
    struct {
        unsigned TX9D :1;
        unsigned TRMT :1;
        unsigned BRGH :1;
        unsigned SENDB :1;
        unsigned SYNC :1;
        unsigned TXEN :1;
        unsigned TX9 :1;
        unsigned CSRC :1;
    };
    struct {
        unsigned TX9D1 :1;
        unsigned TRMT1 :1;
        unsigned BRGH1 :1;
        unsigned SENDB1 :1;
        unsigned SYNC1 :1;
        unsigned TXEN1 :1;
        unsigned TX91 :1;
        unsigned CSRC1 :1;
    };
    struct {
        unsigned :6;
        unsigned TX8_9 :1;
    };
    struct {
        unsigned TXD8 :1;
    };
} TXSTAbits_t;
extern volatile TXSTAbits_t TXSTAbits __attribute__((address(0xFAC)));
# 3286 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
typedef union {
    struct {
        unsigned TX9D :1;
        unsigned TRMT :1;
        unsigned BRGH :1;
        unsigned SENDB :1;
        unsigned SYNC :1;
        unsigned TXEN :1;
        unsigned TX9 :1;
        unsigned CSRC :1;
    };
    struct {
        unsigned TX9D1 :1;
        unsigned TRMT1 :1;
        unsigned BRGH1 :1;
        unsigned SENDB1 :1;
        unsigned SYNC1 :1;
        unsigned TXEN1 :1;
        unsigned TX91 :1;
        unsigned CSRC1 :1;
    };
    struct {
        unsigned :6;
        unsigned TX8_9 :1;
    };
    struct {
        unsigned TXD8 :1;
    };
} TXSTA1bits_t;
extern volatile TXSTA1bits_t TXSTA1bits __attribute__((address(0xFAC)));
# 3410 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char TXREG __attribute__((address(0xFAD)));

__asm("TXREG equ 0FADh");


extern volatile unsigned char TXREG1 __attribute__((address(0xFAD)));

__asm("TXREG1 equ 0FADh");




extern volatile unsigned char RCREG __attribute__((address(0xFAE)));

__asm("RCREG equ 0FAEh");


extern volatile unsigned char RCREG1 __attribute__((address(0xFAE)));

__asm("RCREG1 equ 0FAEh");




extern volatile unsigned char SPBRG __attribute__((address(0xFAF)));

__asm("SPBRG equ 0FAFh");


extern volatile unsigned char SPBRG1 __attribute__((address(0xFAF)));

__asm("SPBRG1 equ 0FAFh");




extern volatile unsigned char SPBRGH __attribute__((address(0xFB0)));

__asm("SPBRGH equ 0FB0h");




extern volatile unsigned char T3CON __attribute__((address(0xFB1)));

__asm("T3CON equ 0FB1h");


typedef union {
    struct {
        unsigned :2;
        unsigned NOT_T3SYNC :1;
    };
    struct {
        unsigned TMR3ON :1;
        unsigned TMR3CS :1;
        unsigned nT3SYNC :1;
        unsigned T3CCP1 :1;
        unsigned T3CKPS :2;
        unsigned T3CCP2 :1;
        unsigned RD16 :1;
    };
    struct {
        unsigned :2;
        unsigned T3SYNC :1;
        unsigned :1;
        unsigned T3CKPS0 :1;
        unsigned T3CKPS1 :1;
    };
    struct {
        unsigned :3;
        unsigned SOSCEN3 :1;
        unsigned :3;
        unsigned RD163 :1;
    };
    struct {
        unsigned :7;
        unsigned T3RD16 :1;
    };
} T3CONbits_t;
extern volatile T3CONbits_t T3CONbits __attribute__((address(0xFB1)));
# 3565 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned short TMR3 __attribute__((address(0xFB2)));

__asm("TMR3 equ 0FB2h");




extern volatile unsigned char TMR3L __attribute__((address(0xFB2)));

__asm("TMR3L equ 0FB2h");




extern volatile unsigned char TMR3H __attribute__((address(0xFB3)));

__asm("TMR3H equ 0FB3h");




extern volatile unsigned char CVRCON2 __attribute__((address(0xFB4)));

__asm("CVRCON2 equ 0FB4h");


typedef union {
    struct {
        unsigned :6;
        unsigned FVRST :1;
        unsigned FVREN :1;
    };
} CVRCON2bits_t;
extern volatile CVRCON2bits_t CVRCON2bits __attribute__((address(0xFB4)));
# 3613 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char CVRCON __attribute__((address(0xFB5)));

__asm("CVRCON equ 0FB5h");


typedef union {
    struct {
        unsigned CVR :4;
        unsigned CVRSS :1;
        unsigned CVRR :1;
        unsigned CVROE :1;
        unsigned CVREN :1;
    };
    struct {
        unsigned CVR0 :1;
        unsigned CVR1 :1;
        unsigned CVR2 :1;
        unsigned CVR3 :1;
    };
    struct {
        unsigned :6;
        unsigned CVROEN :1;
    };
} CVRCONbits_t;
extern volatile CVRCONbits_t CVRCONbits __attribute__((address(0xFB5)));
# 3692 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char ECCP1AS __attribute__((address(0xFB6)));

__asm("ECCP1AS equ 0FB6h");


typedef union {
    struct {
        unsigned PSSBD :2;
        unsigned PSSAC :2;
        unsigned ECCPAS :3;
        unsigned ECCPASE :1;
    };
    struct {
        unsigned PSSBD0 :1;
        unsigned PSSBD1 :1;
        unsigned PSSAC0 :1;
        unsigned PSSAC1 :1;
        unsigned ECCPAS0 :1;
        unsigned ECCPAS1 :1;
        unsigned ECCPAS2 :1;
    };
} ECCP1ASbits_t;
extern volatile ECCP1ASbits_t ECCP1ASbits __attribute__((address(0xFB6)));
# 3774 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PWM1CON __attribute__((address(0xFB7)));

__asm("PWM1CON equ 0FB7h");


typedef union {
    struct {
        unsigned PDC :7;
        unsigned PRSEN :1;
    };
    struct {
        unsigned PDC0 :1;
        unsigned PDC1 :1;
        unsigned PDC2 :1;
        unsigned PDC3 :1;
        unsigned PDC4 :1;
        unsigned PDC5 :1;
        unsigned PDC6 :1;
    };
} PWM1CONbits_t;
extern volatile PWM1CONbits_t PWM1CONbits __attribute__((address(0xFB7)));
# 3844 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char BAUDCON __attribute__((address(0xFB8)));

__asm("BAUDCON equ 0FB8h");


extern volatile unsigned char BAUDCTL __attribute__((address(0xFB8)));

__asm("BAUDCTL equ 0FB8h");


typedef union {
    struct {
        unsigned ABDEN :1;
        unsigned WUE :1;
        unsigned :1;
        unsigned BRG16 :1;
        unsigned CKTXP :1;
        unsigned DTRXP :1;
        unsigned RCIDL :1;
        unsigned ABDOVF :1;
    };
    struct {
        unsigned :4;
        unsigned SCKP :1;
    };
    struct {
        unsigned :5;
        unsigned RXCKP :1;
    };
    struct {
        unsigned :1;
        unsigned W4E :1;
    };
} BAUDCONbits_t;
extern volatile BAUDCONbits_t BAUDCONbits __attribute__((address(0xFB8)));
# 3931 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
typedef union {
    struct {
        unsigned ABDEN :1;
        unsigned WUE :1;
        unsigned :1;
        unsigned BRG16 :1;
        unsigned CKTXP :1;
        unsigned DTRXP :1;
        unsigned RCIDL :1;
        unsigned ABDOVF :1;
    };
    struct {
        unsigned :4;
        unsigned SCKP :1;
    };
    struct {
        unsigned :5;
        unsigned RXCKP :1;
    };
    struct {
        unsigned :1;
        unsigned W4E :1;
    };
} BAUDCTLbits_t;
extern volatile BAUDCTLbits_t BAUDCTLbits __attribute__((address(0xFB8)));
# 4010 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PSTRCON __attribute__((address(0xFB9)));

__asm("PSTRCON equ 0FB9h");


typedef union {
    struct {
        unsigned STRA :1;
        unsigned STRB :1;
        unsigned STRC :1;
        unsigned STRD :1;
        unsigned STRSYNC :1;
    };
} PSTRCONbits_t;
extern volatile PSTRCONbits_t PSTRCONbits __attribute__((address(0xFB9)));
# 4054 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char CCP2CON __attribute__((address(0xFBA)));

__asm("CCP2CON equ 0FBAh");


typedef union {
    struct {
        unsigned CCP2M :4;
        unsigned DC2B :2;
    };
    struct {
        unsigned CCP2M0 :1;
        unsigned CCP2M1 :1;
        unsigned CCP2M2 :1;
        unsigned CCP2M3 :1;
        unsigned DC2B0 :1;
        unsigned DC2B1 :1;
    };
} CCP2CONbits_t;
extern volatile CCP2CONbits_t CCP2CONbits __attribute__((address(0xFBA)));
# 4118 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned short CCPR2 __attribute__((address(0xFBB)));

__asm("CCPR2 equ 0FBBh");




extern volatile unsigned char CCPR2L __attribute__((address(0xFBB)));

__asm("CCPR2L equ 0FBBh");




extern volatile unsigned char CCPR2H __attribute__((address(0xFBC)));

__asm("CCPR2H equ 0FBCh");




extern volatile unsigned char CCP1CON __attribute__((address(0xFBD)));

__asm("CCP1CON equ 0FBDh");


typedef union {
    struct {
        unsigned CCP1M :4;
        unsigned DC1B :2;
        unsigned P1M :2;
    };
    struct {
        unsigned CCP1M0 :1;
        unsigned CCP1M1 :1;
        unsigned CCP1M2 :1;
        unsigned CCP1M3 :1;
        unsigned DC1B0 :1;
        unsigned DC1B1 :1;
        unsigned P1M0 :1;
        unsigned P1M1 :1;
    };
} CCP1CONbits_t;
extern volatile CCP1CONbits_t CCP1CONbits __attribute__((address(0xFBD)));
# 4221 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned short CCPR1 __attribute__((address(0xFBE)));

__asm("CCPR1 equ 0FBEh");




extern volatile unsigned char CCPR1L __attribute__((address(0xFBE)));

__asm("CCPR1L equ 0FBEh");




extern volatile unsigned char CCPR1H __attribute__((address(0xFBF)));

__asm("CCPR1H equ 0FBFh");




extern volatile unsigned char ADCON2 __attribute__((address(0xFC0)));

__asm("ADCON2 equ 0FC0h");


typedef union {
    struct {
        unsigned ADCS :3;
        unsigned ACQT :3;
        unsigned :1;
        unsigned ADFM :1;
    };
    struct {
        unsigned ADCS0 :1;
        unsigned ADCS1 :1;
        unsigned ADCS2 :1;
        unsigned ACQT0 :1;
        unsigned ACQT1 :1;
        unsigned ACQT2 :1;
    };
} ADCON2bits_t;
extern volatile ADCON2bits_t ADCON2bits __attribute__((address(0xFC0)));
# 4313 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char ADCON1 __attribute__((address(0xFC1)));

__asm("ADCON1 equ 0FC1h");


typedef union {
    struct {
        unsigned :4;
        unsigned VCFG :2;
    };
    struct {
        unsigned :4;
        unsigned VCFG0 :1;
        unsigned VCFG1 :1;
    };
    struct {
        unsigned :4;
        unsigned VCFG01 :1;
        unsigned VCFG11 :1;
    };
} ADCON1bits_t;
extern volatile ADCON1bits_t ADCON1bits __attribute__((address(0xFC1)));
# 4364 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char ADCON0 __attribute__((address(0xFC2)));

__asm("ADCON0 equ 0FC2h");


typedef union {
    struct {
        unsigned :1;
        unsigned GO_NOT_DONE :1;
    };
    struct {
        unsigned ADON :1;
        unsigned GO_nDONE :1;
        unsigned CHS :4;
    };
    struct {
        unsigned :1;
        unsigned DONE :1;
        unsigned CHS0 :1;
        unsigned CHS1 :1;
        unsigned CHS2 :1;
        unsigned CHS3 :1;
    };
    struct {
        unsigned :1;
        unsigned NOT_DONE :1;
    };
    struct {
        unsigned :1;
        unsigned nDONE :1;
    };
    struct {
        unsigned :1;
        unsigned GO_DONE :1;
    };
    struct {
        unsigned :1;
        unsigned GO :1;
    };
    struct {
        unsigned :1;
        unsigned GODONE :1;
    };
} ADCON0bits_t;
extern volatile ADCON0bits_t ADCON0bits __attribute__((address(0xFC2)));
# 4483 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned short ADRES __attribute__((address(0xFC3)));

__asm("ADRES equ 0FC3h");




extern volatile unsigned char ADRESL __attribute__((address(0xFC3)));

__asm("ADRESL equ 0FC3h");




extern volatile unsigned char ADRESH __attribute__((address(0xFC4)));

__asm("ADRESH equ 0FC4h");




extern volatile unsigned char SSPCON2 __attribute__((address(0xFC5)));

__asm("SSPCON2 equ 0FC5h");


typedef union {
    struct {
        unsigned SEN :1;
        unsigned RSEN :1;
        unsigned PEN :1;
        unsigned RCEN :1;
        unsigned ACKEN :1;
        unsigned ACKDT :1;
        unsigned ACKSTAT :1;
        unsigned GCEN :1;
    };
} SSPCON2bits_t;
extern volatile SSPCON2bits_t SSPCON2bits __attribute__((address(0xFC5)));
# 4566 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char SSPCON1 __attribute__((address(0xFC6)));

__asm("SSPCON1 equ 0FC6h");


typedef union {
    struct {
        unsigned SSPM :4;
        unsigned CKP :1;
        unsigned SSPEN :1;
        unsigned SSPOV :1;
        unsigned WCOL :1;
    };
    struct {
        unsigned SSPM0 :1;
        unsigned SSPM1 :1;
        unsigned SSPM2 :1;
        unsigned SSPM3 :1;
    };
} SSPCON1bits_t;
extern volatile SSPCON1bits_t SSPCON1bits __attribute__((address(0xFC6)));
# 4636 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char SSPSTAT __attribute__((address(0xFC7)));

__asm("SSPSTAT equ 0FC7h");


typedef union {
    struct {
        unsigned :2;
        unsigned R_NOT_W :1;
    };
    struct {
        unsigned :5;
        unsigned D_NOT_A :1;
    };
    struct {
        unsigned BF :1;
        unsigned UA :1;
        unsigned R_nW :1;
        unsigned S :1;
        unsigned P :1;
        unsigned D_nA :1;
        unsigned CKE :1;
        unsigned SMP :1;
    };
    struct {
        unsigned :2;
        unsigned R :1;
        unsigned :2;
        unsigned D :1;
    };
    struct {
        unsigned :2;
        unsigned W :1;
        unsigned :2;
        unsigned A :1;
    };
    struct {
        unsigned :2;
        unsigned nW :1;
        unsigned :2;
        unsigned nA :1;
    };
    struct {
        unsigned :2;
        unsigned R_W :1;
        unsigned :2;
        unsigned D_A :1;
    };
    struct {
        unsigned :2;
        unsigned NOT_WRITE :1;
    };
    struct {
        unsigned :5;
        unsigned NOT_ADDRESS :1;
    };
    struct {
        unsigned :2;
        unsigned nWRITE :1;
        unsigned :2;
        unsigned nADDRESS :1;
    };
    struct {
        unsigned :3;
        unsigned START :1;
        unsigned STOP :1;
    };
    struct {
        unsigned :2;
        unsigned RW :1;
        unsigned :2;
        unsigned DA :1;
    };
    struct {
        unsigned :2;
        unsigned NOT_W :1;
        unsigned :2;
        unsigned NOT_A :1;
    };
} SSPSTATbits_t;
extern volatile SSPSTATbits_t SSPSTATbits __attribute__((address(0xFC7)));
# 4861 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char SSPADD __attribute__((address(0xFC8)));

__asm("SSPADD equ 0FC8h");




extern volatile unsigned char SSPBUF __attribute__((address(0xFC9)));

__asm("SSPBUF equ 0FC9h");




extern volatile unsigned char T2CON __attribute__((address(0xFCA)));

__asm("T2CON equ 0FCAh");


typedef union {
    struct {
        unsigned T2CKPS :2;
        unsigned TMR2ON :1;
        unsigned T2OUTPS :4;
    };
    struct {
        unsigned T2CKPS0 :1;
        unsigned T2CKPS1 :1;
        unsigned :1;
        unsigned T2OUTPS0 :1;
        unsigned T2OUTPS1 :1;
        unsigned T2OUTPS2 :1;
        unsigned T2OUTPS3 :1;
    };
} T2CONbits_t;
extern volatile T2CONbits_t T2CONbits __attribute__((address(0xFCA)));
# 4946 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char PR2 __attribute__((address(0xFCB)));

__asm("PR2 equ 0FCBh");


extern volatile unsigned char MEMCON __attribute__((address(0xFCB)));

__asm("MEMCON equ 0FCBh");


typedef union {
    struct {
        unsigned :7;
        unsigned EBDIS :1;
    };
    struct {
        unsigned :4;
        unsigned WAIT0 :1;
    };
    struct {
        unsigned :5;
        unsigned WAIT1 :1;
    };
    struct {
        unsigned WM0 :1;
    };
    struct {
        unsigned :1;
        unsigned WM1 :1;
    };
} PR2bits_t;
extern volatile PR2bits_t PR2bits __attribute__((address(0xFCB)));
# 5005 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
typedef union {
    struct {
        unsigned :7;
        unsigned EBDIS :1;
    };
    struct {
        unsigned :4;
        unsigned WAIT0 :1;
    };
    struct {
        unsigned :5;
        unsigned WAIT1 :1;
    };
    struct {
        unsigned WM0 :1;
    };
    struct {
        unsigned :1;
        unsigned WM1 :1;
    };
} MEMCONbits_t;
extern volatile MEMCONbits_t MEMCONbits __attribute__((address(0xFCB)));
# 5056 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char TMR2 __attribute__((address(0xFCC)));

__asm("TMR2 equ 0FCCh");




extern volatile unsigned char T1CON __attribute__((address(0xFCD)));

__asm("T1CON equ 0FCDh");


typedef union {
    struct {
        unsigned :2;
        unsigned NOT_T1SYNC :1;
    };
    struct {
        unsigned TMR1ON :1;
        unsigned TMR1CS :1;
        unsigned nT1SYNC :1;
        unsigned T1OSCEN :1;
        unsigned T1CKPS :2;
        unsigned T1RUN :1;
        unsigned RD16 :1;
    };
    struct {
        unsigned :2;
        unsigned T1SYNC :1;
        unsigned :1;
        unsigned T1CKPS0 :1;
        unsigned T1CKPS1 :1;
    };
    struct {
        unsigned :3;
        unsigned SOSCEN :1;
        unsigned :3;
        unsigned T1RD16 :1;
    };
} T1CONbits_t;
extern volatile T1CONbits_t T1CONbits __attribute__((address(0xFCD)));
# 5166 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned short TMR1 __attribute__((address(0xFCE)));

__asm("TMR1 equ 0FCEh");




extern volatile unsigned char TMR1L __attribute__((address(0xFCE)));

__asm("TMR1L equ 0FCEh");




extern volatile unsigned char TMR1H __attribute__((address(0xFCF)));

__asm("TMR1H equ 0FCFh");




extern volatile unsigned char RCON __attribute__((address(0xFD0)));

__asm("RCON equ 0FD0h");


typedef union {
    struct {
        unsigned NOT_BOR :1;
    };
    struct {
        unsigned :1;
        unsigned NOT_POR :1;
    };
    struct {
        unsigned :2;
        unsigned NOT_PD :1;
    };
    struct {
        unsigned :3;
        unsigned NOT_TO :1;
    };
    struct {
        unsigned :4;
        unsigned NOT_RI :1;
    };
    struct {
        unsigned nBOR :1;
        unsigned nPOR :1;
        unsigned nPD :1;
        unsigned nTO :1;
        unsigned nRI :1;
        unsigned :1;
        unsigned SBOREN :1;
        unsigned IPEN :1;
    };
    struct {
        unsigned BOR :1;
        unsigned POR :1;
        unsigned PD :1;
        unsigned TO :1;
        unsigned RI :1;
    };
} RCONbits_t;
extern volatile RCONbits_t RCONbits __attribute__((address(0xFD0)));
# 5320 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char WDTCON __attribute__((address(0xFD1)));

__asm("WDTCON equ 0FD1h");


typedef union {
    struct {
        unsigned SWDTEN :1;
    };
    struct {
        unsigned SWDTE :1;
    };
} WDTCONbits_t;
extern volatile WDTCONbits_t WDTCONbits __attribute__((address(0xFD1)));
# 5348 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char HLVDCON __attribute__((address(0xFD2)));

__asm("HLVDCON equ 0FD2h");


extern volatile unsigned char LVDCON __attribute__((address(0xFD2)));

__asm("LVDCON equ 0FD2h");


typedef union {
    struct {
        unsigned HLVDL :4;
        unsigned HLVDEN :1;
        unsigned IRVST :1;
        unsigned :1;
        unsigned VDIRMAG :1;
    };
    struct {
        unsigned HLVDL0 :1;
        unsigned HLVDL1 :1;
        unsigned HLVDL2 :1;
        unsigned HLVDL3 :1;
    };
    struct {
        unsigned LVDL0 :1;
        unsigned LVDL1 :1;
        unsigned LVDL2 :1;
        unsigned LVDL3 :1;
        unsigned LVDEN :1;
        unsigned IVRST :1;
    };
    struct {
        unsigned LVV0 :1;
        unsigned LVV1 :1;
        unsigned LVV2 :1;
        unsigned LVV3 :1;
        unsigned :1;
        unsigned BGST :1;
    };
} HLVDCONbits_t;
extern volatile HLVDCONbits_t HLVDCONbits __attribute__((address(0xFD2)));
# 5487 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
typedef union {
    struct {
        unsigned HLVDL :4;
        unsigned HLVDEN :1;
        unsigned IRVST :1;
        unsigned :1;
        unsigned VDIRMAG :1;
    };
    struct {
        unsigned HLVDL0 :1;
        unsigned HLVDL1 :1;
        unsigned HLVDL2 :1;
        unsigned HLVDL3 :1;
    };
    struct {
        unsigned LVDL0 :1;
        unsigned LVDL1 :1;
        unsigned LVDL2 :1;
        unsigned LVDL3 :1;
        unsigned LVDEN :1;
        unsigned IVRST :1;
    };
    struct {
        unsigned LVV0 :1;
        unsigned LVV1 :1;
        unsigned LVV2 :1;
        unsigned LVV3 :1;
        unsigned :1;
        unsigned BGST :1;
    };
} LVDCONbits_t;
extern volatile LVDCONbits_t LVDCONbits __attribute__((address(0xFD2)));
# 5618 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char OSCCON __attribute__((address(0xFD3)));

__asm("OSCCON equ 0FD3h");


typedef union {
    struct {
        unsigned SCS :2;
        unsigned IOFS :1;
        unsigned OSTS :1;
        unsigned IRCF :3;
        unsigned IDLEN :1;
    };
    struct {
        unsigned SCS0 :1;
        unsigned SCS1 :1;
        unsigned :2;
        unsigned IRCF0 :1;
        unsigned IRCF1 :1;
        unsigned IRCF2 :1;
    };
} OSCCONbits_t;
extern volatile OSCCONbits_t OSCCONbits __attribute__((address(0xFD3)));
# 5695 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char T0CON __attribute__((address(0xFD5)));

__asm("T0CON equ 0FD5h");


typedef union {
    struct {
        unsigned T0PS :3;
        unsigned PSA :1;
        unsigned T0SE :1;
        unsigned T0CS :1;
        unsigned T08BIT :1;
        unsigned TMR0ON :1;
    };
    struct {
        unsigned T0PS0 :1;
        unsigned T0PS1 :1;
        unsigned T0PS2 :1;
    };
} T0CONbits_t;
extern volatile T0CONbits_t T0CONbits __attribute__((address(0xFD5)));
# 5765 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned short TMR0 __attribute__((address(0xFD6)));

__asm("TMR0 equ 0FD6h");




extern volatile unsigned char TMR0L __attribute__((address(0xFD6)));

__asm("TMR0L equ 0FD6h");




extern volatile unsigned char TMR0H __attribute__((address(0xFD7)));

__asm("TMR0H equ 0FD7h");




extern volatile unsigned char STATUS __attribute__((address(0xFD8)));

__asm("STATUS equ 0FD8h");


typedef union {
    struct {
        unsigned C :1;
        unsigned DC :1;
        unsigned Z :1;
        unsigned OV :1;
        unsigned N :1;
    };
    struct {
        unsigned CARRY :1;
        unsigned :1;
        unsigned ZERO :1;
        unsigned OVERFLOW :1;
        unsigned NEGATIVE :1;
    };
} STATUSbits_t;
extern volatile STATUSbits_t STATUSbits __attribute__((address(0xFD8)));
# 5857 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned short FSR2 __attribute__((address(0xFD9)));

__asm("FSR2 equ 0FD9h");




extern volatile unsigned char FSR2L __attribute__((address(0xFD9)));

__asm("FSR2L equ 0FD9h");




extern volatile unsigned char FSR2H __attribute__((address(0xFDA)));

__asm("FSR2H equ 0FDAh");




extern volatile unsigned char PLUSW2 __attribute__((address(0xFDB)));

__asm("PLUSW2 equ 0FDBh");




extern volatile unsigned char PREINC2 __attribute__((address(0xFDC)));

__asm("PREINC2 equ 0FDCh");




extern volatile unsigned char POSTDEC2 __attribute__((address(0xFDD)));

__asm("POSTDEC2 equ 0FDDh");




extern volatile unsigned char POSTINC2 __attribute__((address(0xFDE)));

__asm("POSTINC2 equ 0FDEh");




extern volatile unsigned char INDF2 __attribute__((address(0xFDF)));

__asm("INDF2 equ 0FDFh");




extern volatile unsigned char BSR __attribute__((address(0xFE0)));

__asm("BSR equ 0FE0h");




extern volatile unsigned short FSR1 __attribute__((address(0xFE1)));

__asm("FSR1 equ 0FE1h");




extern volatile unsigned char FSR1L __attribute__((address(0xFE1)));

__asm("FSR1L equ 0FE1h");




extern volatile unsigned char FSR1H __attribute__((address(0xFE2)));

__asm("FSR1H equ 0FE2h");




extern volatile unsigned char PLUSW1 __attribute__((address(0xFE3)));

__asm("PLUSW1 equ 0FE3h");




extern volatile unsigned char PREINC1 __attribute__((address(0xFE4)));

__asm("PREINC1 equ 0FE4h");




extern volatile unsigned char POSTDEC1 __attribute__((address(0xFE5)));

__asm("POSTDEC1 equ 0FE5h");




extern volatile unsigned char POSTINC1 __attribute__((address(0xFE6)));

__asm("POSTINC1 equ 0FE6h");




extern volatile unsigned char INDF1 __attribute__((address(0xFE7)));

__asm("INDF1 equ 0FE7h");




extern volatile unsigned char WREG __attribute__((address(0xFE8)));

__asm("WREG equ 0FE8h");
# 5988 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned short FSR0 __attribute__((address(0xFE9)));

__asm("FSR0 equ 0FE9h");




extern volatile unsigned char FSR0L __attribute__((address(0xFE9)));

__asm("FSR0L equ 0FE9h");




extern volatile unsigned char FSR0H __attribute__((address(0xFEA)));

__asm("FSR0H equ 0FEAh");




extern volatile unsigned char PLUSW0 __attribute__((address(0xFEB)));

__asm("PLUSW0 equ 0FEBh");




extern volatile unsigned char PREINC0 __attribute__((address(0xFEC)));

__asm("PREINC0 equ 0FECh");




extern volatile unsigned char POSTDEC0 __attribute__((address(0xFED)));

__asm("POSTDEC0 equ 0FEDh");




extern volatile unsigned char POSTINC0 __attribute__((address(0xFEE)));

__asm("POSTINC0 equ 0FEEh");




extern volatile unsigned char INDF0 __attribute__((address(0xFEF)));

__asm("INDF0 equ 0FEFh");




extern volatile unsigned char INTCON3 __attribute__((address(0xFF0)));

__asm("INTCON3 equ 0FF0h");


typedef union {
    struct {
        unsigned INT1IF :1;
        unsigned INT2IF :1;
        unsigned :1;
        unsigned INT1IE :1;
        unsigned INT2IE :1;
        unsigned :1;
        unsigned INT1IP :1;
        unsigned INT2IP :1;
    };
    struct {
        unsigned INT1F :1;
        unsigned INT2F :1;
        unsigned :1;
        unsigned INT1E :1;
        unsigned INT2E :1;
        unsigned :1;
        unsigned INT1P :1;
        unsigned INT2P :1;
    };
} INTCON3bits_t;
extern volatile INTCON3bits_t INTCON3bits __attribute__((address(0xFF0)));
# 6136 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char INTCON2 __attribute__((address(0xFF1)));

__asm("INTCON2 equ 0FF1h");


typedef union {
    struct {
        unsigned :7;
        unsigned NOT_RBPU :1;
    };
    struct {
        unsigned RBIP :1;
        unsigned :1;
        unsigned TMR0IP :1;
        unsigned :1;
        unsigned INTEDG2 :1;
        unsigned INTEDG1 :1;
        unsigned INTEDG0 :1;
        unsigned nRBPU :1;
    };
    struct {
        unsigned :7;
        unsigned RBPU :1;
    };
} INTCON2bits_t;
extern volatile INTCON2bits_t INTCON2bits __attribute__((address(0xFF1)));
# 6206 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned char INTCON __attribute__((address(0xFF2)));

__asm("INTCON equ 0FF2h");


typedef union {
    struct {
        unsigned RBIF :1;
        unsigned INT0IF :1;
        unsigned TMR0IF :1;
        unsigned RBIE :1;
        unsigned INT0IE :1;
        unsigned TMR0IE :1;
        unsigned PEIE_GIEL :1;
        unsigned GIE_GIEH :1;
    };
    struct {
        unsigned :1;
        unsigned INT0F :1;
        unsigned T0IF :1;
        unsigned :1;
        unsigned INT0E :1;
        unsigned T0IE :1;
        unsigned PEIE :1;
        unsigned GIE :1;
    };
    struct {
        unsigned :6;
        unsigned GIEL :1;
        unsigned GIEH :1;
    };
} INTCONbits_t;
extern volatile INTCONbits_t INTCONbits __attribute__((address(0xFF2)));
# 6323 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile unsigned short PROD __attribute__((address(0xFF3)));

__asm("PROD equ 0FF3h");




extern volatile unsigned char PRODL __attribute__((address(0xFF3)));

__asm("PRODL equ 0FF3h");




extern volatile unsigned char PRODH __attribute__((address(0xFF4)));

__asm("PRODH equ 0FF4h");




extern volatile unsigned char TABLAT __attribute__((address(0xFF5)));

__asm("TABLAT equ 0FF5h");





extern volatile __uint24 TBLPTR __attribute__((address(0xFF6)));


__asm("TBLPTR equ 0FF6h");




extern volatile unsigned char TBLPTRL __attribute__((address(0xFF6)));

__asm("TBLPTRL equ 0FF6h");




extern volatile unsigned char TBLPTRH __attribute__((address(0xFF7)));

__asm("TBLPTRH equ 0FF7h");




extern volatile unsigned char TBLPTRU __attribute__((address(0xFF8)));

__asm("TBLPTRU equ 0FF8h");





extern volatile __uint24 PCLAT __attribute__((address(0xFF9)));


__asm("PCLAT equ 0FF9h");



extern volatile __uint24 PC __attribute__((address(0xFF9)));


__asm("PC equ 0FF9h");




extern volatile unsigned char PCL __attribute__((address(0xFF9)));

__asm("PCL equ 0FF9h");




extern volatile unsigned char PCLATH __attribute__((address(0xFFA)));

__asm("PCLATH equ 0FFAh");




extern volatile unsigned char PCLATU __attribute__((address(0xFFB)));

__asm("PCLATU equ 0FFBh");




extern volatile unsigned char STKPTR __attribute__((address(0xFFC)));

__asm("STKPTR equ 0FFCh");


typedef union {
    struct {
        unsigned STKPTR :5;
        unsigned :1;
        unsigned STKUNF :1;
        unsigned STKFUL :1;
    };
    struct {
        unsigned SP0 :1;
        unsigned SP1 :1;
        unsigned SP2 :1;
        unsigned SP3 :1;
        unsigned SP4 :1;
        unsigned :2;
        unsigned STKOVF :1;
    };
} STKPTRbits_t;
extern volatile STKPTRbits_t STKPTRbits __attribute__((address(0xFFC)));
# 6491 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile __uint24 TOS __attribute__((address(0xFFD)));


__asm("TOS equ 0FFDh");




extern volatile unsigned char TOSL __attribute__((address(0xFFD)));

__asm("TOSL equ 0FFDh");




extern volatile unsigned char TOSH __attribute__((address(0xFFE)));

__asm("TOSH equ 0FFEh");




extern volatile unsigned char TOSU __attribute__((address(0xFFF)));

__asm("TOSU equ 0FFFh");
# 6526 "/opt/microchip/xc8/v2.20/pic/include/proc/pic18f26k20.h" 3
extern volatile __bit ABDEN __attribute__((address(0x7DC0)));


extern volatile __bit ABDOVF __attribute__((address(0x7DC7)));


extern volatile __bit ACKDT __attribute__((address(0x7E2D)));


extern volatile __bit ACKEN __attribute__((address(0x7E2C)));


extern volatile __bit ACKSTAT __attribute__((address(0x7E2E)));


extern volatile __bit ACQT0 __attribute__((address(0x7E03)));


extern volatile __bit ACQT1 __attribute__((address(0x7E04)));


extern volatile __bit ACQT2 __attribute__((address(0x7E05)));


extern volatile __bit ADCS0 __attribute__((address(0x7E00)));


extern volatile __bit ADCS1 __attribute__((address(0x7E01)));


extern volatile __bit ADCS2 __attribute__((address(0x7E02)));


extern volatile __bit ADDEN __attribute__((address(0x7D5B)));


extern volatile __bit ADEN __attribute__((address(0x7D5B)));


extern volatile __bit ADFM __attribute__((address(0x7E07)));


extern volatile __bit ADIE __attribute__((address(0x7CEE)));


extern volatile __bit ADIF __attribute__((address(0x7CF6)));


extern volatile __bit ADIP __attribute__((address(0x7CFE)));


extern volatile __bit ADON __attribute__((address(0x7E10)));


extern volatile __bit AN0 __attribute__((address(0x7C00)));


extern volatile __bit AN1 __attribute__((address(0x7C01)));


extern volatile __bit AN10 __attribute__((address(0x7C09)));


extern volatile __bit AN11 __attribute__((address(0x7C0C)));


extern volatile __bit AN12 __attribute__((address(0x7C08)));


extern volatile __bit AN2 __attribute__((address(0x7C02)));


extern volatile __bit AN3 __attribute__((address(0x7C03)));


extern volatile __bit AN4 __attribute__((address(0x7C05)));


extern volatile __bit AN8 __attribute__((address(0x7C0A)));


extern volatile __bit AN9 __attribute__((address(0x7C0B)));


extern volatile __bit ANS0 __attribute__((address(0x7BF0)));


extern volatile __bit ANS1 __attribute__((address(0x7BF1)));


extern volatile __bit ANS10 __attribute__((address(0x7BFA)));


extern volatile __bit ANS11 __attribute__((address(0x7BFB)));


extern volatile __bit ANS12 __attribute__((address(0x7BFC)));


extern volatile __bit ANS2 __attribute__((address(0x7BF2)));


extern volatile __bit ANS3 __attribute__((address(0x7BF3)));


extern volatile __bit ANS4 __attribute__((address(0x7BF4)));


extern volatile __bit ANS8 __attribute__((address(0x7BF8)));


extern volatile __bit ANS9 __attribute__((address(0x7BF9)));


extern volatile __bit BCLIE __attribute__((address(0x7D03)));


extern volatile __bit BCLIF __attribute__((address(0x7D0B)));


extern volatile __bit BCLIP __attribute__((address(0x7D13)));


extern volatile __bit BF __attribute__((address(0x7E38)));


extern volatile __bit BGST __attribute__((address(0x7E95)));


extern volatile __bit BOR __attribute__((address(0x7E80)));


extern volatile __bit BRG16 __attribute__((address(0x7DC3)));


extern volatile __bit BRGH __attribute__((address(0x7D62)));


extern volatile __bit BRGH1 __attribute__((address(0x7D62)));


extern volatile __bit C12IN0M __attribute__((address(0x7C00)));


extern volatile __bit C12IN0N __attribute__((address(0x7C00)));


extern volatile __bit C12IN1M __attribute__((address(0x7C01)));


extern volatile __bit C12IN1N __attribute__((address(0x7C01)));


extern volatile __bit C12IN2M __attribute__((address(0x7C0B)));


extern volatile __bit C12IN2N __attribute__((address(0x7C0B)));


extern volatile __bit C12IN3M __attribute__((address(0x7C09)));


extern volatile __bit C12IN3N __attribute__((address(0x7C09)));


extern volatile __bit C1CH0 __attribute__((address(0x7BD8)));


extern volatile __bit C1CH1 __attribute__((address(0x7BD9)));


extern volatile __bit C1IE __attribute__((address(0x7D06)));


extern volatile __bit C1IF __attribute__((address(0x7D0E)));


extern volatile __bit C1INP __attribute__((address(0x7C03)));


extern volatile __bit C1IP __attribute__((address(0x7D16)));


extern volatile __bit C1OE __attribute__((address(0x7BDD)));


extern volatile __bit C1ON __attribute__((address(0x7BDF)));


extern volatile __bit __attribute__((__deprecated__)) C1OUT __attribute__((address(0x7BDE)));


extern volatile __bit C1POL __attribute__((address(0x7BDC)));


extern volatile __bit C1R __attribute__((address(0x7BDA)));


extern volatile __bit C1RSEL __attribute__((address(0x7BCD)));


extern volatile __bit C1SP __attribute__((address(0x7BDB)));


extern volatile __bit C2CH0 __attribute__((address(0x7BD0)));


extern volatile __bit C2CH1 __attribute__((address(0x7BD1)));


extern volatile __bit C2IE __attribute__((address(0x7D05)));


extern volatile __bit C2IF __attribute__((address(0x7D0D)));


extern volatile __bit C2INP __attribute__((address(0x7C02)));


extern volatile __bit C2IP __attribute__((address(0x7D15)));


extern volatile __bit C2OE __attribute__((address(0x7BD5)));


extern volatile __bit C2ON __attribute__((address(0x7BD7)));


extern volatile __bit __attribute__((__deprecated__)) C2OUT __attribute__((address(0x7BD6)));


extern volatile __bit C2POL __attribute__((address(0x7BD4)));


extern volatile __bit C2R __attribute__((address(0x7BD2)));


extern volatile __bit C2RSEL __attribute__((address(0x7BCC)));


extern volatile __bit C2SP __attribute__((address(0x7BD3)));


extern volatile __bit CARRY __attribute__((address(0x7EC0)));


extern volatile __bit CCP1 __attribute__((address(0x7C12)));


extern volatile __bit CCP1IE __attribute__((address(0x7CEA)));


extern volatile __bit CCP1IF __attribute__((address(0x7CF2)));


extern volatile __bit CCP1IP __attribute__((address(0x7CFA)));


extern volatile __bit CCP1M0 __attribute__((address(0x7DE8)));


extern volatile __bit CCP1M1 __attribute__((address(0x7DE9)));


extern volatile __bit CCP1M2 __attribute__((address(0x7DEA)));


extern volatile __bit CCP1M3 __attribute__((address(0x7DEB)));


extern volatile __bit CCP2IE __attribute__((address(0x7D00)));


extern volatile __bit CCP2IF __attribute__((address(0x7D08)));


extern volatile __bit CCP2IP __attribute__((address(0x7D10)));


extern volatile __bit CCP2M0 __attribute__((address(0x7DD0)));


extern volatile __bit CCP2M1 __attribute__((address(0x7DD1)));


extern volatile __bit CCP2M2 __attribute__((address(0x7DD2)));


extern volatile __bit CCP2M3 __attribute__((address(0x7DD3)));


extern volatile __bit CCP2_PA2 __attribute__((address(0x7C0B)));


extern volatile __bit CCP9E __attribute__((address(0x7C23)));


extern volatile __bit CFGS __attribute__((address(0x7D36)));


extern volatile __bit CHS0 __attribute__((address(0x7E12)));


extern volatile __bit CHS1 __attribute__((address(0x7E13)));


extern volatile __bit CHS2 __attribute__((address(0x7E14)));


extern volatile __bit CHS3 __attribute__((address(0x7E15)));


extern volatile __bit CK __attribute__((address(0x7C16)));


extern volatile __bit CKE __attribute__((address(0x7E3E)));


extern volatile __bit CKP __attribute__((address(0x7E34)));


extern volatile __bit CKTXP __attribute__((address(0x7DC4)));


extern volatile __bit CMIE __attribute__((address(0x7D06)));


extern volatile __bit CMIF __attribute__((address(0x7D0E)));


extern volatile __bit CMIP __attribute__((address(0x7D16)));


extern volatile __bit CREN __attribute__((address(0x7D5C)));


extern volatile __bit CSRC __attribute__((address(0x7D67)));


extern volatile __bit CSRC1 __attribute__((address(0x7D67)));


extern volatile __bit CVR0 __attribute__((address(0x7DA8)));


extern volatile __bit CVR1 __attribute__((address(0x7DA9)));


extern volatile __bit CVR2 __attribute__((address(0x7DAA)));


extern volatile __bit CVR3 __attribute__((address(0x7DAB)));


extern volatile __bit CVREF __attribute__((address(0x7C02)));


extern volatile __bit CVREN __attribute__((address(0x7DAF)));


extern volatile __bit CVROE __attribute__((address(0x7DAE)));


extern volatile __bit CVROEN __attribute__((address(0x7DAE)));


extern volatile __bit CVRR __attribute__((address(0x7DAD)));


extern volatile __bit CVRSS __attribute__((address(0x7DAC)));


extern volatile __bit DA __attribute__((address(0x7E3D)));


extern volatile __bit DC __attribute__((address(0x7EC1)));


extern volatile __bit DC1B0 __attribute__((address(0x7DEC)));


extern volatile __bit DC1B1 __attribute__((address(0x7DED)));


extern volatile __bit DC2B0 __attribute__((address(0x7DD4)));


extern volatile __bit DC2B1 __attribute__((address(0x7DD5)));


extern volatile __bit DONE __attribute__((address(0x7E11)));


extern volatile __bit DT __attribute__((address(0x7C17)));


extern volatile __bit DTRXP __attribute__((address(0x7DC5)));


extern volatile __bit D_A __attribute__((address(0x7E3D)));


extern volatile __bit D_NOT_A __attribute__((address(0x7E3D)));


extern volatile __bit D_nA __attribute__((address(0x7E3D)));


extern volatile __bit EBDIS __attribute__((address(0x7E5F)));


extern volatile __bit ECCPAS0 __attribute__((address(0x7DB4)));


extern volatile __bit ECCPAS1 __attribute__((address(0x7DB5)));


extern volatile __bit ECCPAS2 __attribute__((address(0x7DB6)));


extern volatile __bit ECCPASE __attribute__((address(0x7DB7)));


extern volatile __bit EEADR0 __attribute__((address(0x7D48)));


extern volatile __bit EEADR1 __attribute__((address(0x7D49)));


extern volatile __bit EEADR2 __attribute__((address(0x7D4A)));


extern volatile __bit EEADR3 __attribute__((address(0x7D4B)));


extern volatile __bit EEADR4 __attribute__((address(0x7D4C)));


extern volatile __bit EEADR5 __attribute__((address(0x7D4D)));


extern volatile __bit EEADR6 __attribute__((address(0x7D4E)));


extern volatile __bit EEADR7 __attribute__((address(0x7D4F)));


extern volatile __bit EEADR8 __attribute__((address(0x7D50)));


extern volatile __bit EEADR9 __attribute__((address(0x7D51)));


extern volatile __bit EEFS __attribute__((address(0x7D36)));


extern volatile __bit EEIE __attribute__((address(0x7D04)));


extern volatile __bit EEIF __attribute__((address(0x7D0C)));


extern volatile __bit EEIP __attribute__((address(0x7D14)));


extern volatile __bit EEPGD __attribute__((address(0x7D37)));


extern volatile __bit FERR __attribute__((address(0x7D5A)));


extern volatile __bit FLT0 __attribute__((address(0x7C08)));


extern volatile __bit FREE __attribute__((address(0x7D34)));


extern volatile __bit FVREN __attribute__((address(0x7DA7)));


extern volatile __bit FVRST __attribute__((address(0x7DA6)));


extern volatile __bit GCEN __attribute__((address(0x7E2F)));


extern volatile __bit GIE __attribute__((address(0x7F97)));


extern volatile __bit GIEH __attribute__((address(0x7F97)));


extern volatile __bit GIEL __attribute__((address(0x7F96)));


extern volatile __bit GIE_GIEH __attribute__((address(0x7F97)));


extern volatile __bit GO __attribute__((address(0x7E11)));


extern volatile __bit GODONE __attribute__((address(0x7E11)));


extern volatile __bit GO_DONE __attribute__((address(0x7E11)));


extern volatile __bit GO_NOT_DONE __attribute__((address(0x7E11)));


extern volatile __bit GO_nDONE __attribute__((address(0x7E11)));


extern volatile __bit HLVDEN __attribute__((address(0x7E94)));


extern volatile __bit HLVDIE __attribute__((address(0x7D02)));


extern volatile __bit HLVDIF __attribute__((address(0x7D0A)));


extern volatile __bit HLVDIN __attribute__((address(0x7C05)));


extern volatile __bit HLVDIP __attribute__((address(0x7D12)));


extern volatile __bit HLVDL0 __attribute__((address(0x7E90)));


extern volatile __bit HLVDL1 __attribute__((address(0x7E91)));


extern volatile __bit HLVDL2 __attribute__((address(0x7E92)));


extern volatile __bit HLVDL3 __attribute__((address(0x7E93)));


extern volatile __bit IDLEN __attribute__((address(0x7E9F)));


extern volatile __bit INT0 __attribute__((address(0x7C08)));


extern volatile __bit INT0E __attribute__((address(0x7F94)));


extern volatile __bit INT0F __attribute__((address(0x7F91)));


extern volatile __bit INT0IE __attribute__((address(0x7F94)));


extern volatile __bit INT0IF __attribute__((address(0x7F91)));


extern volatile __bit INT1 __attribute__((address(0x7C09)));


extern volatile __bit INT1E __attribute__((address(0x7F83)));


extern volatile __bit INT1F __attribute__((address(0x7F80)));


extern volatile __bit INT1IE __attribute__((address(0x7F83)));


extern volatile __bit INT1IF __attribute__((address(0x7F80)));


extern volatile __bit INT1IP __attribute__((address(0x7F86)));


extern volatile __bit INT1P __attribute__((address(0x7F86)));


extern volatile __bit INT2 __attribute__((address(0x7C0A)));


extern volatile __bit INT2E __attribute__((address(0x7F84)));


extern volatile __bit INT2F __attribute__((address(0x7F81)));


extern volatile __bit INT2IE __attribute__((address(0x7F84)));


extern volatile __bit INT2IF __attribute__((address(0x7F81)));


extern volatile __bit INT2IP __attribute__((address(0x7F87)));


extern volatile __bit INT2P __attribute__((address(0x7F87)));


extern volatile __bit INTEDG0 __attribute__((address(0x7F8E)));


extern volatile __bit INTEDG1 __attribute__((address(0x7F8D)));


extern volatile __bit INTEDG2 __attribute__((address(0x7F8C)));


extern volatile __bit INTSRC __attribute__((address(0x7CDF)));


extern volatile __bit IOCB4 __attribute__((address(0x7BEC)));


extern volatile __bit IOCB5 __attribute__((address(0x7BED)));


extern volatile __bit IOCB6 __attribute__((address(0x7BEE)));


extern volatile __bit IOCB7 __attribute__((address(0x7BEF)));


extern volatile __bit IOFS __attribute__((address(0x7E9A)));


extern volatile __bit IPEN __attribute__((address(0x7E87)));


extern volatile __bit IRCF0 __attribute__((address(0x7E9C)));


extern volatile __bit IRCF1 __attribute__((address(0x7E9D)));


extern volatile __bit IRCF2 __attribute__((address(0x7E9E)));


extern volatile __bit IRVST __attribute__((address(0x7E95)));


extern volatile __bit IVRST __attribute__((address(0x7E95)));


extern volatile __bit KBI0 __attribute__((address(0x7C0C)));


extern volatile __bit KBI1 __attribute__((address(0x7C0D)));


extern volatile __bit KBI2 __attribute__((address(0x7C0E)));


extern volatile __bit KBI3 __attribute__((address(0x7C0F)));


extern volatile __bit LA0 __attribute__((address(0x7C48)));


extern volatile __bit LA1 __attribute__((address(0x7C49)));


extern volatile __bit LA2 __attribute__((address(0x7C4A)));


extern volatile __bit LA3 __attribute__((address(0x7C4B)));


extern volatile __bit LA4 __attribute__((address(0x7C4C)));


extern volatile __bit LA5 __attribute__((address(0x7C4D)));


extern volatile __bit LA6 __attribute__((address(0x7C4E)));


extern volatile __bit LA7 __attribute__((address(0x7C4F)));


extern volatile __bit LATA0 __attribute__((address(0x7C48)));


extern volatile __bit LATA1 __attribute__((address(0x7C49)));


extern volatile __bit LATA2 __attribute__((address(0x7C4A)));


extern volatile __bit LATA3 __attribute__((address(0x7C4B)));


extern volatile __bit LATA4 __attribute__((address(0x7C4C)));


extern volatile __bit LATA5 __attribute__((address(0x7C4D)));


extern volatile __bit LATA6 __attribute__((address(0x7C4E)));


extern volatile __bit LATA7 __attribute__((address(0x7C4F)));


extern volatile __bit LATB0 __attribute__((address(0x7C50)));


extern volatile __bit LATB1 __attribute__((address(0x7C51)));


extern volatile __bit LATB2 __attribute__((address(0x7C52)));


extern volatile __bit LATB3 __attribute__((address(0x7C53)));


extern volatile __bit LATB4 __attribute__((address(0x7C54)));


extern volatile __bit LATB5 __attribute__((address(0x7C55)));


extern volatile __bit LATB6 __attribute__((address(0x7C56)));


extern volatile __bit LATB7 __attribute__((address(0x7C57)));


extern volatile __bit LATC0 __attribute__((address(0x7C58)));


extern volatile __bit LATC1 __attribute__((address(0x7C59)));


extern volatile __bit LATC2 __attribute__((address(0x7C5A)));


extern volatile __bit LATC3 __attribute__((address(0x7C5B)));


extern volatile __bit LATC4 __attribute__((address(0x7C5C)));


extern volatile __bit LATC5 __attribute__((address(0x7C5D)));


extern volatile __bit LATC6 __attribute__((address(0x7C5E)));


extern volatile __bit LATC7 __attribute__((address(0x7C5F)));


extern volatile __bit LB0 __attribute__((address(0x7C50)));


extern volatile __bit LB1 __attribute__((address(0x7C51)));


extern volatile __bit LB2 __attribute__((address(0x7C52)));


extern volatile __bit LB3 __attribute__((address(0x7C53)));


extern volatile __bit LB4 __attribute__((address(0x7C54)));


extern volatile __bit LB5 __attribute__((address(0x7C55)));


extern volatile __bit LB6 __attribute__((address(0x7C56)));


extern volatile __bit LB7 __attribute__((address(0x7C57)));


extern volatile __bit LC0 __attribute__((address(0x7C58)));


extern volatile __bit LC1 __attribute__((address(0x7C59)));


extern volatile __bit LC2 __attribute__((address(0x7C5A)));


extern volatile __bit LC3 __attribute__((address(0x7C5B)));


extern volatile __bit LC4 __attribute__((address(0x7C5C)));


extern volatile __bit LC5 __attribute__((address(0x7C5D)));


extern volatile __bit LC6 __attribute__((address(0x7C5E)));


extern volatile __bit LC7 __attribute__((address(0x7C5F)));


extern volatile __bit LVDEN __attribute__((address(0x7E94)));


extern volatile __bit LVDIE __attribute__((address(0x7D02)));


extern volatile __bit LVDIF __attribute__((address(0x7D0A)));


extern volatile __bit LVDIN __attribute__((address(0x7C05)));


extern volatile __bit LVDIP __attribute__((address(0x7D12)));


extern volatile __bit LVDL0 __attribute__((address(0x7E90)));


extern volatile __bit LVDL1 __attribute__((address(0x7E91)));


extern volatile __bit LVDL2 __attribute__((address(0x7E92)));


extern volatile __bit LVDL3 __attribute__((address(0x7E93)));


extern volatile __bit LVV0 __attribute__((address(0x7E90)));


extern volatile __bit LVV1 __attribute__((address(0x7E91)));


extern volatile __bit LVV2 __attribute__((address(0x7E92)));


extern volatile __bit LVV3 __attribute__((address(0x7E93)));


extern volatile __bit MC1OUT __attribute__((address(0x7BCF)));


extern volatile __bit MC2OUT __attribute__((address(0x7BCE)));


extern volatile __bit MCLR __attribute__((address(0x7C23)));


extern volatile __bit MSK0 __attribute__((address(0x7BB8)));


extern volatile __bit MSK1 __attribute__((address(0x7BB9)));


extern volatile __bit MSK2 __attribute__((address(0x7BBA)));


extern volatile __bit MSK3 __attribute__((address(0x7BBB)));


extern volatile __bit MSK4 __attribute__((address(0x7BBC)));


extern volatile __bit MSK5 __attribute__((address(0x7BBD)));


extern volatile __bit MSK6 __attribute__((address(0x7BBE)));


extern volatile __bit MSK7 __attribute__((address(0x7BBF)));


extern volatile __bit NEGATIVE __attribute__((address(0x7EC4)));


extern volatile __bit NOT_A __attribute__((address(0x7E3D)));


extern volatile __bit NOT_ADDRESS __attribute__((address(0x7E3D)));


extern volatile __bit NOT_BOR __attribute__((address(0x7E80)));


extern volatile __bit NOT_DONE __attribute__((address(0x7E11)));


extern volatile __bit NOT_MCLR __attribute__((address(0x7C23)));


extern volatile __bit NOT_PD __attribute__((address(0x7E82)));


extern volatile __bit NOT_POR __attribute__((address(0x7E81)));


extern volatile __bit NOT_RBPU __attribute__((address(0x7F8F)));


extern volatile __bit NOT_RI __attribute__((address(0x7E84)));


extern volatile __bit NOT_SS __attribute__((address(0x7C05)));


extern volatile __bit NOT_T1SYNC __attribute__((address(0x7E6A)));


extern volatile __bit NOT_T3SYNC __attribute__((address(0x7D8A)));


extern volatile __bit NOT_TO __attribute__((address(0x7E83)));


extern volatile __bit NOT_W __attribute__((address(0x7E3A)));


extern volatile __bit NOT_WRITE __attribute__((address(0x7E3A)));


extern volatile __bit OERR __attribute__((address(0x7D59)));


extern volatile __bit OSCFIE __attribute__((address(0x7D07)));


extern volatile __bit OSCFIF __attribute__((address(0x7D0F)));


extern volatile __bit OSCFIP __attribute__((address(0x7D17)));


extern volatile __bit OSTS __attribute__((address(0x7E9B)));


extern volatile __bit OV __attribute__((address(0x7EC3)));


extern volatile __bit OVERFLOW __attribute__((address(0x7EC3)));


extern volatile __bit P1A __attribute__((address(0x7C12)));


extern volatile __bit P1B __attribute__((address(0x7C0A)));


extern volatile __bit P1C __attribute__((address(0x7C09)));


extern volatile __bit P1D __attribute__((address(0x7C0C)));


extern volatile __bit P1M0 __attribute__((address(0x7DEE)));


extern volatile __bit P1M1 __attribute__((address(0x7DEF)));


extern volatile __bit PA1 __attribute__((address(0x7C12)));


extern volatile __bit PA2 __attribute__((address(0x7C11)));


extern volatile __bit PC3E __attribute__((address(0x7C23)));


extern volatile __bit PD __attribute__((address(0x7E82)));


extern volatile __bit PDC0 __attribute__((address(0x7DB8)));


extern volatile __bit PDC1 __attribute__((address(0x7DB9)));


extern volatile __bit PDC2 __attribute__((address(0x7DBA)));


extern volatile __bit PDC3 __attribute__((address(0x7DBB)));


extern volatile __bit PDC4 __attribute__((address(0x7DBC)));


extern volatile __bit PDC5 __attribute__((address(0x7DBD)));


extern volatile __bit PDC6 __attribute__((address(0x7DBE)));


extern volatile __bit PEIE __attribute__((address(0x7F96)));


extern volatile __bit PEIE_GIEL __attribute__((address(0x7F96)));


extern volatile __bit PEN __attribute__((address(0x7E2A)));


extern volatile __bit PGC __attribute__((address(0x7C0E)));


extern volatile __bit PGD __attribute__((address(0x7C0F)));


extern volatile __bit PGM __attribute__((address(0x7C0D)));


extern volatile __bit PLLEN __attribute__((address(0x7CDE)));


extern volatile __bit POR __attribute__((address(0x7E81)));


extern volatile __bit PRSEN __attribute__((address(0x7DBF)));


extern volatile __bit PSA __attribute__((address(0x7EAB)));


extern volatile __bit PSSAC0 __attribute__((address(0x7DB2)));


extern volatile __bit PSSAC1 __attribute__((address(0x7DB3)));


extern volatile __bit PSSBD0 __attribute__((address(0x7DB0)));


extern volatile __bit PSSBD1 __attribute__((address(0x7DB1)));


extern volatile __bit __attribute__((__deprecated__)) RA0 __attribute__((address(0x7C00)));


extern volatile __bit __attribute__((__deprecated__)) RA1 __attribute__((address(0x7C01)));


extern volatile __bit __attribute__((__deprecated__)) RA2 __attribute__((address(0x7C02)));


extern volatile __bit __attribute__((__deprecated__)) RA3 __attribute__((address(0x7C03)));


extern volatile __bit __attribute__((__deprecated__)) RA4 __attribute__((address(0x7C04)));


extern volatile __bit __attribute__((__deprecated__)) RA5 __attribute__((address(0x7C05)));


extern volatile __bit __attribute__((__deprecated__)) RA6 __attribute__((address(0x7C06)));


extern volatile __bit __attribute__((__deprecated__)) RA7 __attribute__((address(0x7C07)));


extern volatile __bit __attribute__((__deprecated__)) RB0 __attribute__((address(0x7C08)));


extern volatile __bit __attribute__((__deprecated__)) RB1 __attribute__((address(0x7C09)));


extern volatile __bit __attribute__((__deprecated__)) RB2 __attribute__((address(0x7C0A)));


extern volatile __bit __attribute__((__deprecated__)) RB3 __attribute__((address(0x7C0B)));


extern volatile __bit __attribute__((__deprecated__)) RB4 __attribute__((address(0x7C0C)));


extern volatile __bit __attribute__((__deprecated__)) RB5 __attribute__((address(0x7C0D)));


extern volatile __bit __attribute__((__deprecated__)) RB6 __attribute__((address(0x7C0E)));


extern volatile __bit __attribute__((__deprecated__)) RB7 __attribute__((address(0x7C0F)));


extern volatile __bit RBIE __attribute__((address(0x7F93)));


extern volatile __bit RBIF __attribute__((address(0x7F90)));


extern volatile __bit RBIP __attribute__((address(0x7F88)));


extern volatile __bit RBPU __attribute__((address(0x7F8F)));


extern volatile __bit __attribute__((__deprecated__)) RC0 __attribute__((address(0x7C10)));


extern volatile __bit __attribute__((__deprecated__)) RC1 __attribute__((address(0x7C11)));


extern volatile __bit RC1IE __attribute__((address(0x7CED)));


extern volatile __bit RC1IF __attribute__((address(0x7CF5)));


extern volatile __bit RC1IP __attribute__((address(0x7CFD)));


extern volatile __bit __attribute__((__deprecated__)) RC2 __attribute__((address(0x7C12)));


extern volatile __bit __attribute__((__deprecated__)) RC3 __attribute__((address(0x7C13)));


extern volatile __bit __attribute__((__deprecated__)) RC4 __attribute__((address(0x7C14)));


extern volatile __bit __attribute__((__deprecated__)) RC5 __attribute__((address(0x7C15)));


extern volatile __bit __attribute__((__deprecated__)) RC6 __attribute__((address(0x7C16)));


extern volatile __bit __attribute__((__deprecated__)) RC7 __attribute__((address(0x7C17)));


extern volatile __bit RC8_9 __attribute__((address(0x7D5E)));


extern volatile __bit RC9 __attribute__((address(0x7D5E)));


extern volatile __bit RCD8 __attribute__((address(0x7D58)));


extern volatile __bit RCEN __attribute__((address(0x7E2B)));


extern volatile __bit RCIDL __attribute__((address(0x7DC6)));


extern volatile __bit RCIE __attribute__((address(0x7CED)));


extern volatile __bit RCIF __attribute__((address(0x7CF5)));


extern volatile __bit RCIP __attribute__((address(0x7CFD)));


extern volatile __bit RD __attribute__((address(0x7D30)));


extern volatile __bit RD163 __attribute__((address(0x7D8F)));


extern volatile __bit RE3 __attribute__((address(0x7C23)));


extern volatile __bit RI __attribute__((address(0x7E84)));


extern volatile __bit RJPU __attribute__((address(0x7C07)));


extern volatile __bit RSEN __attribute__((address(0x7E29)));


extern volatile __bit RW __attribute__((address(0x7E3A)));


extern volatile __bit RX __attribute__((address(0x7C17)));


extern volatile __bit RX9 __attribute__((address(0x7D5E)));


extern volatile __bit RX9D __attribute__((address(0x7D58)));


extern volatile __bit RXCKP __attribute__((address(0x7DC5)));


extern volatile __bit R_NOT_W __attribute__((address(0x7E3A)));


extern volatile __bit R_W __attribute__((address(0x7E3A)));


extern volatile __bit R_nW __attribute__((address(0x7E3A)));


extern volatile __bit SBOREN __attribute__((address(0x7E86)));


extern volatile __bit SCK __attribute__((address(0x7C13)));


extern volatile __bit SCKP __attribute__((address(0x7DC4)));


extern volatile __bit SCL __attribute__((address(0x7C13)));


extern volatile __bit SCS0 __attribute__((address(0x7E98)));


extern volatile __bit SCS1 __attribute__((address(0x7E99)));


extern volatile __bit SDA __attribute__((address(0x7C14)));


extern volatile __bit SDI __attribute__((address(0x7C14)));


extern volatile __bit SDO __attribute__((address(0x7C15)));


extern volatile __bit SEN __attribute__((address(0x7E28)));


extern volatile __bit SENDB __attribute__((address(0x7D63)));


extern volatile __bit SENDB1 __attribute__((address(0x7D63)));


extern volatile __bit SLRA __attribute__((address(0x7BC0)));


extern volatile __bit SLRB __attribute__((address(0x7BC1)));


extern volatile __bit SLRC __attribute__((address(0x7BC2)));


extern volatile __bit SMP __attribute__((address(0x7E3F)));


extern volatile __bit SOSCEN __attribute__((address(0x7E6B)));


extern volatile __bit SOSCEN3 __attribute__((address(0x7D8B)));


extern volatile __bit SP0 __attribute__((address(0x7FE0)));


extern volatile __bit SP1 __attribute__((address(0x7FE1)));


extern volatile __bit SP2 __attribute__((address(0x7FE2)));


extern volatile __bit SP3 __attribute__((address(0x7FE3)));


extern volatile __bit SP4 __attribute__((address(0x7FE4)));


extern volatile __bit SPEN __attribute__((address(0x7D5F)));


extern volatile __bit SREN __attribute__((address(0x7D5D)));


extern volatile __bit SRENA __attribute__((address(0x7D5D)));


extern volatile __bit SS __attribute__((address(0x7C05)));


extern volatile __bit SSPEN __attribute__((address(0x7E35)));


extern volatile __bit SSPIE __attribute__((address(0x7CEB)));


extern volatile __bit SSPIF __attribute__((address(0x7CF3)));


extern volatile __bit SSPIP __attribute__((address(0x7CFB)));


extern volatile __bit SSPM0 __attribute__((address(0x7E30)));


extern volatile __bit SSPM1 __attribute__((address(0x7E31)));


extern volatile __bit SSPM2 __attribute__((address(0x7E32)));


extern volatile __bit SSPM3 __attribute__((address(0x7E33)));


extern volatile __bit SSPOV __attribute__((address(0x7E36)));


extern volatile __bit START __attribute__((address(0x7E3B)));


extern volatile __bit STKFUL __attribute__((address(0x7FE7)));


extern volatile __bit STKOVF __attribute__((address(0x7FE7)));


extern volatile __bit STKUNF __attribute__((address(0x7FE6)));


extern volatile __bit STOP __attribute__((address(0x7E3C)));


extern volatile __bit STRA __attribute__((address(0x7DC8)));


extern volatile __bit STRB __attribute__((address(0x7DC9)));


extern volatile __bit STRC __attribute__((address(0x7DCA)));


extern volatile __bit STRD __attribute__((address(0x7DCB)));


extern volatile __bit STRSYNC __attribute__((address(0x7DCC)));


extern volatile __bit SWDTE __attribute__((address(0x7E88)));


extern volatile __bit SWDTEN __attribute__((address(0x7E88)));


extern volatile __bit SYNC __attribute__((address(0x7D64)));


extern volatile __bit SYNC1 __attribute__((address(0x7D64)));


extern volatile __bit T08BIT __attribute__((address(0x7EAE)));


extern volatile __bit T0CKI __attribute__((address(0x7C04)));


extern volatile __bit T0CS __attribute__((address(0x7EAD)));


extern volatile __bit T0IE __attribute__((address(0x7F95)));


extern volatile __bit T0IF __attribute__((address(0x7F92)));


extern volatile __bit T0PS0 __attribute__((address(0x7EA8)));


extern volatile __bit T0PS1 __attribute__((address(0x7EA9)));


extern volatile __bit T0PS2 __attribute__((address(0x7EAA)));


extern volatile __bit T0SE __attribute__((address(0x7EAC)));


extern volatile __bit T13CKI __attribute__((address(0x7C10)));


extern volatile __bit T1CKI __attribute__((address(0x7C10)));


extern volatile __bit T1CKPS0 __attribute__((address(0x7E6C)));


extern volatile __bit T1CKPS1 __attribute__((address(0x7E6D)));


extern volatile __bit T1OSCEN __attribute__((address(0x7E6B)));


extern volatile __bit T1OSI __attribute__((address(0x7C11)));


extern volatile __bit T1OSO __attribute__((address(0x7C10)));


extern volatile __bit T1RD16 __attribute__((address(0x7E6F)));


extern volatile __bit T1RUN __attribute__((address(0x7E6E)));


extern volatile __bit T1SYNC __attribute__((address(0x7E6A)));


extern volatile __bit T2CKPS0 __attribute__((address(0x7E50)));


extern volatile __bit T2CKPS1 __attribute__((address(0x7E51)));


extern volatile __bit T2OUTPS0 __attribute__((address(0x7E53)));


extern volatile __bit T2OUTPS1 __attribute__((address(0x7E54)));


extern volatile __bit T2OUTPS2 __attribute__((address(0x7E55)));


extern volatile __bit T2OUTPS3 __attribute__((address(0x7E56)));


extern volatile __bit T3CCP1 __attribute__((address(0x7D8B)));


extern volatile __bit T3CCP2 __attribute__((address(0x7D8E)));


extern volatile __bit T3CKI __attribute__((address(0x7C10)));


extern volatile __bit T3CKPS0 __attribute__((address(0x7D8C)));


extern volatile __bit T3CKPS1 __attribute__((address(0x7D8D)));


extern volatile __bit T3RD16 __attribute__((address(0x7D8F)));


extern volatile __bit T3SYNC __attribute__((address(0x7D8A)));


extern volatile __bit TMR0IE __attribute__((address(0x7F95)));


extern volatile __bit TMR0IF __attribute__((address(0x7F92)));


extern volatile __bit TMR0IP __attribute__((address(0x7F8A)));


extern volatile __bit TMR0ON __attribute__((address(0x7EAF)));


extern volatile __bit TMR1CS __attribute__((address(0x7E69)));


extern volatile __bit TMR1IE __attribute__((address(0x7CE8)));


extern volatile __bit TMR1IF __attribute__((address(0x7CF0)));


extern volatile __bit TMR1IP __attribute__((address(0x7CF8)));


extern volatile __bit TMR1ON __attribute__((address(0x7E68)));


extern volatile __bit TMR2IE __attribute__((address(0x7CE9)));


extern volatile __bit TMR2IF __attribute__((address(0x7CF1)));


extern volatile __bit TMR2IP __attribute__((address(0x7CF9)));


extern volatile __bit TMR2ON __attribute__((address(0x7E52)));


extern volatile __bit TMR3CS __attribute__((address(0x7D89)));


extern volatile __bit TMR3IE __attribute__((address(0x7D01)));


extern volatile __bit TMR3IF __attribute__((address(0x7D09)));


extern volatile __bit TMR3IP __attribute__((address(0x7D11)));


extern volatile __bit TMR3ON __attribute__((address(0x7D88)));


extern volatile __bit TO __attribute__((address(0x7E83)));


extern volatile __bit TRISA0 __attribute__((address(0x7C90)));


extern volatile __bit TRISA1 __attribute__((address(0x7C91)));


extern volatile __bit TRISA2 __attribute__((address(0x7C92)));


extern volatile __bit TRISA3 __attribute__((address(0x7C93)));


extern volatile __bit TRISA4 __attribute__((address(0x7C94)));


extern volatile __bit TRISA5 __attribute__((address(0x7C95)));


extern volatile __bit TRISA6 __attribute__((address(0x7C96)));


extern volatile __bit TRISA7 __attribute__((address(0x7C97)));


extern volatile __bit TRISB0 __attribute__((address(0x7C98)));


extern volatile __bit TRISB1 __attribute__((address(0x7C99)));


extern volatile __bit TRISB2 __attribute__((address(0x7C9A)));


extern volatile __bit TRISB3 __attribute__((address(0x7C9B)));


extern volatile __bit TRISB4 __attribute__((address(0x7C9C)));


extern volatile __bit TRISB5 __attribute__((address(0x7C9D)));


extern volatile __bit TRISB6 __attribute__((address(0x7C9E)));


extern volatile __bit TRISB7 __attribute__((address(0x7C9F)));


extern volatile __bit TRISC0 __attribute__((address(0x7CA0)));


extern volatile __bit TRISC1 __attribute__((address(0x7CA1)));


extern volatile __bit TRISC2 __attribute__((address(0x7CA2)));


extern volatile __bit TRISC3 __attribute__((address(0x7CA3)));


extern volatile __bit TRISC4 __attribute__((address(0x7CA4)));


extern volatile __bit TRISC5 __attribute__((address(0x7CA5)));


extern volatile __bit TRISC6 __attribute__((address(0x7CA6)));


extern volatile __bit TRISC7 __attribute__((address(0x7CA7)));


extern volatile __bit TRMT __attribute__((address(0x7D61)));


extern volatile __bit TRMT1 __attribute__((address(0x7D61)));


extern volatile __bit TUN0 __attribute__((address(0x7CD8)));


extern volatile __bit TUN1 __attribute__((address(0x7CD9)));


extern volatile __bit TUN2 __attribute__((address(0x7CDA)));


extern volatile __bit TUN3 __attribute__((address(0x7CDB)));


extern volatile __bit TUN4 __attribute__((address(0x7CDC)));


extern volatile __bit TUN5 __attribute__((address(0x7CDD)));


extern volatile __bit TX __attribute__((address(0x7C16)));


extern volatile __bit TX1IE __attribute__((address(0x7CEC)));


extern volatile __bit TX1IF __attribute__((address(0x7CF4)));


extern volatile __bit TX1IP __attribute__((address(0x7CFC)));


extern volatile __bit TX8_9 __attribute__((address(0x7D66)));


extern volatile __bit TX9 __attribute__((address(0x7D66)));


extern volatile __bit TX91 __attribute__((address(0x7D66)));


extern volatile __bit TX9D __attribute__((address(0x7D60)));


extern volatile __bit TX9D1 __attribute__((address(0x7D60)));


extern volatile __bit TXD8 __attribute__((address(0x7D60)));


extern volatile __bit TXEN __attribute__((address(0x7D65)));


extern volatile __bit TXEN1 __attribute__((address(0x7D65)));


extern volatile __bit TXIE __attribute__((address(0x7CEC)));


extern volatile __bit TXIF __attribute__((address(0x7CF4)));


extern volatile __bit TXIP __attribute__((address(0x7CFC)));


extern volatile __bit UA __attribute__((address(0x7E39)));


extern volatile __bit ULPWUIN __attribute__((address(0x7C00)));


extern volatile __bit VCFG0 __attribute__((address(0x7E0C)));


extern volatile __bit VCFG01 __attribute__((address(0x7E0C)));


extern volatile __bit VCFG1 __attribute__((address(0x7E0D)));


extern volatile __bit VCFG11 __attribute__((address(0x7E0D)));


extern volatile __bit VDIRMAG __attribute__((address(0x7E97)));


extern volatile __bit VPP __attribute__((address(0x7C23)));


extern volatile __bit VREFM __attribute__((address(0x7C02)));


extern volatile __bit VREFN __attribute__((address(0x7C02)));


extern volatile __bit VREFP __attribute__((address(0x7C03)));


extern volatile __bit W4E __attribute__((address(0x7DC1)));


extern volatile __bit WAIT0 __attribute__((address(0x7E5C)));


extern volatile __bit WAIT1 __attribute__((address(0x7E5D)));


extern volatile __bit WCOL __attribute__((address(0x7E37)));


extern volatile __bit WM0 __attribute__((address(0x7E58)));


extern volatile __bit WM1 __attribute__((address(0x7E59)));


extern volatile __bit WPUB0 __attribute__((address(0x7BE0)));


extern volatile __bit WPUB1 __attribute__((address(0x7BE1)));


extern volatile __bit WPUB2 __attribute__((address(0x7BE2)));


extern volatile __bit WPUB3 __attribute__((address(0x7BE3)));


extern volatile __bit WPUB4 __attribute__((address(0x7BE4)));


extern volatile __bit WPUB5 __attribute__((address(0x7BE5)));


extern volatile __bit WPUB6 __attribute__((address(0x7BE6)));


extern volatile __bit WPUB7 __attribute__((address(0x7BE7)));


extern volatile __bit WR __attribute__((address(0x7D31)));


extern volatile __bit WREN __attribute__((address(0x7D32)));


extern volatile __bit WRERR __attribute__((address(0x7D33)));


extern volatile __bit WUE __attribute__((address(0x7DC1)));


extern volatile __bit ZERO __attribute__((address(0x7EC2)));


extern volatile __bit nA __attribute__((address(0x7E3D)));


extern volatile __bit nADDRESS __attribute__((address(0x7E3D)));


extern volatile __bit nBOR __attribute__((address(0x7E80)));


extern volatile __bit nDONE __attribute__((address(0x7E11)));


extern volatile __bit nMCLR __attribute__((address(0x7C23)));


extern volatile __bit nPD __attribute__((address(0x7E82)));


extern volatile __bit nPOR __attribute__((address(0x7E81)));


extern volatile __bit nRBPU __attribute__((address(0x7F8F)));


extern volatile __bit nRI __attribute__((address(0x7E84)));


extern volatile __bit nSS __attribute__((address(0x7C05)));


extern volatile __bit nT1SYNC __attribute__((address(0x7E6A)));


extern volatile __bit nT3SYNC __attribute__((address(0x7D8A)));


extern volatile __bit nTO __attribute__((address(0x7E83)));


extern volatile __bit nW __attribute__((address(0x7E3A)));


extern volatile __bit nWRITE __attribute__((address(0x7E3A)));
# 425 "/opt/microchip/xc8/v2.20/pic/include/pic18_chip_select.h" 2 3
# 9 "/opt/microchip/xc8/v2.20/pic/include/pic18.h" 2 3
# 18 "/opt/microchip/xc8/v2.20/pic/include/pic18.h" 3
__attribute__((__unsupported__("The " "flash_write" " routine is no longer supported. Please use the MPLAB X MCC."))) void flash_write(const unsigned char *, unsigned int, __far unsigned char *);
__attribute__((__unsupported__("The " "EraseFlash" " routine is no longer supported. Please use the MPLAB X MCC."))) void EraseFlash(unsigned long startaddr, unsigned long endaddr);







# 1 "/opt/microchip/xc8/v2.20/pic/include/errata.h" 1 3
# 27 "/opt/microchip/xc8/v2.20/pic/include/pic18.h" 2 3
# 49 "/opt/microchip/xc8/v2.20/pic/include/pic18.h" 3
#pragma intrinsic(__nop)
extern void __nop(void);
# 158 "/opt/microchip/xc8/v2.20/pic/include/pic18.h" 3
__attribute__((__unsupported__("The " "Read_b_eep" " routine is no longer supported. Please use the MPLAB X MCC."))) unsigned char Read_b_eep(unsigned int badd);
__attribute__((__unsupported__("The " "Busy_eep" " routine is no longer supported. Please use the MPLAB X MCC."))) void Busy_eep(void);
__attribute__((__unsupported__("The " "Write_b_eep" " routine is no longer supported. Please use the MPLAB X MCC."))) void Write_b_eep(unsigned int badd, unsigned char bdat);
# 194 "/opt/microchip/xc8/v2.20/pic/include/pic18.h" 3
unsigned char __t1rd16on(void);
unsigned char __t3rd16on(void);






#pragma intrinsic(_delay)
extern __attribute__((nonreentrant)) void _delay(unsigned long);
#pragma intrinsic(_delaywdt)
extern __attribute__((nonreentrant)) void _delaywdt(unsigned long);
#pragma intrinsic(_delay3)
extern __attribute__((nonreentrant)) void _delay3(unsigned char);
# 33 "/opt/microchip/xc8/v2.20/pic/include/xc.h" 2 3
# 42 "mcc_generated_files/TCPIPLibrary/ENC28J60.c" 2

# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 1 3
# 22 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 127 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long uintptr_t;
# 142 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long intptr_t;
# 158 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef signed char int8_t;




typedef short int16_t;
# 173 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long int32_t;





typedef long long int64_t;
# 188 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long long intmax_t;





typedef unsigned char uint8_t;




typedef unsigned short uint16_t;
# 209 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long uint32_t;





typedef unsigned long long uint64_t;
# 229 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long long uintmax_t;
# 23 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3

typedef int8_t int_fast8_t;

typedef int64_t int_fast64_t;


typedef int8_t int_least8_t;
typedef int16_t int_least16_t;

typedef int24_t int_least24_t;

typedef int32_t int_least32_t;

typedef int64_t int_least64_t;


typedef uint8_t uint_fast8_t;

typedef uint64_t uint_fast64_t;


typedef uint8_t uint_least8_t;
typedef uint16_t uint_least16_t;

typedef uint24_t uint_least24_t;

typedef uint32_t uint_least32_t;

typedef uint64_t uint_least64_t;
# 139 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/stdint.h" 1 3
typedef int32_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef uint32_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
# 140 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3
# 43 "mcc_generated_files/TCPIPLibrary/ENC28J60.c" 2

# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/stddef.h" 1 3
# 19 "/opt/microchip/xc8/v2.20/pic/include/c99/stddef.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 132 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long ptrdiff_t;
# 20 "/opt/microchip/xc8/v2.20/pic/include/c99/stddef.h" 2 3
# 44 "mcc_generated_files/TCPIPLibrary/ENC28J60.c" 2

# 1 "mcc_generated_files/TCPIPLibrary/ENC28J60.h" 1
# 55 "mcc_generated_files/TCPIPLibrary/ENC28J60.h"
typedef enum
{
    rcr_inst = 0x00,
    rbm_inst = 0x3A,
    wcr_inst = 0x40,
    wbm_inst = 0x7A,
    bfs_inst = 0x80,
    bfc_inst = 0xa0,
    src_inst = 0xFF
}spi_inst_t;

typedef enum
{
 sfr_bank0 = 0x00,
 sfr_bank1 = 0x40,
 sfr_bank2 = 0x80,
 sfr_bank3 = 0xC0,
 sfr_common = 0xE0
} sfr_bank_t;

typedef enum {

 J60_ERDPTL = (0x00 | 0x00),
 J60_ERDPTH = (0x00 | 0x01),
 J60_EWRPTL = (0x00 | 0x02),
 J60_EWRPTH = (0x00 | 0x03),
 J60_ETXSTL = (0x00 | 0x04),
 J60_ETXSTH = (0x00 | 0x05),
 J60_ETXNDL = (0x00 | 0x06),
 J60_ETXNDH = (0x00 | 0x07),
 J60_ERXSTL = (0x00 | 0x08),
 J60_ERXSTH = (0x00 | 0x09),
 J60_ERXNDL = (0x00 | 0x0A),
 J60_ERXNDH = (0x00 | 0x0B),
 J60_ERXRDPTL = (0x00 | 0x0C),
 J60_ERXRDPTH = (0x00 | 0x0D),
 J60_ERXWRPTL = (0x00 | 0x0E),
 J60_ERXWRPTH = (0x00 | 0x0F),
 J60_EDMASTL = (0x00 | 0x10),
 J60_EDMASTH = (0x00 | 0x11),
 J60_EDMANDL = (0x00 | 0x12),
 J60_EDMANDH = (0x00 | 0x13),
 J60_EDMADSTL = (0x00 | 0x14),
 J60_EDMADSTH = (0x00 | 0x15),
 J60_EDMACSL = (0x00 | 0x16),
 J60_EDMACSH = (0x00 | 0x17),
 RSRV_018 = (0x00 | 0x18),
 RSRV_019 = (0x00 | 0x19),
 RSRV_01A = (0x00 | 0x1A),

 J60_EHT0 = (0x40 | 0x00),
 J60_EHT1 = (0x40 | 0x01),
 J60_EHT2 = (0x40 | 0x02),
 J60_EHT3 = (0x40 | 0x03),
 J60_EHT4 = (0x40 | 0x04),
 J60_EHT5 = (0x40 | 0x05),
 J60_EHT6 = (0x40 | 0x06),
 J60_EHT7 = (0x40 | 0x07),
 J60_EPMM0 = (0x40 | 0x08),
 J60_EPMM1 = (0x40 | 0x09),
 J60_EPMM2 = (0x40 | 0x0A),
 J60_EPMM3 = (0x40 | 0x0B),
 J60_EPMM4 = (0x40 | 0x0C),
 J60_EPMM5 = (0x40 | 0x0D),
 J60_EPMM6 = (0x40 | 0x0E),
 J60_EPMM7 = (0x40 | 0x0F),
 J60_EPMCSL = (0x40 | 0x10),
 J60_EPMCSH = (0x40 | 0x11),
 RSRV_112 = (0x40 | 0x12),
 RSRV_113 = (0x40 | 0x13),
 J60_EPMOL = (0x40 | 0x14),
 J60_EPMOH = (0x40 | 0x15),
 RSRV_116 = (0x40 | 0x16),
 RSRV_117 = (0x40 | 0x17),
 J60_ERXFCON = (0x40 | 0x18),
 J60_EPKTCNT = (0x40 | 0x19),
 RSRV_11A = (0x40 | 0x1A),

 J60_MACON1 = (0x80 | 0x00),
 RSRV_201 = (0x80 | 0x01),
 J60_MACON3 = (0x80 | 0x02),
 J60_MACON4 = (0x80 | 0x03),
 J60_MABBIPG = (0x80 | 0x04),
 RSRV_205 = (0x80 | 0x05),
 J60_MAIPGL = (0x80 | 0x06),
 J60_MAIPGH = (0x80 | 0x07),
 J60_MACLCON1 = (0x80 | 0x08),
 J60_MACLCON2 = (0x80 | 0x09),
 J60_MAMXFLL = (0x80 | 0x0A),
 J60_MAMXFLH = (0x80 | 0x0B),
 RSRV_20C = (0x80 | 0x0C),
 RSRV_20D = (0x80 | 0x0D),
 RSRV_20E = (0x80 | 0x0E),
 RSRV_20F = (0x80 | 0x0F),
 RSRV_210 = (0x80 | 0x10),
 RSRV_211 = (0x80 | 0x11),
 J60_MICMD = (0x80 | 0x12),
 RSRV_213 = (0x80 | 0x13),
 J60_MIREGADR = (0x80 | 0x14),
 RSRV_215 = (0x80 | 0x15),
 J60_MIWRL = (0x80 | 0x16),
 J60_MIWRH = (0x80 | 0x17),
 J60_MIRDL = (0x80 | 0x18),
 J60_MIRDH = (0x80 | 0x19),
 RSRV_21A = (0x80 | 0x1A),

 J60_MAADR5 = (0xC0 | 0x00),
 J60_MAADR6 = (0xC0 | 0x01),
 J60_MAADR3 = (0xC0 | 0x02),
 J60_MAADR4 = (0xC0 | 0x03),
 J60_MAADR1 = (0xC0 | 0x04),
 J60_MAADR2 = (0xC0 | 0x05),
 J60_EBSTSD = (0xC0 | 0x06),
 J60_EBSTCON = (0xC0 | 0x07),
 J60_EBSTCSL = (0xC0 | 0x08),
 J60_EBSTCSH = (0xC0 | 0x09),
 J60_MISTAT = (0xC0 | 0x0A),
 RSRV_30B = (0xC0 | 0x0B),
 RSRV_30C = (0xC0 | 0x0C),
 RSRV_30D = (0xC0 | 0x0D),
 RSRV_30E = (0xC0 | 0x0E),
 RSRV_30F = (0xC0 | 0x0F),
 RSRV_310 = (0xC0 | 0x10),
 RSRV_311 = (0xC0 | 0x11),
 J60_EREVID = (0xC0 | 0x12),
 RSRV_313 = (0xC0 | 0x13),
 RSRV_314 = (0xC0 | 0x14),
 J60_ECOCON = (0xC0 | 0x15),
 RSRV_316 = (0xC0 | 0x16),
 J60_EFLOCON = (0xC0 | 0x17),
 J60_EPAUSL = (0xC0 | 0x18),
 J60_EPAUSH = (0xC0 | 0x19),
 RSRV_31A = (0xC0 | 0x1A),

 J60_EIE = (0xE0 | 0x1B),
 J60_EIR = (0xE0 | 0x1C),
 J60_ESTAT = (0xE0 | 0x1D),
 J60_ECON2 = (0xE0 | 0x1E),
 J60_ECON1 = (0xE0 | 0x1F)
} enc28j60_registers_t;

typedef enum {
 J60_PHCON1 = 0x00,
 J60_PHSTAT1 = 0x01,
 J60_PHID1 = 0x02,
 J60_PHID2 = 0x03,
 J60_PHCON2 = 0x10,
 J60_PHSTAT2 = 0x11,
 J60_PHIE = 0x12,
 J60_PHIR = 0x13,
 J60_PHLCON = 0x14
} enc28j60_phy_registers_t;

typedef union
{
    uint8_t val[4];
    struct
    {
        uint16_t byteCount;

        unsigned longDropEvent:1;
        unsigned :1;
        unsigned oldCarrierEvent:1;
        unsigned :1;
        unsigned CRCError:1;
        unsigned lengthCheckError:1;
        unsigned lengthOOR:1;
        unsigned receivedOK:1;

        unsigned multicastPacket:1;
        unsigned broadcastPacket:1;
        unsigned dribbleNibble:1;
        unsigned controlFrame:1;
        unsigned pauseControl:1;
        unsigned unknownOP:1;
        unsigned vlan :1;
        unsigned zero :1;
    };
} receiveStatusVector_t;


typedef union
{
    char val;
    struct
    {
        unsigned RXERIF:1;
        unsigned TXERIF:1;
        unsigned :1;
        unsigned TXIF:1;
        unsigned LINKIF:1;
        unsigned DMAIF:1;
        unsigned PKTIF:1;
        unsigned :1;
    };
} eir_t;


typedef union
{
    unsigned int val;
    struct
    {
        unsigned :5;
        unsigned PLRITY :1;
        unsigned :3;
        unsigned DPXSTAT:1;
        unsigned LSTAT :1;
        unsigned COLSTAT:1;
        unsigned RXSTAT :1;
        unsigned TXSTAT :1;
        unsigned :2;

    };
} phstat2_t;
# 45 "mcc_generated_files/TCPIPLibrary/ENC28J60.c" 2

# 1 "mcc_generated_files/TCPIPLibrary/../mcc.h" 1
# 50 "mcc_generated_files/TCPIPLibrary/../mcc.h"
# 1 "mcc_generated_files/TCPIPLibrary/../pin_manager.h" 1
# 152 "mcc_generated_files/TCPIPLibrary/../pin_manager.h"
void PIN_MANAGER_Initialize (void);
# 164 "mcc_generated_files/TCPIPLibrary/../pin_manager.h"
void PIN_MANAGER_IOC(void);
# 50 "mcc_generated_files/TCPIPLibrary/../mcc.h" 2


# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/stdbool.h" 1 3
# 52 "mcc_generated_files/TCPIPLibrary/../mcc.h" 2

# 1 "mcc_generated_files/TCPIPLibrary/../interrupt_manager.h" 1
# 109 "mcc_generated_files/TCPIPLibrary/../interrupt_manager.h"
void __attribute__((picinterrupt(("")))) INTERRUPT_InterruptManager(void);
# 121 "mcc_generated_files/TCPIPLibrary/../interrupt_manager.h"
void INTERRUPT_Initialize (void);
# 53 "mcc_generated_files/TCPIPLibrary/../mcc.h" 2

# 1 "mcc_generated_files/TCPIPLibrary/../spi_driver.h" 1
# 26 "mcc_generated_files/TCPIPLibrary/../spi_driver.h"
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/stdio.h" 1 3
# 24 "/opt/microchip/xc8/v2.20/pic/include/c99/stdio.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3





typedef void * va_list[1];




typedef void * __isoc_va_list[1];
# 137 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long ssize_t;
# 246 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long long off_t;
# 399 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef struct _IO_FILE FILE;
# 25 "/opt/microchip/xc8/v2.20/pic/include/c99/stdio.h" 2 3
# 52 "/opt/microchip/xc8/v2.20/pic/include/c99/stdio.h" 3
typedef union _G_fpos64_t {
 char __opaque[16];
 double __align;
} fpos_t;

extern FILE *const stdin;
extern FILE *const stdout;
extern FILE *const stderr;





FILE *fopen(const char *restrict, const char *restrict);
FILE *freopen(const char *restrict, const char *restrict, FILE *restrict);
int fclose(FILE *);

int remove(const char *);
int rename(const char *, const char *);

int feof(FILE *);
int ferror(FILE *);
int fflush(FILE *);
void clearerr(FILE *);

int fseek(FILE *, long, int);
long ftell(FILE *);
void rewind(FILE *);

int fgetpos(FILE *restrict, fpos_t *restrict);
int fsetpos(FILE *, const fpos_t *);

size_t fread(void *restrict, size_t, size_t, FILE *restrict);
size_t fwrite(const void *restrict, size_t, size_t, FILE *restrict);

int fgetc(FILE *);
int getc(FILE *);
int getchar(void);
int ungetc(int, FILE *);

int fputc(int, FILE *);
int putc(int, FILE *);
int putchar(int);

char *fgets(char *restrict, int, FILE *restrict);

char *gets(char *);


int fputs(const char *restrict, FILE *restrict);
int puts(const char *);

#pragma printf_check(printf) const
#pragma printf_check(vprintf) const
#pragma printf_check(sprintf) const
#pragma printf_check(snprintf) const
#pragma printf_check(vsprintf) const
#pragma printf_check(vsnprintf) const

int printf(const char *restrict, ...);
int fprintf(FILE *restrict, const char *restrict, ...);
int sprintf(char *restrict, const char *restrict, ...);
int snprintf(char *restrict, size_t, const char *restrict, ...);

int vprintf(const char *restrict, __isoc_va_list);
int vfprintf(FILE *restrict, const char *restrict, __isoc_va_list);
int vsprintf(char *restrict, const char *restrict, __isoc_va_list);
int vsnprintf(char *restrict, size_t, const char *restrict, __isoc_va_list);

int scanf(const char *restrict, ...);
int fscanf(FILE *restrict, const char *restrict, ...);
int sscanf(const char *restrict, const char *restrict, ...);
int vscanf(const char *restrict, __isoc_va_list);
int vfscanf(FILE *restrict, const char *restrict, __isoc_va_list);
int vsscanf(const char *restrict, const char *restrict, __isoc_va_list);

void perror(const char *);

int setvbuf(FILE *restrict, char *restrict, int, size_t);
void setbuf(FILE *restrict, char *restrict);

char *tmpnam(char *);
FILE *tmpfile(void);




FILE *fmemopen(void *restrict, size_t, const char *restrict);
FILE *open_memstream(char **, size_t *);
FILE *fdopen(int, const char *);
FILE *popen(const char *, const char *);
int pclose(FILE *);
int fileno(FILE *);
int fseeko(FILE *, off_t, int);
off_t ftello(FILE *);
int dprintf(int, const char *restrict, ...);
int vdprintf(int, const char *restrict, __isoc_va_list);
void flockfile(FILE *);
int ftrylockfile(FILE *);
void funlockfile(FILE *);
int getc_unlocked(FILE *);
int getchar_unlocked(void);
int putc_unlocked(int, FILE *);
int putchar_unlocked(int);
ssize_t getdelim(char **restrict, size_t *restrict, int, FILE *restrict);
ssize_t getline(char **restrict, size_t *restrict, FILE *restrict);
int renameat(int, const char *, int, const char *);
char *ctermid(char *);







char *tempnam(const char *, const char *);
# 26 "mcc_generated_files/TCPIPLibrary/../spi_driver.h" 2




# 1 "mcc_generated_files/TCPIPLibrary/../spi_types.h" 1
# 28 "mcc_generated_files/TCPIPLibrary/../spi_types.h"
typedef enum {
    MAC
} spi_modes;
# 30 "mcc_generated_files/TCPIPLibrary/../spi_driver.h" 2





__attribute__((inline)) void spi_close(void);

_Bool spi_master_open(spi_modes spiUniqueConfiguration);
_Bool spi_slave_open(spi_modes spiUniqueConfiguration);

uint8_t spi_exchangeByte(uint8_t b);

void spi_exchangeBlock(void *block, size_t blockSize);
void spi_writeBlock(void *block, size_t blockSize);
void spi_readBlock(void *block, size_t blockSize);
# 54 "mcc_generated_files/TCPIPLibrary/../mcc.h" 2

# 1 "mcc_generated_files/TCPIPLibrary/../tmr1.h" 1
# 95 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
void TMR1_Initialize(void);
# 126 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
void TMR1_StartTimer(void);
# 156 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
void TMR1_StopTimer(void);
# 189 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
uint16_t TMR1_ReadTimer(void);
# 215 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
void TMR1_WriteTimer(uint16_t timerVal);
# 247 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
void TMR1_Reload(void);
# 263 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
void TMR1_ISR(void);
# 281 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
void TMR1_CallBack(void);
# 299 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
 void TMR1_SetInterruptHandler(void (* InterruptHandler)(void));
# 317 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
extern void (*TMR1_InterruptHandler)(void);
# 335 "mcc_generated_files/TCPIPLibrary/../tmr1.h"
void TMR1_DefaultInterruptHandler(void);
# 55 "mcc_generated_files/TCPIPLibrary/../mcc.h" 2

# 1 "mcc_generated_files/TCPIPLibrary/../tmr2.h" 1
# 103 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
void TMR2_Initialize(void);
# 132 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
void TMR2_StartTimer(void);
# 164 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
void TMR2_StopTimer(void);
# 199 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
uint8_t TMR2_ReadTimer(void);
# 238 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
void TMR2_WriteTimer(uint8_t timerVal);
# 290 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
void TMR2_LoadPeriodRegister(uint8_t periodVal);
# 308 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
void TMR2_ISR(void);
# 326 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
 void TMR2_CallBack(void);
# 343 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
 void TMR2_SetInterruptHandler(void (* InterruptHandler)(void));
# 361 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
extern void (*TMR2_InterruptHandler)(void);
# 379 "mcc_generated_files/TCPIPLibrary/../tmr2.h"
void TMR2_DefaultInterruptHandler(void);
# 56 "mcc_generated_files/TCPIPLibrary/../mcc.h" 2

# 1 "mcc_generated_files/TCPIPLibrary/../tmr0.h" 1
# 100 "mcc_generated_files/TCPIPLibrary/../tmr0.h"
void TMR0_Initialize(void);
# 129 "mcc_generated_files/TCPIPLibrary/../tmr0.h"
void TMR0_StartTimer(void);
# 161 "mcc_generated_files/TCPIPLibrary/../tmr0.h"
void TMR0_StopTimer(void);
# 197 "mcc_generated_files/TCPIPLibrary/../tmr0.h"
uint16_t TMR0_ReadTimer(void);
# 236 "mcc_generated_files/TCPIPLibrary/../tmr0.h"
void TMR0_WriteTimer(uint16_t timerVal);
# 272 "mcc_generated_files/TCPIPLibrary/../tmr0.h"
void TMR0_Reload(void);
# 310 "mcc_generated_files/TCPIPLibrary/../tmr0.h"
_Bool TMR0_HasOverflowOccured(void);
# 57 "mcc_generated_files/TCPIPLibrary/../mcc.h" 2

# 1 "mcc_generated_files/TCPIPLibrary/../eusart1.h" 1
# 76 "mcc_generated_files/TCPIPLibrary/../eusart1.h"
void EUSART1_Initialize(void);
# 85 "mcc_generated_files/TCPIPLibrary/../eusart1.h"
_Bool EUSART1_is_tx_ready(void);
# 94 "mcc_generated_files/TCPIPLibrary/../eusart1.h"
_Bool EUSART1_is_rx_ready(void);
# 103 "mcc_generated_files/TCPIPLibrary/../eusart1.h"
_Bool EUSART1_is_tx_done(void);






uint8_t EUSART1_Read(void);
# 119 "mcc_generated_files/TCPIPLibrary/../eusart1.h"
void EUSART1_Write(uint8_t txData);
# 58 "mcc_generated_files/TCPIPLibrary/../mcc.h" 2

# 1 "mcc_generated_files/TCPIPLibrary/../TCPIPLibrary/network.h" 1
# 44 "mcc_generated_files/TCPIPLibrary/../TCPIPLibrary/network.h"
# 1 "mcc_generated_files/TCPIPLibrary/tcpip_types.h" 1
# 49 "mcc_generated_files/TCPIPLibrary/tcpip_types.h"
typedef enum {TCB_ERROR = -1, TCB_NO_ERROR = 0} tcbError_t;
# 64 "mcc_generated_files/TCPIPLibrary/tcpip_types.h"
typedef struct
{
    uint8_t destinationMAC[6];
    uint8_t sourceMAC[6];
    union
    {
        uint16_t type;
        uint16_t length;
        uint16_t tpid;
    }id;




} ethernetFrame_t;

uint8_t Control_Byte = 0x00;
# 133 "mcc_generated_files/TCPIPLibrary/tcpip_types.h"
typedef struct
{
    unsigned ihl:4;
    unsigned version:4;
    unsigned ecn:2;
    unsigned dscp:6;
    uint16_t length;
    uint16_t identifcation;
    unsigned fragmentOffsetHigh:5;
    unsigned :1;
    unsigned dontFragment:1;
    unsigned moreFragments:1;
    uint8_t fragmentOffsetLow;
    uint8_t timeToLive;
    uint8_t protocol;
    uint16_t headerCksm;
    uint32_t srcIpAddress;
    uint32_t dstIpAddress;


} ipv4Header_t;


typedef struct
{
    uint32_t srcIpAddress;
    uint32_t dstIpAddress;
    uint8_t protocol;
    uint8_t z;
    uint16_t length;
} ipv4_pseudo_header_t;

typedef struct
{
    union
    {
        uint16_t typeCode;
        struct
        {
            uint8_t code;
            uint8_t type;
        };
    };
    uint16_t checksum;
} icmpHeader_t;


typedef struct
{
    union
    {
        uint16_t typeCode;
        struct
        {
            uint8_t code;
            uint8_t type;
        };
    };
    uint16_t checksum;
} icmpv6Header_t;


typedef enum
{
    ECHO_REPLY = 0x0000,

    DEST_NETWORK_UNREACHABLE = 0x0300,
    DEST_HOST_UNREACHABLE = 0x0301,
    DEST_PROTOCOL_UNREACHABLE = 0x0302,
    DEST_PORT_UNREACHABLE = 0x0303,
    FRAGMENTATION_REQUIRED = 0x0304,
    SOURCE_ROUTE_FAILED = 0x0305,
    DESTINATION_NETWORK_UNKNOWN = 0x0306,
    SOURCE_HOST_ISOLATED = 0x0307,
    NETWORK_ADMINISTRATIVELY_PROHIBITED = 0x0308,
    HOST_ADMINISTRATIVELY_PROHIBITED = 0x0309,
    NETWORK_UNREACHABLE_FOR_TOS = 0x030A,
    HOST_UNREACHABLE_FOR_TOS = 0x030B,
    COMMUNICATION_ADMINISTRATIVELY_PROHIBITED = 0x030C,
    HOST_PRECEDENCE_VIOLATION = 0x030D,
    PRECEDENCE_CUTOFF_IN_EFFECT = 0x030E,

    SOURCE_QUENCH = 0x0400,

    REDIRECT_DATAGRAM_FOR_THE_NETWORK = 0x0500,
    REDIRECT_DATAGRAM_FOR_THE_HOST = 0x0501,
    REDIRECT_DATAGRAM_FOR_THE_TOS_AND_NETWORK = 0x0502,
    REDIRECT_DATAGRAM_FOR_THE_TOS_AND_HOST = 0x0503,

    ALTERNATE_HOST_ADDRESS = 0x0600,

    ECHO_REQUEST = 0x0800,


    UNASSIGNED_ECHO_TYPE_CODE_REQUEST_1 = 0x082A,
    UNASSIGNED_ECHO_TYPE_CODE_REQUEST_2 = 0x08FC,

    ROUTER_ADVERTISEMENT = 0x0900,
    ROUTER_SOLICITATION = 0x0A00,
    TRACEROUTE = 0x3000
} icmpTypeCodes_t;

typedef struct
{
    uint16_t srcPort;
    uint16_t dstPort;
    uint16_t length;
    uint16_t checksum;
} udpHeader_t;

typedef struct
{
    uint16_t sourcePort;
    uint16_t destPort;
    uint32_t sequenceNumber;
    uint32_t ackNumber;
    union{
        uint8_t byte13;
        struct{
            uint8_t ns:1;
            uint8_t reserved:3;
            uint8_t dataOffset:4;
        };
    };

    union{
        uint8_t flags;
        struct{
            uint8_t fin:1;
            uint8_t syn:1;
            uint8_t rst:1;
            uint8_t psh:1;
            uint8_t ack:1;
            uint8_t urg:1;
            uint8_t ece:1;
            uint8_t cwr:1;
        };
    };

    uint16_t windowSize;
    uint16_t checksum;
    uint16_t urgentPtr;



} tcpHeader_t;
# 296 "mcc_generated_files/TCPIPLibrary/tcpip_types.h"
typedef enum
{
    HOPOPT_TCPIP = 0,
    ICMP_TCPIP = 1,
    IGMP_TCPIP = 2,
    GGP_TCPIP = 3,
    IPV4_TCPIP = 4,
    ST_TCPIP = 5,
    TCP_TCPIP = 6,
    CBT_TCPIP = 7,
    EGP_TCPIP = 8,
    IGP_TCPIP = 9,
    BBN_RCC_MON_TCPIP = 10,
    NVP_II_TCPIP = 11,
    PUP_TCPIP = 12,
    ARGUS_TCPIP = 13,
    EMCON_TCPIP = 14,
    XNET_TCPIP = 15,
    CHAOS_TCPIP = 16,
    UDP_TCPIP = 17,
    MUX_TCPIP = 18,
    DCN_MEAS_TCPIP = 19,
    HMP_TCPIP = 20,
    PRM_TCPIP = 21,
    XNS_IDP_TCPIP = 22,
    TRUNK_1_TCPIP = 23,
    TRUNK_2_TCPIP = 24,
    LEAF_1_TCPIP = 25,
    LEAF_2_TCPIP = 26,
    RDP_TCPIP = 27,
    IRTP_TCPIP = 28,
    ISO_TP4_TCPIP = 29,
    NETBLT_TCPIP = 30,
    MFE_NSP_TCPIP = 31,
    MERIT_INP_TCPIP = 32,
    DCCP_TCPIP = 33,
    THREEPC_TCPIP = 34,
    IDPR_TCPIP = 35,
    XTP_TCPIP = 36,
    DDP_TCPIP = 37,
    IDPR_CMTP_TCPIP = 38,
    TPpp_TCPIP = 39,
    IL_TCPIP = 40,
    IPV6_TUNNEL_TCPIP = 41,
    SDRP_TCPIP = 42,
    IPV6_Route_TCPIP = 43,
    IPV6_Frag_TCPIP = 44,
    IDRP_TCPIP = 45,
    RSVP_TCPIP = 46,
    GRE_TCPIP = 47,
    DSR_TCPIP = 48,
    BNA_TCPIP = 49,
    ESP_TCPIP = 50,
    AH_TCPIP = 51,
    I_NLSP_TCPIP = 52,
    SWIPE_TCPIP = 53,
    NARP_TCPIP = 54,
    MOBILE_TCPIP = 55,
    TLSP_TCPIP = 56,
    SKIP_TCPIP = 57,
    IPV6_ICMP_TCPIP = 58,
    IPV6_NoNxt_TCPIP = 59,
    IPV6_Opts_TCPIP = 60,
    CFTP_TCPIP = 62,
    SAT_EXPAK_TCPIP = 64,
    KRYPTOLAN_TCPIP = 65,
    RVD_TCPIP = 66,
    IPPC_TCPIP = 67,
    SAT_MON_TCPIP = 69,
    VISA_TCPIP = 70,
    IPCV_TCPIP = 71,
    CPNX_TCPIP = 72,
    CPHB_TCPIP = 73,
    WSN_TCPIP = 74,
    PVP_TCPIP = 75,
    BR_SAT_MON_TCPIP = 76,
    SUN_ND_TCPIP = 77,
    WB_MON_TCPIP = 78,
    WB_EXPAK_TCPIP = 79,
    ISO_IP_TCPIP = 80,
    VMTP_TCPIP = 81,
    SECURE_VMTP_TCPIP = 82,
    VINES_TCPIP = 83,
    TTP_TCPIP = 84,
    IPTM_TCPIP = 84,
    NSFNET_IGP_TCPIP = 85,
    DGP_TCPIP = 86,
    TCF_TCPIP = 87,
    EIGRP_TCPIP = 88,
    OSPFIGP_TCPIP = 89,
    Sprite_RPC_TCPIP = 90,
    LARP_TCPIP = 91,
    MTP_TCPIP = 92,
    AX25_TCPIP = 93,
    IPIP_TCPIP = 94,
    MICP_TCPIP = 95,
    SCC_SP_TCPIP = 96,
    ETHERIP_TCPIP = 97,
    ENCAP_TCPIP = 98,
    GMTP_TCPIP = 100,
    IFMP_TCPIP = 101,
    PNNI_TCPIP = 102,
    PIM_TCPIP = 103,
    ARIS_TCPIP = 104,
    SCPS_TCPIP = 105,
    QNX_TCPIP = 106,
    A_N_TCPIP = 107,
    IPComp_TCPIP = 108,
    SNP_TCPIP = 109,
    Compaq_Peer_TCPIP = 110,
    IPX_in_IP_TCPIP = 111,
    VRRP_TCPIP = 112,
    PGM_TCPIP = 113,
    L2TP_TCPIP = 115,
    DDX_TCPIP = 116,
    IATP_TCPIP = 117,
    STP_TCPIP = 118,
    SRP_TCPIP = 119,
    UTI_TCPIP = 120,
    SMP_TCPIP = 121,
    SM_TCPIP = 122,
    PTP_TCPIP = 123,
    ISIS_TCPIP = 124,
    FIRE_TCPIP = 125,
    CRTP_TCPIP = 126,
    CRUDP_TCPIP = 127,
    SSCOPMCE_TCPIP = 128,
    IPLT_TCPIP = 129,
    SPS_TCPIP = 130,
    PIPE_TCPIP = 131,
    SCTP_TCPIP = 132,
    FC_TCPIP = 133
} ipProtocolNumbers;

typedef struct
{
    union{
        uint32_t s_addr;
        uint8_t s_addr_byte[4];
    };
}inAddr_t;

typedef struct
{
   union {
        uint8_t s6_u8[16];
        uint16_t s6_u16[8];
        uint32_t s6_u32[4];
    } s6;



}in6Addr_t;

typedef struct
{
    uint16_t port;
    inAddr_t addr;
}sockaddr_in_t;

typedef struct {

    uint16_t in6_port;
    uint32_t in6_flowinfo;
    in6Addr_t in6_addr;
    uint32_t in6_scope_id;
}sockaddr_in6_t;

extern const char *network_errors[];

typedef enum
{
    ERROR =0,
    SUCCESS,
    LINK_NOT_FOUND,
    BUFFER_BUSY,
    TX_LOGIC_NOT_IDLE,
    MAC_NOT_FOUND,
    IP_WRONG_VERSION,
    IPV4_CHECKSUM_FAILS,
    DEST_IP_NOT_MATCHED,
    ICMP_CHECKSUM_FAILS,
    UDP_CHECKSUM_FAILS,
    TCP_CHECKSUM_FAILS,
    DMA_TIMEOUT,
    PORT_NOT_AVAILABLE,
    ARP_IP_NOT_MATCHED,
    EAPoL_PACKET_FAILURE,
    INCORRECT_IPV4_HLEN,
    IPV4_NO_OPTIONS,
    TX_QUEUED
}error_msg;

typedef struct
{
    inAddr_t dest_addr;
}destIP_t;


typedef int8_t socklistsize_t;

typedef void (*ip_receive_function_ptr)(int);
# 44 "mcc_generated_files/TCPIPLibrary/../TCPIPLibrary/network.h" 2
# 72 "mcc_generated_files/TCPIPLibrary/../TCPIPLibrary/network.h"
void Network_Init(void);
# 84 "mcc_generated_files/TCPIPLibrary/../TCPIPLibrary/network.h"
void Network_Read(void);
# 96 "mcc_generated_files/TCPIPLibrary/../TCPIPLibrary/network.h"
void Network_Manage(void);
# 108 "mcc_generated_files/TCPIPLibrary/../TCPIPLibrary/network.h"
void Network_WaitForLink(void);
uint16_t Network_GetStartPosition(void);

void timersInit(void);
# 59 "mcc_generated_files/TCPIPLibrary/../mcc.h" 2
# 75 "mcc_generated_files/TCPIPLibrary/../mcc.h"
void SYSTEM_Initialize(void);
# 88 "mcc_generated_files/TCPIPLibrary/../mcc.h"
void OSCILLATOR_Initialize(void);
# 46 "mcc_generated_files/TCPIPLibrary/ENC28J60.c" 2

# 1 "mcc_generated_files/TCPIPLibrary/ethernet_driver.h" 1
# 47 "mcc_generated_files/TCPIPLibrary/ethernet_driver.h"
# 1 "mcc_generated_files/TCPIPLibrary/mac_address.h" 1
# 51 "mcc_generated_files/TCPIPLibrary/mac_address.h"
typedef union
{
    uint8_t mac_array[6];
    struct { uint8_t byte1,byte2,byte3,byte4,byte5,byte6; } s;
} mac48Address_t;

extern const mac48Address_t broadcastMAC;
extern mac48Address_t macAddress;
extern mac48Address_t hostMacAddress;

const mac48Address_t *MAC_getAddress(void);
# 47 "mcc_generated_files/TCPIPLibrary/ethernet_driver.h" 2
# 70 "mcc_generated_files/TCPIPLibrary/ethernet_driver.h"
typedef struct
{
 unsigned error:1;
 unsigned pktReady:1;
 unsigned up:1;
 unsigned idle:1;
 unsigned linkChange:1;
        unsigned bufferBusy:1;
        unsigned :3;
        uint16_t saveRDPT;
        uint16_t saveWRPT;
} ethernetDriver_t;

typedef struct
{
    uint16_t flags;
    uint16_t packetStart;
    uint16_t packetEnd;

    void *prevPacket;
    void *nextPacket;
} txPacket_t;

extern volatile ethernetDriver_t ethData;





void ETH_CloseSPI(void);
void ETH_OpenSPI(void);

void ETH_Init(void);
void ETH_EventHandler(void);
void ETH_NextPacketUpdate(void);
void ETH_ResetReceiver(void);
void ETH_SendSystemReset(void);


uint16_t ETH_ReadBlock(void*, uint16_t);
uint8_t ETH_Read8(void);
uint16_t ETH_Read16(void);
uint32_t ETH_Read24(void);
uint32_t ETH_Read32(void);
void ETH_Dump(uint16_t);
void ETH_Flush(void);

uint16_t ETH_GetFreeTxBufferSize(void);

error_msg ETH_WriteStart(const mac48Address_t *dest_mac, uint16_t type);
uint16_t ETH_WriteString(const char *string);
uint16_t ETH_WriteBlock(const void *, uint16_t);
void ETH_Write8(uint8_t);
void ETH_Write16(uint16_t);
void ETH_Write24(uint32_t data);
void ETH_Write32(uint32_t);
void ETH_Insert(uint8_t *,uint16_t, uint16_t);
error_msg ETH_Copy(uint16_t);
error_msg ETH_Send(void);

uint16_t ETH_TxComputeChecksum(uint16_t position, uint16_t len, uint16_t seed);
uint16_t ETH_RxComputeChecksum(uint16_t len, uint16_t seed);

void ETH_GetMAC(uint8_t *);
void ETH_SetMAC(uint8_t *);
uint16_t ETH_GetWritePtr();
void ETH_SaveRDPT(void);
void ETH_ResetReadPtr();
uint16_t ETH_GetReadPtr(void);
void ETH_SetReadPtr(uint16_t);
uint16_t ETH_GetStatusVectorByteCount(void);
void ETH_SetStatusVectorByteCount(uint16_t);

void ETH_ResetByteCount(void);
uint16_t ETH_GetByteCount(void);

uint16_t ETH_ReadSavedWRPT(void);
void ETH_SaveWRPT(void);
void ETH_SetRxByteCount(uint16_t count);
uint16_t ETH_GetRxByteCount();

_Bool ETH_CheckLinkUp(void);

void ETH_TxReset(void);
void ETH_MoveBackReadPtr(uint16_t offset);
# 47 "mcc_generated_files/TCPIPLibrary/ENC28J60.c" 2
# 58 "mcc_generated_files/TCPIPLibrary/ENC28J60.c"
volatile ethernetDriver_t ethData;
const mac48Address_t *eth_MAC;
static uint16_t nextPacketPointer;
static receiveStatusVector_t rxPacketStatusVector;
sfr_bank_t lastBank;
# 72 "mcc_generated_files/TCPIPLibrary/ENC28J60.c"
static uint8_t ENC28_Rcr8(enc28j60_registers_t);
static uint16_t ENC28_Rcr16(enc28j60_registers_t);

static void ENC28_Wcr8(enc28j60_registers_t, uint8_t);
static void ENC28_Wcr16(enc28j60_registers_t, uint16_t);

static void ENC28_Bfs(enc28j60_registers_t, char);
static void ENC28_Bfc(enc28j60_registers_t, char);

static uint16_t ENC28_PhyRead(enc28j60_phy_registers_t a);
static void ENC28_PhyWrite(enc28j60_phy_registers_t a, uint16_t d);

static void ENC28_BankSel(enc28j60_registers_t);

static uint16_t checksumCalculation(uint16_t, uint16_t, uint16_t);

uint16_t TXPacketSize;







static void ENC28_BankSel(enc28j60_registers_t r)
{
    uint8_t a = r & 0xE0;

    if (a != sfr_common && a != lastBank)
    {
        lastBank = a;

        do{LATC2 = 0;} while(0);
        spi_exchangeByte(bfc_inst | 0x1F);
        spi_exchangeByte(0x03);
        do{LATC2 = 1;} while(0);
        __nop();
        __nop();

        do{LATC2 = 0;} while(0);
        spi_exchangeByte(bfs_inst | 0x1F);
        spi_exchangeByte(a >> 6);
        do{LATC2 = 1;} while(0);
    }
}




void ETH_CloseSPI(void)
{
    spi_close();
}




void ETH_OpenSPI(void)
{
    while (!spi_master_open(MAC));
}




void ETH_Init(void)
{
    ETH_OpenSPI();


    ethData.error = 0;
    ethData.up = 0;
    ethData.linkChange = 0;
    ethData.bufferBusy = 0;
    ethData.saveRDPT = 0;

    lastBank = sfr_bank0;

    _delay((unsigned long)((100)*(16000000/4000000.0)));

    ETH_SendSystemReset();
    _delay((unsigned long)((10)*(16000000/4000.0)));


    while (!(ENC28_Rcr8(J60_ESTAT) & 0x01));


    nextPacketPointer = (0);

    ENC28_Bfs (J60_ECON2, 0x80);


    ENC28_Wcr16(J60_ETXSTL, (0x1FFF - (1500)));
    ENC28_Wcr16(J60_ETXNDL, (0x1FFF));
    ENC28_Wcr16(J60_ERXSTL, (0));
    ENC28_Wcr16(J60_ERXNDL, ((0x1FFF - (1500)) - 2));
    ENC28_Wcr16(J60_ERDPTL,nextPacketPointer);

    ENC28_Wcr16(J60_ERDPTL, (0));
    ENC28_Wcr16(J60_EWRPTL, (0x1FFF - (1500)));


    ENC28_Wcr8(J60_ERXFCON, 0b10101001);


    eth_MAC = MAC_getAddress();


    ENC28_Wcr8(J60_MACON1, 0x0D);
    ENC28_Wcr8(J60_MACON3, 0x22);
    ENC28_Wcr8(J60_MACON4, 0x40);
    ENC28_Wcr16(J60_MAIPGL, 0x0c12);
    ENC28_Wcr8(J60_MABBIPG, 0x12);
    ENC28_Wcr16(J60_MAMXFLL, (1500));
    ENC28_Wcr8(J60_MAADR1, eth_MAC->mac_array[0]); __nop();
    ENC28_Wcr8(J60_MAADR2, eth_MAC->mac_array[1]); __nop();
    ENC28_Wcr8(J60_MAADR3, eth_MAC->mac_array[2]); __nop();
    ENC28_Wcr8(J60_MAADR4, eth_MAC->mac_array[3]); __nop();
    ENC28_Wcr8(J60_MAADR5, eth_MAC->mac_array[4]); __nop();
    ENC28_Wcr8(J60_MAADR6, eth_MAC->mac_array[5]); __nop();

    ENC28_Wcr8(J60_ECON1, 0x04);


    ENC28_PhyWrite(J60_PHCON1, 0x0000);
    ENC28_PhyWrite(J60_PHCON2, 0x0100);
    ENC28_PhyWrite(J60_PHLCON, 0x0472);



    ENC28_Wcr8(J60_EIE, 0xDB);
    ENC28_Wcr16(J60_PHIE, 0x12);


    ETH_CheckLinkUp();

}




void ETH_EventHandler(void)
{
    eir_t eir_val;
    phstat2_t phstat2_val;



        eir_val.val = ENC28_Rcr8(J60_EIR);
        phstat2_val.val = ENC28_Rcr16(J60_PHSTAT2);

        if (eir_val.LINKIF)
        {
           ethData.linkChange = 1;
           ethData.up = 0;
           if(ETH_CheckLinkUp())
            {
            }
           if (phstat2_val.DPXSTAT)
           {
               ENC28_Wcr16(J60_MABBIPG, 0x15);
               ENC28_Bfs(J60_PHSTAT2,0x01);
           }
           else
           {
               ENC28_Wcr16(J60_MABBIPG, 0x12);
               ENC28_Bfc(J60_PHSTAT2,0x01);
           }
        }
        if(eir_val.TXIF)
        {
            ethData.bufferBusy = 0;
            ENC28_Bfc(J60_EIR,0x08);
        }
        if (eir_val.PKTIF || ENC28_Rcr8(J60_EPKTCNT))
        {
            ethData.pktReady = 1;
        }
        ENC28_Wcr8(J60_EIR, eir_val.val);
}

void ETH_ResetReceiver(void)
{
    uint8_t econ1;
    econ1 = ENC28_Rcr8(J60_ECON1);
    ENC28_Wcr8(J60_ECON1,(econ1 | 0x40));
}





void ETH_NextPacketUpdate(void)
{


    if (nextPacketPointer == (0))
        ENC28_Wcr16(J60_ERXRDPTL, ((0x1FFF - (1500)) - 2));
    else
        ENC28_Wcr16(J60_ERXRDPTL, nextPacketPointer-1);

    ENC28_Wcr16(J60_ERDPTL, nextPacketPointer);

    do{LATC2 = 0;} while(0);
    spi_exchangeByte(rbm_inst);
    ((char *) &nextPacketPointer)[0] = spi_exchangeByte(0);
    ((char *) &nextPacketPointer)[1] = spi_exchangeByte(0);
    ((char *) &rxPacketStatusVector)[0] = spi_exchangeByte(0);
    ((char *) &rxPacketStatusVector)[1] = spi_exchangeByte(0);
    ((char *) &rxPacketStatusVector)[2] = spi_exchangeByte(0);
    ((char *) &rxPacketStatusVector)[3] = spi_exchangeByte(0);
    do{LATC2 = 1;} while(0);
    rxPacketStatusVector.byteCount -= 4;

}






static uint8_t ENC28_Rcr8(enc28j60_registers_t a)
{
    uint8_t v;

    ENC28_BankSel(a);
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(rcr_inst | (a & 0x1F));
    v = spi_exchangeByte(0);
    do{LATC2 = 1;} while(0);

    return v;
}






static uint16_t ENC28_Rcr16(enc28j60_registers_t a)
{
    uint16_t v;

    ENC28_BankSel(a);
    a &= 0x1F;
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(rcr_inst | (a));
    ((char *) &v)[0] = spi_exchangeByte(0);
    do{LATC2 = 1;} while(0);
    __nop();
    __nop();
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(rcr_inst | (a + 1));
    ((char *) &v)[1] = spi_exchangeByte(0);
    do{LATC2 = 1;} while(0);

    return v;
}






static void ENC28_Wcr8(enc28j60_registers_t a, uint8_t v)
{
    ENC28_BankSel(a);
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wcr_inst | (a & 0x1F));
    spi_exchangeByte(v);
    do{LATC2 = 1;} while(0);
}






static void ENC28_Wcr16(enc28j60_registers_t a, uint16_t v)
{
    ENC28_BankSel(a);
    a &= 0x1F;
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wcr_inst | (a));
    spi_exchangeByte(((char *) &v)[0]);
    do{LATC2 = 1;} while(0);
    __nop();
    __nop();
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wcr_inst | (a + 1));
    spi_exchangeByte(((char *) &v)[1]);
    do{LATC2 = 1;} while(0);
}






static void ENC28_Bfs(enc28j60_registers_t a, char bits)
{
    ENC28_BankSel(a);
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(bfs_inst | (a & 0x1F));
    spi_exchangeByte(bits);
    do{LATC2 = 1;} while(0);
}






static void ENC28_Bfc(enc28j60_registers_t a, char bits)
{
    ENC28_BankSel(a);
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(bfc_inst | (a & 0x1F));
    spi_exchangeByte(bits);
    do{LATC2 = 1;} while(0);
}






static void ENC28_PhyWrite(enc28j60_phy_registers_t a, uint16_t d)
{
    uint8_t v=1;

    ENC28_Wcr8(J60_MIREGADR, a);
    ENC28_Wcr16(J60_MIWRL, d);
    while(v & 0x01)
    {
        v = ENC28_Rcr8(J60_MISTAT);
    }
}






static uint16_t ENC28_PhyRead(enc28j60_phy_registers_t a)
{
    ENC28_Wcr8(J60_MIREGADR, a);
    ENC28_Bfs(J60_MICMD, 0x01);
    while (ENC28_Rcr8(J60_MISTAT) & 0x01);
    ENC28_Bfc(J60_MICMD, 0x01);

    return ENC28_Rcr16(J60_MIRDL);
}




void ETH_SendSystemReset(void)
{
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(src_inst);
    do{LATC2 = 1;} while(0);
}





_Bool ETH_CheckLinkUp(void)
{
    uint16_t phstat2;

    phstat2 = ENC28_PhyRead(J60_PHSTAT2);

    if(phstat2 & 0x0400)
    {
        ethData.up = 1;
        return 1;
    }
    else
        return 0;
}






uint8_t ETH_Read8(void)
{
    uint8_t b;

    do{LATC2 = 0;} while(0);
    spi_exchangeByte(rbm_inst);
    b = spi_exchangeByte(0);
    do{LATC2 = 1;} while(0);

    return b;
}





uint16_t ETH_Read16(void)
{
    uint16_t b;

    do{LATC2 = 0;} while(0);
    spi_exchangeByte(rbm_inst);
    b = spi_exchangeByte(0)<< 8;
    b |= spi_exchangeByte(0);
    do{LATC2 = 1;} while(0);

    return b;
}





uint32_t ETH_Read24(void)
{
    uint32_t b;

    do{LATC2 = 0;} while(0);
    spi_exchangeByte(rbm_inst);
    ((char *) &b)[2] = spi_exchangeByte(0);
    ((char *) &b)[1] = spi_exchangeByte(0);
    ((char *) &b)[0] = spi_exchangeByte(0);
    do{LATC2 = 1;} while(0);

    return b;
}





uint32_t ETH_Read32(void)
{
    uint32_t b;

    do{LATC2 = 0;} while(0);
    spi_exchangeByte(rbm_inst);
    ((char *) &b)[3] = spi_exchangeByte(0);
    ((char *) &b)[2] = spi_exchangeByte(0);
    ((char *) &b)[1] = spi_exchangeByte(0);
    ((char *) &b)[0] = spi_exchangeByte(0);
    do{LATC2 = 1;} while(0);

    return b;
}







uint16_t ETH_ReadBlock(void *buffer, uint16_t length)
{
    uint16_t readCount = length;
    char *p = buffer;

    if (rxPacketStatusVector.byteCount < length)
        readCount = rxPacketStatusVector.byteCount;
    length = readCount;
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(rbm_inst);
    while (length--) *p++ = spi_exchangeByte(0);
    do{LATC2 = 1;} while(0);

    return readCount;
}





void ETH_Write8(uint8_t data)
{
    TXPacketSize += 1;
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wbm_inst);
    spi_exchangeByte(data);
    do{LATC2 = 1;} while(0);
}





void ETH_Write16(uint16_t data)
{
    TXPacketSize += 2;
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wbm_inst);
    spi_exchangeByte(data >> 8);
    spi_exchangeByte(data);
    do{LATC2 = 1;} while(0);
}





void ETH_Write24(uint32_t data)
{
    TXPacketSize += 2;
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wbm_inst);
     spi_exchangeByte(data >> 16);
    spi_exchangeByte(data >> 8);
    spi_exchangeByte(data);
    do{LATC2 = 1;} while(0);
}





void ETH_Write32(uint32_t data)
{
    TXPacketSize += 4;
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wbm_inst);
    spi_exchangeByte(data >> 24);
    spi_exchangeByte(data >> 16);
    spi_exchangeByte(data >> 8);
    spi_exchangeByte(data);
    do{LATC2 = 1;} while(0);
}

uint16_t ETH_WriteString(const char *string)
{
    uint16_t length = 0;

    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wbm_inst);
    while(*string)
    {
        spi_exchangeByte(*string++);
        length ++;
    }
    do{LATC2 = 1;} while(0);
    TXPacketSize += length;

    return length;
}






uint16_t ETH_WriteBlock(const void* data, uint16_t length)
{
    char *p = (char*)data;
    TXPacketSize += length;
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wbm_inst);
    while (length--) {
        spi_exchangeByte(*p++);
    }
    do{LATC2 = 1;} while(0);

    return length;
}






uint16_t ETH_GetFreeTxBufferSize(void)
{
    return (uint16_t)((0x1FFF) - ENC28_Rcr16(J60_EWRPTL));
}
# 659 "mcc_generated_files/TCPIPLibrary/ENC28J60.c"
error_msg ETH_WriteStart(const mac48Address_t *dest_mac, uint16_t type)
{
    if(ethData.bufferBusy)
    {
        return BUFFER_BUSY;
    }

    if((ENC28_Rcr8(J60_ECON1) & 0x08))
    {
        return TX_LOGIC_NOT_IDLE;
    }

    ENC28_Wcr16(J60_ETXSTL, (0x1FFF - (1500)));
    ENC28_Wcr16(J60_EWRPTL, (0x1FFF - (1500)));

    TXPacketSize = 0;
    do{LATC2 = 0;} while(0);
    spi_exchangeByte(wbm_inst);
    spi_exchangeByte(Control_Byte);
    spi_exchangeByte(dest_mac->mac_array[0]);
    spi_exchangeByte(dest_mac->mac_array[1]);
    spi_exchangeByte(dest_mac->mac_array[2]);
    spi_exchangeByte(dest_mac->mac_array[3]);
    spi_exchangeByte(dest_mac->mac_array[4]);
    spi_exchangeByte(dest_mac->mac_array[5]);
    spi_exchangeByte(eth_MAC->mac_array[0]);
    spi_exchangeByte(eth_MAC->mac_array[1]);
    spi_exchangeByte(eth_MAC->mac_array[2]);
    spi_exchangeByte(eth_MAC->mac_array[3]);
    spi_exchangeByte(eth_MAC->mac_array[4]);
    spi_exchangeByte(eth_MAC->mac_array[5]);
    spi_exchangeByte(type >> 8);
    spi_exchangeByte(type & 0x0FF);
    do{LATC2 = 1;} while(0);
    TXPacketSize += 15;
    ethData.bufferBusy = 1;

    return SUCCESS;
}





error_msg ETH_Send(void)
{
    ENC28_Wcr16(J60_ETXNDL,(0x1FFF - (1500))+TXPacketSize);
    if (!ethData.up)
    {
        return LINK_NOT_FOUND;
    }
    if(!ethData.bufferBusy)
    {
        return BUFFER_BUSY;
    }
    ENC28_Bfs(J60_ECON1, 0x08);
    ethData.bufferBusy = 0;

    return SUCCESS;
}






void ETH_Dump(uint16_t length)
{
    uint16_t newRXTail;

    length = (rxPacketStatusVector.byteCount <= length) ? rxPacketStatusVector.byteCount : length;
    if (length) {;
        newRXTail = ENC28_Rcr16(J60_ERDPTL);
        newRXTail += length;

        ENC28_Wcr16(J60_ERDPTL, newRXTail);

        rxPacketStatusVector.byteCount -= length;
    }
}




void ETH_Flush(void)
{
    ethData.pktReady = 0;
    if (nextPacketPointer == (0))ENC28_Wcr16(J60_ERXRDPTL, ((0x1FFF - (1500)) - 2));
            else ENC28_Wcr16(J60_ERXRDPTL,nextPacketPointer-1);
    ENC28_Wcr16(J60_ERDPTL, nextPacketPointer);

    ENC28_Bfs(J60_ECON2, 0x40);
}







void ETH_Insert(uint8_t *data, uint16_t len, uint16_t offset)
{
    uint16_t current_tx_pointer = 0;
    offset+=sizeof(Control_Byte);

    current_tx_pointer = ENC28_Rcr16(J60_EWRPTL);
    ENC28_Wcr16(J60_EWRPTL, (0x1FFF - (1500))+offset);
    while (len--)
    {
         do{LATC2 = 0;} while(0);
         spi_exchangeByte(wbm_inst);
         spi_exchangeByte(*data++);
         do{LATC2 = 1;} while(0);
    }
    ENC28_Wcr16(J60_EWRPTL, current_tx_pointer);
}






error_msg ETH_Copy(uint16_t len)
{
    uint16_t tx_buffer_address;
    uint16_t timer;
    uint16_t temp_len;

    timer = 2 * len;

    while ((ENC28_Rcr8(J60_ECON1) & 0x20) != 0 && --timer) __nop();

    if((ENC28_Rcr8(J60_ECON1) & 0x20) == 0)
    {
        tx_buffer_address = ENC28_Rcr16(J60_EWRPTL);

        ENC28_Wcr16(J60_EDMADSTL, tx_buffer_address);
        ENC28_Wcr16(J60_EDMASTL, ENC28_Rcr16(J60_ERDPTL));

        tx_buffer_address += len;
        temp_len = ENC28_Rcr16(J60_ERDPTL) + len;

        if(temp_len > ((0x1FFF - (1500)) - 2))
        {
            temp_len = temp_len - (((0x1FFF - (1500)) - 2)) + (0);
            ENC28_Wcr16(J60_EDMANDL, temp_len);
        }else
        {
            ENC28_Wcr16(J60_EDMANDL, temp_len);
        }


        ENC28_Bfc(J60_ECON1, 0x10);

        ENC28_Bfs(J60_ECON1, 0x20);
        timer = 40 * len;
        while ((ENC28_Rcr8(J60_ECON1) & 0x20)!=0 && --timer) __nop();
        if((ENC28_Rcr8(J60_ECON1) & 0x20)==0)
        {

            ENC28_Wcr16(J60_EWRPTL, tx_buffer_address);

            TXPacketSize += len;
            return SUCCESS;
        }
    }
    __asm("reset");
    return DMA_TIMEOUT;
}


static uint16_t ETH_ComputeChecksum(uint16_t len, uint16_t seed)
{
    uint32_t cksm;
    uint16_t v;

    cksm = seed;

    while(len > 1)
    {
        v = 0;
        ((char *)&v)[1] = ETH_Read8();
        ((char *)&v)[0] = ETH_Read8();
        cksm += v;
        len -= 2;
    }

    if(len)
    {
        v = 0;
        ((char *)&v)[1] = ETH_Read8();
        ((char *)&v)[0] = 0;
        cksm += v;
    }


    while(cksm >> 16)
    {
        cksm = (cksm & 0x0FFFF) + (cksm>>16);
    }


    cksm = ~cksm;


    return cksm;
}







uint16_t ETH_TxComputeChecksum(uint16_t position, uint16_t length, uint16_t seed)
{
    uint32_t cksm;


    position+= sizeof(Control_Byte);

    while ((ENC28_Rcr8(J60_ECON1) & 0x20) != 0);

    ENC28_Wcr16(J60_EDMASTL, ((0x1FFF - (1500)) + position));
    ENC28_Wcr16(J60_EDMANDL, (0x1FFF - (1500)) + position + (length-1));

    if (!(ENC28_Rcr8(J60_ECON1) & 0x10))
    {

        ENC28_Bfs(J60_ECON1, 0x30);
        while ((ENC28_Rcr8(J60_ECON1) & 0x20) != 0);
        ENC28_Bfc(J60_ECON1,0x10);

        cksm = ENC28_Rcr16(J60_EDMACSL);
        if (seed)
        {
            seed=~(seed);
            cksm+=seed;
            while(cksm >> 16)
            {
                cksm = (cksm & 0x0FFFF) + (cksm>>16);
            }
        }
        cksm = ((((uint16_t)cksm & (uint16_t)0xFF00) >> 8) | (((uint16_t)cksm & (uint16_t)0x00FF) << 8));
    }
    return cksm;
}







uint16_t ETH_RxComputeChecksum(uint16_t len, uint16_t seed)
{
    uint16_t rxptr;
    uint32_t cksm;


    rxptr = ENC28_Rcr16(J60_ERDPTL);;

    cksm = ETH_ComputeChecksum( len, seed);


     ENC28_Wcr16(J60_ERDPTL,rxptr);


    return ((cksm & 0xFF00) >> 8) | ((cksm & 0x00FF) << 8);
}





void ETH_GetMAC(uint8_t *macAddr)
{
   *macAddr++ = ENC28_Rcr8(J60_MAADR1);
    *macAddr++ = ENC28_Rcr8(J60_MAADR2);
    *macAddr++ = ENC28_Rcr8(J60_MAADR3);
    *macAddr++ = ENC28_Rcr8(J60_MAADR4);
    *macAddr++ = ENC28_Rcr8(J60_MAADR5);
    *macAddr++ = ENC28_Rcr8(J60_MAADR6);
}





void ETH_SetMAC(uint8_t *macAddr)
{
    ENC28_Wcr8(J60_MAADR1, *macAddr++ );
    ENC28_Wcr8(J60_MAADR2, *macAddr++ );
    ENC28_Wcr8(J60_MAADR3, *macAddr++ );
    ENC28_Wcr8(J60_MAADR4, *macAddr++ );
    ENC28_Wcr8(J60_MAADR5, *macAddr++ );
    ENC28_Wcr8(J60_MAADR6, *macAddr++ );
}

void ETH_SaveRDPT(void)
{
    ethData.saveRDPT = ENC28_Rcr16(J60_ERDPTL);
}

uint16_t ETH_GetReadPtr(void)
{
    return ENC28_Rcr16(J60_ERDPTL);
}

void ETH_SetReadPtr(uint16_t rdptr)
{
   ENC28_Wcr16(J60_ERDPTL, rdptr);
}

void ETH_MoveBackReadPtr(uint16_t offset)
{
    uint16_t rdptr;

    rdptr = ENC28_Rcr16(J60_ERDPTL);
    ENC28_Wcr16(J60_ERDPTL, rdptr-offset);
    ETH_SetRxByteCount(offset);

}

void ETH_ResetReadPtr()
{
    ENC28_Wcr16(J60_ERDPTL, (0));
}

uint16_t ETH_GetWritePtr()
{
    return ENC28_Rcr16(J60_EWRPTL);
}

uint16_t ETH_GetRxByteCount()
{
    return (rxPacketStatusVector.byteCount);
}

void ETH_SetRxByteCount(uint16_t count)
{
    rxPacketStatusVector.byteCount += count;
}

void ETH_ResetByteCount(void)
{
    ethData.saveWRPT = ENC28_Rcr16(J60_EWRPTL);
}

uint16_t ETH_GetByteCount(void)
{
     uint16_t wptr;

    wptr = ENC28_Rcr16(J60_EWRPTL);

    return (wptr - ethData.saveWRPT);
}

void ETH_SaveWRPT(void)
{
    ethData.saveWRPT = ENC28_Rcr16(J60_EWRPTL);
}

uint16_t ETH_ReadSavedWRPT(void)
{
    return ethData.saveWRPT;
}

uint16_t ETH_GetStatusVectorByteCount(void)
{
    return(rxPacketStatusVector.byteCount);
}

void ETH_SetStatusVectorByteCount(uint16_t bc)
{
    rxPacketStatusVector.byteCount=bc;
}

void ETH_TxReset(void)
{
    uint8_t econ1;
    econ1 = ENC28_Rcr8(J60_ECON1);

    ENC28_Wcr8(J60_ECON1,(econ1 | 0x08));

    ethData.bufferBusy = 0;
    ETH_ResetByteCount();

    ENC28_Wcr16(J60_ETXSTL, (0x1FFF - (1500)));
    ENC28_Wcr16(J60_EWRPTL, (0x1FFF - (1500)));

}
