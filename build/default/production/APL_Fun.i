# 1 "APL_Fun.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/opt/microchip/xc8/v2.20/pic/include/language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "APL_Fun.c" 2








# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/stddef.h" 1 3



# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/musl_xc8.h" 1 3
# 5 "/opt/microchip/xc8/v2.20/pic/include/c99/stddef.h" 2 3
# 19 "/opt/microchip/xc8/v2.20/pic/include/c99/stddef.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 18 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long int wchar_t;
# 122 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned size_t;
# 132 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long ptrdiff_t;
# 168 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef __int24 int24_t;
# 204 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef __uint24 uint24_t;
# 20 "/opt/microchip/xc8/v2.20/pic/include/c99/stddef.h" 2 3
# 10 "APL_Fun.c" 2
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/string.h" 1 3
# 10 "/opt/microchip/xc8/v2.20/pic/include/c99/string.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/features.h" 1 3
# 11 "/opt/microchip/xc8/v2.20/pic/include/c99/string.h" 2 3
# 25 "/opt/microchip/xc8/v2.20/pic/include/c99/string.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 411 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef struct __locale_struct * locale_t;
# 26 "/opt/microchip/xc8/v2.20/pic/include/c99/string.h" 2 3

void *memcpy (void *restrict, const void *restrict, size_t);
void *memmove (void *, const void *, size_t);
void *memset (void *, int, size_t);
int memcmp (const void *, const void *, size_t);
void *memchr (const void *, int, size_t);

char *strcpy (char *restrict, const char *restrict);
char *strncpy (char *restrict, const char *restrict, size_t);

char *strcat (char *restrict, const char *restrict);
char *strncat (char *restrict, const char *restrict, size_t);

int strcmp (const char *, const char *);
int strncmp (const char *, const char *, size_t);

int strcoll (const char *, const char *);
size_t strxfrm (char *restrict, const char *restrict, size_t);

char *strchr (const char *, int);
char *strrchr (const char *, int);

size_t strcspn (const char *, const char *);
size_t strspn (const char *, const char *);
char *strpbrk (const char *, const char *);
char *strstr (const char *, const char *);
char *strtok (char *restrict, const char *restrict);

size_t strlen (const char *);

char *strerror (int);
# 65 "/opt/microchip/xc8/v2.20/pic/include/c99/string.h" 3
char *strtok_r (char *restrict, const char *restrict, char **restrict);
int strerror_r (int, char *, size_t);
char *stpcpy(char *restrict, const char *restrict);
char *stpncpy(char *restrict, const char *restrict, size_t);
size_t strnlen (const char *, size_t);
char *strdup (const char *);
char *strndup (const char *, size_t);
char *strsignal(int);
char *strerror_l (int, locale_t);
int strcoll_l (const char *, const char *, locale_t);
size_t strxfrm_l (char *restrict, const char *restrict, size_t, locale_t);




void *memccpy (void *restrict, const void *restrict, int, size_t);
# 11 "APL_Fun.c" 2

# 1 "./APL_Fun.h" 1
# 35 "./APL_Fun.h"
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 1 3
# 22 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 1 3
# 127 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long uintptr_t;
# 142 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long intptr_t;
# 158 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef signed char int8_t;




typedef short int16_t;
# 173 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long int32_t;





typedef long long int64_t;
# 188 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef long long intmax_t;





typedef unsigned char uint8_t;




typedef unsigned short uint16_t;
# 209 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long uint32_t;





typedef unsigned long long uint64_t;
# 229 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/alltypes.h" 3
typedef unsigned long long uintmax_t;
# 23 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3

typedef int8_t int_fast8_t;

typedef int64_t int_fast64_t;


typedef int8_t int_least8_t;
typedef int16_t int_least16_t;

typedef int24_t int_least24_t;

typedef int32_t int_least32_t;

typedef int64_t int_least64_t;


typedef uint8_t uint_fast8_t;

typedef uint64_t uint_fast64_t;


typedef uint8_t uint_least8_t;
typedef uint16_t uint_least16_t;

typedef uint24_t uint_least24_t;

typedef uint32_t uint_least32_t;

typedef uint64_t uint_least64_t;
# 139 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 3
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/bits/stdint.h" 1 3
typedef int32_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef uint32_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
# 140 "/opt/microchip/xc8/v2.20/pic/include/c99/stdint.h" 2 3
# 36 "./APL_Fun.h" 2
# 1 "/opt/microchip/xc8/v2.20/pic/include/c99/stdbool.h" 1 3
# 37 "./APL_Fun.h" 2

void Clear_Apps(void);
void Swap_App1_to_App(void);
void ReadRTCC(void);
void RTCC_Set(uint8_t*);
void RTCC_Get(void);

void Search_Control(void);
void Put_Base(void);

uint8_t Put_BufferRx(void);
void Clear_BufferRx(void);
_Bool Resp_MB_NORM(void);
_Bool Resp_MB_ASOC(void);
_Bool Serv_Buff_stat(void);

_Bool Put_Buffer_TcpIp(void);
_Bool Get_BufferRx_TcpIp(void);
_Bool Put_Buffer_MB_ASO(void);


_Bool Get_Tipo2_BufferRx_MB(void);
_Bool Get_Buffer_TcpIp(void);
# 13 "APL_Fun.c" 2
# 1 "./MIWI.h" 1
# 24 "./MIWI.h"
typedef enum State_t_Def{
    RECB=0,
    TRANS_W
}State_t;


void Ini_MIWI(void);
_Bool FunRx(void);
_Bool FunTx(void);

_Bool Serv_MB(State_t);
# 14 "APL_Fun.c" 2
# 1 "./App.h" 1
# 17 "./App.h"
typedef struct DATE_t_Def
    {
        uint8_t Seg;
        uint8_t Min;
        uint8_t Hou;
        uint8_t Day;
        uint8_t Mon;
        uint8_t Yea;
    }DATE_t;

    typedef struct DATERE_t_Def
    {
        uint8_t Hou;
        uint8_t Day;
        uint8_t Mon;
    }DATERE_t;

    typedef struct DATECons_t_Def
    {

        uint8_t Seg;
        uint8_t Min;
        uint8_t Hou;

    }DATERECons_t;

typedef struct MED_t_Def
    {
        DATERE_t DateMesu;
        uint16_t Mesure;
    }MED_t;

typedef struct APP_t_Def
    {
        uint16_t ID;
        uint8_t Tipo;
        uint8_t Secuencia;
        uint8_t Nro_intentos;
        uint8_t Noise;
        uint8_t Signal;
        uint8_t Bat;
        uint16_t Slot;
        DATE_t DateTx;
        MED_t MedidaRE;
        uint8_t MedidaDif[24];
        uint8_t SlotDif;

    }APP_t;


    typedef struct APP_t_Cons_Def
    {
        uint16_t ID;
        uint8_t Tipo;
        uint8_t Secuencia;
        uint8_t Nro_intentos;
        uint8_t Noise;
        uint8_t Signal;
        uint8_t Bat;
        uint16_t Slot;
        DATERECons_t DateTx;
        MED_t MedidaRE;
        uint8_t MedidaDif[24];

    }APP_t_Cons;

 typedef struct APP_t_Cons_Complet_Def{
     APP_t_Cons Buffer[15];
     uint8_t point;
     uint8_t send_point;

 }APP_t_Cons_Complet;

 typedef struct APP1_t_Def
    {
        uint16_t ID;
        uint8_t Tipo;
        uint8_t Secuencia;
        uint16_t Slot;

    }APP1_t;

 typedef struct APP_t_Buffer_Def
    {
        _Bool Sent;
        _Bool Rece;
        uint8_t Sec_GW;
        uint16_t ID;

        APP1_t Element;

    }Gen_Buffer_t;





extern APP_t App;
extern APP_t App1;
extern uint8_t AddMIWI[8];

extern APP_t_Cons_Complet BufferRX;

extern uint16_t BaseSlot[50][2];

extern Gen_Buffer_t Buffer[6];


extern char Pass[10];
extern char Pass_AU[10];


extern _Bool Buff_Rx_MB;
extern _Bool Sem_TCP_DONE;
extern _Bool Sem_MB_DONE;
extern _Bool Sem_ModBus_Free;
extern _Bool Sem_Free_App;
extern _Bool Sem_Free_App1;


extern uint16_t Id_GW;
extern uint16_t Id_GW_AU;
extern uint8_t GW_Secuencia;
extern DATE_t GW_RTCC;
# 15 "APL_Fun.c" 2



void Clear_Apps(void){

    App.ID=0;
    App.Tipo=0;;
    App.Secuencia=0;
    App.Nro_intentos=0;
    App.Noise=0;
    App.Signal=0;
    App.Bat=0;
    App.Slot=0;
    App.DateTx.Day=0;
    App.DateTx.Hou=0;
    App.DateTx.Min=0;
    App.DateTx.Mon=0;
    App.DateTx.Seg=0;
    App.DateTx.Yea=0;
    App.MedidaRE.Mesure=0;
    App.MedidaRE.DateMesu.Day=0;
    App.MedidaRE.DateMesu.Mon=0;
    App.MedidaRE.DateMesu.Hou=0;
    App.MedidaDif[0]=0;

    App.SlotDif=0;


    App1.ID=0;
    App1.Tipo=0;;
    App1.Secuencia=0;
    App1.Nro_intentos=0;
    App1.Noise=0;
    App1.Signal=0;
    App1.Bat=0;
    App1.Slot=0;
    App1.DateTx.Day=0;
    App1.DateTx.Hou=0;
    App1.DateTx.Min=0;
    App1.DateTx.Mon=0;
    App1.DateTx.Seg=0;
    App1.DateTx.Yea=0;
    App1.MedidaRE.Mesure=0;
    App1.MedidaRE.DateMesu.Day=0;
    App1.MedidaRE.DateMesu.Mon=0;
    App1.MedidaRE.DateMesu.Hou=0;
    App1.MedidaDif[0]=0;

    App1.SlotDif=0;

};

void Swap_App1_to_App(void){

    uint8_t *p=((void*)0),*p1=((void*)0),i;
    static uint8_t j=0;

    p=&App1.Tipo;
    p1=&App.Tipo;

    App.ID=App1.ID;

    for(i=0;i<6;i++){
        *p1=*p;
        p++;
        p1++;
    }

    BufferRX.Buffer[j].Slot=App1.Slot;

    p=&App1.DateTx.Seg;
    p1=&BufferRX.Buffer[j].DateTx.Seg;

    for(i=0;i<6;i++){
        *p1=*p;
        p++;
        p1++;
    }

    p=&App1.MedidaRE.DateMesu.Hou;
    p1 =&(BufferRX.Buffer[j].MedidaRE.DateMesu.Hou);


        for (i = 0; i < 3; i++) {
            *p1= *p ;
            p++;
            p1++;
        }

    for (i = 0; i < 24; i++) {

        BufferRX.Buffer[j].MedidaDif[i]=App1.MedidaDif[i];

    }

}

void ReadRTCC(void){
    return;
}
void RTCC_Set(uint8_t* p){
    return;
}

void RTCC_Get(void){
    uint8_t *p1,*p,i;
    ReadRTCC();
    p1=& GW_RTCC.Seg;
    p=&App1.DateTx.Seg;

    for(i=0;i<6;i++){
        *p=*p1;
        p++;
        p1++;
    }

    return;
}

void Search_Control(void){

    uint8_t i=0;


    while(BaseSlot[i][0]!=App1.ID && i<(50 -1) && BaseSlot[i][0]!=0 )
        i++;

    if (BaseSlot[i][0]!=App1.ID) {
        App1.Slot=0;
        return;
    }

    App1.Slot=BaseSlot[i][1];



    while(i<49){

        BaseSlot[i][0]=BaseSlot[i+1][0];
        BaseSlot[i][1]=BaseSlot[i+1][1];
        i++;
    }
    BaseSlot[i][0]=0;
    BaseSlot[i][1]=0;


    return;

}
void Put_Base(void){

    uint8_t i=0;

    while(BaseSlot[i][0]!=0 && i<(50 -1))
        i++;

    if (BaseSlot[i][0]==0) {

        BaseSlot[i][0]=App.ID;
        BaseSlot[i][1]=App.Slot;

    }

}



uint8_t Put_BufferRx(void) {

    uint8_t *p=((void*)0),*p1=((void*)0),i,j;
    static _Bool s=1;

    if (s){
        BufferRX.point=0;
        s--;
    }

    j=BufferRX.point;

    BufferRX.Buffer[j].ID=App1.ID;

    p=&App1.Tipo;
    p1=&BufferRX.Buffer[j].Tipo;

    for(i=0;i<6;i++){
        *p1=*p;
        p++;
        p1++;
    }

    BufferRX.Buffer[j].Slot=App1.Slot;

    BufferRX.Buffer[j].DateTx.Seg=App1.DateTx.Seg;
    BufferRX.Buffer[j].DateTx.Min=App1.DateTx.Min;
    BufferRX.Buffer[j].DateTx.Hou=App1.DateTx.Hou;
# 220 "APL_Fun.c"
    BufferRX.Buffer[j].MedidaRE.Mesure=App1.MedidaRE.Mesure;
    BufferRX.Buffer[j].MedidaRE.DateMesu.Hou=App1.MedidaRE.DateMesu.Hou;
    BufferRX.Buffer[j].MedidaRE.DateMesu.Day=App1.MedidaRE.DateMesu.Day;
    BufferRX.Buffer[j].MedidaRE.DateMesu.Mon=App1.MedidaRE.DateMesu.Mon;

    p=App1.MedidaDif;
    p1=BufferRX.Buffer[j].MedidaDif;

    for (i = 0; i < 24; i++) {
        *p1= *p;
        p++;
        p1++;
    }

    return(BufferRX.point++);

}

void Clear_BufferRx(void){

    uint8_t *p=((void*)0),*p1=((void*)0),i,d;
    static uint8_t j=0;

    d=BufferRX.send_point+1;

    while (BufferRX.Buffer[j+d].Tipo!=0 && j<=BufferRX.send_point && (j+d)<15){

        BufferRX.Buffer[j].ID=BufferRX.Buffer[j+d].ID;



        p=&BufferRX.Buffer[j].Tipo;
        p1=&BufferRX.Buffer[j+d].Tipo;

        for(i=0;i<6;i++){
            *p=*p1;
            p++;
            p1++;
        }

        BufferRX.Buffer[j].DateTx.Seg=BufferRX.Buffer[j+d].DateTx.Seg;
        BufferRX.Buffer[j].DateTx.Min=BufferRX.Buffer[j+d].DateTx.Min;
        BufferRX.Buffer[j].DateTx.Hou=BufferRX.Buffer[j+d].DateTx.Hou;

        BufferRX.Buffer[j].MedidaRE.DateMesu.Hou=BufferRX.Buffer[j+d].MedidaRE.DateMesu.Hou;
        BufferRX.Buffer[j].MedidaRE.DateMesu.Day=BufferRX.Buffer[j+d].MedidaRE.DateMesu.Day;
        BufferRX.Buffer[j].MedidaRE.DateMesu.Mon=BufferRX.Buffer[j+d].MedidaRE.DateMesu.Mon;

        BufferRX.Buffer[j].MedidaRE.Mesure=BufferRX.Buffer[j+d].MedidaRE.Mesure;

        BufferRX.Buffer[j].Slot=BufferRX.Buffer[j+d].Slot;




        p=App1.MedidaDif;
        p1=BufferRX.Buffer[j].MedidaDif;


        for (i = 0; i < 24; i++) {
                *p= *p1 ;
                p++;
                p1++;
        }

        BufferRX.Buffer[j+d].ID=0;
        BufferRX.Buffer[j+d].Tipo=0;

        j++;
    }

    BufferRX.point=j;

    Buff_Rx_MB=0;
}

_Bool Resp_MB_NORM(void){

    static _Bool s=1;

    if (s){
        s--;
        RTCC_Get();
        Search_Control();
    }
    if (Serv_MB(TRANS_W)){
        s++;
        return(1);
    }
    return(0);
}

_Bool Resp_MB_ASOC(void){
    static uint8_t j=0;
    static _Bool s=1;

    if(s){
        while(j<6){
            if (Buffer[j].Sent && Buffer[j].Rece && (Buffer[j].ID) ){

                s--;
                App1.ID=Buffer[j].Element.ID;
                App1.Slot=Buffer[j].Element.Slot;
                App1.SlotDif=Buffer[j].ID;




                RTCC_Get();


                Serv_MB(TRANS_W);
                return(0);
            }

            j++;

        }
        if(j==(6)) j=0;

    }else if (Serv_MB(TRANS_W)){

        s++;

        Buffer[j].Element.ID=0;
        Buffer[j].Element.Tipo=0;
        Buffer[j].Element.Slot=0;
        Buffer[j].Element.Secuencia=0;
        Buffer[j].Sec_GW=0;
        Buffer[j].Sent=0;
        Buffer[j].Rece=0;
        Buffer[j].ID=0;


        return(1);
    }

    return(0);
}

_Bool Serv_Buff_stat(void){
    return(0);
}

_Bool Put_Buffer_TcpIp(void){
    uint8_t j=0;
    while(j<6){

        if (Buffer[j].Sent && (Buffer[j].Sec_GW==App.Secuencia) ){

            Buffer[j].ID=App.ID;
            Buffer[j].Element.Slot=App.Slot;
            Buffer[j].Rece=1;
            return(1);
        }
        j++;
    }
    return(0);
}

_Bool Get_BufferRx_TcpIp(void){

     return (1);
}
_Bool Put_Buffer_MB_ASO(void){
    uint8_t i=0;
    static uint8_t j=0;

    while (i<6){
        if (App1.ID==Buffer[i].Element.ID && Buffer[i].Rece==0){

            Buffer[i].Element.Tipo=2;
            Buffer[i].Element.Slot=0;
            Buffer[i].Element.Secuencia=App1.Secuencia;
            Buffer[i].Sec_GW=0;
            Buffer[i].Sent=0;
            Buffer[i].Rece=0;
            Buffer[i].ID=0;


            return(1);
        }

        i++;
    }
    i=0;

    while (i<6){
        if (App1.ID==Buffer[i].Element.ID && Buffer[i].Rece){


            Buffer[i].Element.Secuencia=App1.Secuencia;

            return(1);
        }
        i++;
    }
    i=0;

    while (i<6){

        if (!Buffer[i].Element.ID){

            Buffer[i].Element.ID=App1.ID;
            Buffer[i].Element.Tipo=2;
            Buffer[i].Element.Slot=0;
            Buffer[i].Element.Secuencia=App1.Secuencia;
            Buffer[i].Sec_GW=0;
            Buffer[i].Sent=0;
            Buffer[i].Rece=0;
            Buffer[i].ID=0;


            return(1);
        }

        i++;
    }


    i=0;
    while (i<(6 -3)){
        if (!Buffer[i].Rece && Buffer[i].Sent ){

            Buffer[i].Element.ID=App1.ID;
            Buffer[i].Element.Tipo=2;
            Buffer[i].Element.Slot=0;
            Buffer[i].Element.Secuencia=App1.Secuencia;
            Buffer[i].Sec_GW=0;
            Buffer[i].Sent=0;
            Buffer[i].Rece=0;
            Buffer[i].ID=0;


            return(1);
        }
        i++;
    }

    Buffer[j].Element.ID=App1.ID;
    Buffer[j].Element.Tipo=2;
    Buffer[j].Element.Slot=0;
    Buffer[j].Element.Secuencia=App1.Secuencia;
    Buffer[j].Sec_GW=0;
    Buffer[j].Sent=0;
    Buffer[j].Rece=0;
    Buffer[i].ID=0;




    if (j<(6 -1))j++;
        else j=0;

    return(0);
}
# 538 "APL_Fun.c"
_Bool Get_Tipo2_BufferRx_MB(void){
    uint8_t i=0;

    while(i<6){

        if (Buffer[i].Element.Tipo==2 && Buffer[i].ID && Buffer[i].Rece ){

            return(1);
        }
        i++;
    }
    return(0);
}

_Bool Get_Buffer_TcpIp(void){

    uint8_t i=0;
    while (i<6){

        if (!Buffer[i].Sent && (Buffer[i].Element.ID!=0) ){

            App.ID=Buffer[i].Element.ID;
            App.Tipo=2;
            Buffer[i].Sec_GW=GW_Secuencia;
            Buffer[i].Sent=1;
            return(1);
        }
        i++;
    }
    return(0);
}
