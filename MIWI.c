#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "system.h"
//#include "gtypedefs.h"
//#include "symbol.h"
//#include "delay.h"

//variables globales
#include "App.h"
//App que intercambia tramas modbus para presenar a Miwi 
#include "APP_MIWI_ModBus.h"
//servidor maestro modbus que se encarga de establecer la 
//comunicacion entre los diferentes dispositivos de la red modbus
#include "Modbus_M.h"
//Se encarga de establecer la conexion administras las secuencias entre PAM y el gateWay
#include "MIWI.h"

#define VER_MAJOR  2
#define VER_MINOR  0

//Can only be used on structures, due to address misalignment issues on 16bit and 32bit platforms
#define u16(a)  *(uint16_t*)(&a)
// Defines  /////////////////////////////////////
#define lowNipple(x)     ((uint8_t)((x)&0x0F))
#define highNipple(x)    ((uint8_t)(((x)>>4)&0x0F))
#define halfnippleToByte(hn,ln) ( (((hn)&0x0F)<<4) | (highNipple(ln))) 

#define lowByte(x)     ((uint8_t)((x)&0xFF))
#define highByte(x)    ((uint8_t)(((x)>>8)&0xFF))
#define lowWord(x)     ((uint16_t)((x)&0xFFFF))
#define highWord(x)    (((uint16_t)(((x)>>16)&0xFFFF)))
#define wordstoDWord(hw,lw) ((((uint16_t)(hw&0xFFFF))<<16) | ((uint16_t)lw))
#define bitRead(value,bit)  (((value) >> (bit)) & 0x01)
#define bitSet(value,bit)   ((value) |= (1ul << (bit)))
#define bitClear(value,bit) ((value) &= ~(1ul <<(bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value,bit) : bitClear(value,bit))
#define bytesToWord(hb,lb)             ( (((uint16_t)(hb&0xFF))<<8) | ((uint16_t)lb) )
#define Float_CD_AB    //else it is Float_CD_AB

void Ini_MIWI(void)
{
    
    
}    

bool FunRx(void)
{
    uint8_t i=0,j,k;
    uint16_t A;
    //uint8_t *p;    
    //DIRECCION ID O MAC en la asociacion
    //llama al main de ModbusM la secuencia es:
    /*primero: consulta
      segundo: llegada y espera de datos
      Tercero: recepcion
      cuarto: presentacion de buffer modbus a capa superior miwi almaceno N tramas entrantes
      quinto: Trasmitir datos de capa superio
      Sexto: escucchar recepcion de ack
    */
    if(State_Put==Serv_Master_Modbus()){
        /* caso que haya trama valida recibida modbus*/
        //servicio que define una trama valida recibida para almacenar y no un ACK
        
        //---------------almaceno en un buffer los datos recibidos------------//
        if (Rx_Buffer_MODMIWI.point <Length_buffer_RX){
            do{ 
                Rx_Buffer_MODMIWI.Message[Rx_Buffer_MODMIWI.point].Payload[i] =modbus_rx.data[i];
                i++;
            }while (i<modbus_rx.len);
            Rx_Buffer_MODMIWI.point++;
        }
        
        //---------vacio el registro modbus_rx----------------------------------
        Modbus_DiscardMessage();
        //----------------------------------------------------------------------
        
        if (k>0) k=(uint8_t)(Rx_Buffer_MODMIWI.point -1);
        //A=(uint16_t) Rx_Buffer_MODMIWI.Message[k].Payload[0];
        //A=A<<8;
        App1.ID=bytesToWord(Rx_Buffer_MODMIWI.Message[k].Payload[0],Rx_Buffer_MODMIWI.Message[k].Payload[1]);
        App1.Tipo=highNipple(Rx_Buffer_MODMIWI.Message[k].Payload[2]);    

        if (App1.Tipo==1 ||App1.Tipo==2 ){

            App1.Secuencia=(uint8_t)halfnippleToByte(Rx_Buffer_MODMIWI.Message[k].Payload[2],Rx_Buffer_MODMIWI.Message[k].Payload[3]);
            if (App1.Tipo==2){
                //debe reflejar el numero de intentos de trasnmision    
                //App1.Secuencia=rxMessage[0].Payload[3]; 
                return(true);
            }    
                        
            //p=&App1.Secuencia;        
            //for(i=3;i<8;i++)
            //    *p++=Rx_Buffer_MODMIWI.Message[k].Payload[i];
            
            App1.Nro_intentos=lowNipple(Rx_Buffer_MODMIWI.Message[k].Payload[3]);
            App1.Noise=highNipple(Rx_Buffer_MODMIWI.Message[k].Payload[4]);
            App1.Signal=lowNipple(Rx_Buffer_MODMIWI.Message[k].Payload[4]);
            App1.Bat=Rx_Buffer_MODMIWI.Message[k].Payload[5];
            App1.Slot=bytesToWord(Rx_Buffer_MODMIWI.Message[k].Payload[6],Rx_Buffer_MODMIWI.Message[k].Payload[7]);
            //-----------------------------------------------------------
            /*MiApp_WriteData(App.T);                                       //tipo de trama 8 bits
            //-----------------------------------------------------------
            MiApp_WriteData(App.Sec);                                       //Secuencia 8bits
            //----------------------------------------------------------- 

            MiApp_WriteData(App.N);                                         //intentos de transmision 8 bits   
            //----------------------------------------------------------- 
            MiApp_WriteData(App.Noise);                                     //calidad de ambiente  8bits
            MiApp_WriteData(App.Singnal);                                   //calidad de ambiente  8bits
            //----------------------------------------------------------- 
            MiApp_WriteData(App.Bat);                                       //bateria 8bits      
            */
            //A=(uint16_t)rxMessage[0].Payload[i++];                             //i=8
            //A=A<<8;
            //App1.Slot=A|(uint16_t)rxMessage[0].Payload[i++];                   //i=9
            
            App1.DateTx.Seg=Rx_Buffer_MODMIWI.Message[k].Payload[8];                         //i=10
            App1.DateTx.Min=Rx_Buffer_MODMIWI.Message[k].Payload[9];                         //seg,min

            /*MiApp_WriteData(App.Slot);                                    //ranura de transmision
            MiApp_WriteData(App.DateTx);                                    //tiempo de transmision
            */
            App1.MedidaRE.Mesure=bytesToWord(Rx_Buffer_MODMIWI.Message[k].Payload[10],Rx_Buffer_MODMIWI.Message[k].Payload[11]);
             //Hou,Day,Mon
            App1.MedidaRE.DateMesu.Hou=Rx_Buffer_MODMIWI.Message[k].Payload[12];
            App1.MedidaRE.DateMesu.Day=Rx_Buffer_MODMIWI.Message[k].Payload[13]; 
            App1.MedidaRE.DateMesu.Mon=Rx_Buffer_MODMIWI.Message[k].Payload[14]; 
            
            for (j= 0; j < Tomas; j++) { //23                               //ultima version va un index o puntero no tomas                    
                //A=(uint16_t)rxMessage[0].Payload[i++];
                //A=A<<8;
                
                App1.MedidaDif[j]=(uint8_t)(Rx_Buffer_MODMIWI.Message[k].Payload[15+j]);
                //Hou,Day,Mon
                //App1.Medida[j].DateMesu.Hou=rxMessage[0].Payload[i++]; 
                //App1.Medida[j].DateMesu.Day=rxMessage[0].Payload[i++]; 
                //App1.Medida[j].DateMesu.Mon=rxMessage[0].Payload[i++]; 
            }
            //TOTAL BYTES ENVIADOS 14+ TOMAS =APROX 38 QUEDAN {127-HEADRER}= XBEE_PAYLOAD
        }   
            return(true);
    }
    
    App1.Tipo=0;
    App1.ID=0;
    return(false);
    
}
//------------------------------------------------------------------------------
/*!
  Function:
	BOOL FunTx(void)

 @brief  Description:
	Writes a single byte to a TCP socket.

  Precondition:
	TCP is initialized.

  Parameters:
 @param c1 the first argument.
 @param c2 the second argument.
	hTCP - The socket to which data is to be written.
	byte - The byte to write.
 @return
 Return Values:
	TRUE - The byte was written to the transmit buffer.
	FALSE - The transmit buffer was full, or the socket is not connected.
*/
//------------------------------------------------------------------------------
bool FunTx(void)
{
    static bool s=false,s0=false,s1=false;
    //uint16_t A;
    uint8_t i,k=0;    
    //ROM addr_t coord={0};   
    //memset(coord.bytes,0x00,sizeof(coord));        
    //Serv_Master_Modbus();

    if (!s)
    {        
        k=Tx_Buffer_MODMIWI.point;
        s++;
        //Tx_Buffer_MODMIWI.Message[k].Payload[13]
        //A=App1.ID;
        //A=A>>8; 
        Tx_Buffer_MODMIWI.Message[k].Payload[0]=highByte(App1.ID);
        Tx_Buffer_MODMIWI.Message[k].Payload[1]=lowByte(App1.ID);
        
        Tx_Buffer_MODMIWI.Message[k].Payload[2]=(uint8_t)halfnippleToByte(App1.Tipo,App1.Secuencia);
        Tx_Buffer_MODMIWI.Message[k].Payload[3]=(uint8_t)halfnippleToByte(App1.Secuencia,App1.DateTx.Seg);
        
        Tx_Buffer_MODMIWI.Message[k].Payload[4]=(uint8_t)halfnippleToByte(App1.DateTx.Seg,App1.DateTx.Min);
        Tx_Buffer_MODMIWI.Message[k].Payload[5]=(uint8_t)halfnippleToByte(App1.DateTx.Min,App1.DateTx.Hou);
        Tx_Buffer_MODMIWI.Message[k].Payload[6]=(uint8_t)halfnippleToByte(App1.DateTx.Hou,App1.DateTx.Day);
        Tx_Buffer_MODMIWI.Message[k].Payload[7]=(uint8_t)(((0x0f&App1.DateTx.Day)<<4)|(App1.DateTx.Mon&0x0f)); //cuidado con el mon es de 4 bits 0-15 no bcd
        Tx_Buffer_MODMIWI.Message[k].Payload[8]=App1.DateTx.Yea;
        
        Tx_Buffer_MODMIWI.Message[k].Payload[9]=highByte(App1.Slot);
        Tx_Buffer_MODMIWI.Message[k].Payload[10]=lowByte(App1.Slot);
        
        if(App1.Tipo==2){
            Tx_Buffer_MODMIWI.Message[k].Payload[12]=highByte(App1.SlotDif); //reuso la variable para ID que envia el servidor
            Tx_Buffer_MODMIWI.Message[k].Payload[13]=lowByte(App1.SlotDif);
        }
        //p=&App1.DateTx.Seg;
        //for(i=0;i<6;i++)
        //{    
        //    Tx_Buffer_MODMIWI.Message[k].Payload[4+i]=halfnippleToByte(lowNipple(*p),highNipple(*(p+1)));
        //    p++;
        //    txMessage[0].Payload[i++]=*(p++); //proceso primer paquete 90 bytes en total el buffer
        // }
        //A=App1.Slot;
        //A=A>>8; 
        //Tx_Buffer_MODMIWI.Message[k].Payload[i]=(uint8_t)A;        
    }
    
    /*indica que ya efectuo el envio y recibio ACK paso de State_AcK a State_Req*/
    // Paso de hacer la consulta con State_Req>State_get> hora estoy para transmitir en State_Put >State_ACK luego State_Req
    
    switch (Serv_Master_Modbus())
    {        
        case(State_Put):
                s0=1;
            break;
        
        case(State_Ack):                 
                s1=1;
                s0=0;
            break;
        
        case(State_Req):                
                if (s0)
                    return(true);   //difiero hay algun problema paso de put->Req sin ack
                                    //debo volver a intentar sin eliminar el TXBUFFER                                                                                //dejo el RX-buffer-modmiwi sin cambio lo atiendo en otro momento       
                if (s1)
                {                                                       
                    s--;
                    s1--;
                    if (Tx_Buffer_MODMIWI.point>0) 
                        Rx_Buffer_MODMIWI.point--;
                    // actualizo el puntero
                    //TXpoint pora esta aplicacion es siempre el primero                      
                }
                else 
                    return(false);
            break;  
            
        default:
            return(true);
    }
    //return(ModBus_UnicastAddress(txMessage[0].Adress));  si resulta en una transmision correcta   
    return(false);
}

/*****************************************************************************
*  Function:
*	BOOL Serv_MIWI(void)
*
*  Description:
*	Writes a single byte to a TCP socket.
*
*  Precondition:
*	TCP is initialized.
*
*  Parameters:
* @param c1 the first argument.
* @param c2 the second argument.
*	hTCP - The socket to which data is to be written.
*	byte - The byte to write.
* @return
* Return Values:
*	TRUE - The byte was written to the transmit buffer.
*	FALSE - The transmit buffer was full, or the socket is not connected.
*****************************************************************************/
bool Serv_MB(State_t State){ // DA EVOLUVION AL STACK
//bool Serv_MIWI(void){ // DA EVOLUVION AL STACK
    //DEBO DECIDIR SI TRANSMITO O RECIBO
    //SI USO App1 no puedo discriminar tengo los siguientes casos:
    /* recibomiwi-analizo-respondomiwi. Solucion maquina de estados.
     * reciboTCPIP-???
     */
  
    switch(State){ 
        case RECB:  //Si el PANCO recibio Paquetes
            /*------Consulto al PAN por TRAMAS A RECIBIR verifico el estado el buffer miwi y almaceno las tramas segun corresponda----*/
            
            //if(ModBus_MessageAvailable())// Verifico si exite una trama segun mi protocolo para modbus
            // {
                /*--------------Almaceno el ADDRES--------------------------------------------*/
                //Deberia saber a quien consulto en primer instancia solo un esclavo de haber mas es un broadcast y luego escuchar respuestas aleatorias diferidas
                //uso una ADD modbus valida de la PAN 0000 sin uso
                // 01:master; 02:PAN; 
                // A0:LORA0; 
                // B0:plc0; B1:plc1 ,B2:plc2
                //memcpy(AddMIWI,rxMessage[0].Adress,Length_Add); 
                // Cx:libre 
                /*--------------extraigo campos de la trama----------------------------------*/
                // FUNRX CONSULTA POR MODBUS Y BAJO LAS TRAMAS DE LA PAN 
                if(FunRx()){ //debe haber un estado interno primero solicito por modbus y luego escucho dentro de funrx
                    
                    //Caso de transmicion y recepcion completas
                    Sem_MB_DONE=true; //tomo el recurso para operarlo
                    
                    //Modbus_DiscardMessage(); // Libero buffer si exite una trama segun mi protocolo para modbus Todos loa registos estan en App1 salvados
                    //proximo estado ??
                    //State=TRANS;
                    return(true);
                }
                //Modbus_DiscardMessage();//ya lo hago antes
                         
           // }
                           
        break;
        case TRANS_W:
            //TRANSMITE LOS DATOS DIRECTO DEL PAN AL COOR ENTONCES DEBO EN FUNTX COLOCAR LA TRAMA MIWI EN MODBUS
            //transmito tramas de consulta de datos en el PAN, pueden ser varias o en secuencias segun la aplicacion.
            if(FunTx())
            //proximo estado
            //   State=RECB;
            return(true);
            
        break;    
    }
    return(false);
}