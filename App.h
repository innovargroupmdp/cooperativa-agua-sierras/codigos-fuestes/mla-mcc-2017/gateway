/* 
 * File:   App.h
 * Author: cba
 *
 * Created on February 13, 2018, 4:04 PM
 */

#include <stdint.h>
#include <stdbool.h>

#define Tomas         24
#define AlarmaBuffer  10
#define BufferRX_length 15
#define Bufferlength  6
#define ADDRESS_LEN   8
#define BaseSlot_LEN  50
typedef struct DATE_t_Def      
    {
        uint8_t Seg;
        uint8_t Min;
        uint8_t Hou;
        uint8_t Day;
        uint8_t Mon;
        uint8_t Yea;
    }DATE_t;
    
    typedef struct DATERE_t_Def      
    {    
        uint8_t Hou;
        uint8_t Day;
        uint8_t Mon;    
    }DATERE_t;
    
    typedef struct DATECons_t_Def      
    {
    
        uint8_t Seg;
        uint8_t Min;
        uint8_t Hou;
    
    }DATERECons_t;
    
typedef struct MED_t_Def      
    {
        DATERE_t  DateMesu;
        uint16_t  Mesure;          
    }MED_t;
    
typedef struct APP_t_Def
    {
        uint16_t  ID;
        uint8_t   Tipo; // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
        uint8_t   Secuencia;
        uint8_t   Nro_intentos;
        uint8_t   Noise;
        uint8_t   Signal;
        uint8_t   Bat;
        uint16_t  Slot;
        DATE_t    DateTx;   //tratar de reducir para ajustar el tama�o de buffer solo importa seg min
        MED_t     MedidaRE; //medida ultima
        uint8_t   MedidaDif[Tomas];
        uint8_t   SlotDif;

    }APP_t;
    
    
    typedef struct APP_t_Cons_Def
    {
        uint16_t  ID;
        uint8_t   Tipo; // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
        uint8_t   Secuencia;
        uint8_t   Nro_intentos;
        uint8_t   Noise;
        uint8_t   Signal;
        uint8_t   Bat;        
        uint16_t  Slot;
        DATERECons_t DateTx;   //tratar de reducir para ajustar el tama�o de buffer solo importa seg min
        MED_t     MedidaRE; //medida ultima
        uint8_t   MedidaDif[Tomas];
        
    }APP_t_Cons;
    
 typedef struct APP_t_Cons_Complet_Def{
     APP_t_Cons Buffer[BufferRX_length];
     uint8_t    point;
     uint8_t    send_point;
     
 }APP_t_Cons_Complet;
    
 typedef struct APP1_t_Def
    {
        uint16_t  ID; //MAC
        uint8_t   Tipo; // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
        uint8_t   Secuencia;
        uint16_t  Slot;
        
    }APP1_t;
    
 typedef struct APP_t_Buffer_Def
    { 
        bool     Sent; // indica envio
        bool     Rece; // indica recibido
        uint8_t  Sec_GW;//Secuencia con la que se envio
        uint16_t ID;   // ID de autenticacion del servidor       
        //uint8_t  Add[ADDRESS_LEN];  // Direccion del nodo para retransmitrir solo para PANCO
        APP1_t   Element;// Datos del MEDIDOR
        
    }Gen_Buffer_t;
    
    
    
//APP_t BufferTX[1];
    
extern APP_t App;           //variables de comando o de intercambio TCPIP
extern APP_t App1;          //variables de comando o de intercambio MIWI
extern uint8_t  AddMIWI[ADDRESS_LEN];  //intercambio     
//----Buffer y bases------------------------------
extern APP_t_Cons_Complet BufferRX;  //buffer Recepcion MIWI, por lo gral tramas Norm tipo 1

extern uint16_t BaseSlot[BaseSlot_LEN][2];  //[ID][NEW-Slot] buffer Recepcion MIWI para cambioos de slot

extern Gen_Buffer_t Buffer[Bufferlength]; //buffer recepcion-transmision tcp-ip

//----------------------------------
extern  char Pass[10];       //pass de GW
extern  char Pass_AU[10];    //Pass recibida

//----semaforos------------------------------
extern bool Buff_Rx_MB;   //bandera buffer rx en capacidad de transmision supera limite para envios modo Normal
extern bool Sem_TCP_DONE;   //bandera de APP rx en capacidad de transmision 1 indica libre- 0 ocupado
extern bool Sem_MB_DONE;  //bandera de APP rx en capacidad de transmision 1 indica libre- 0 ocupado
extern bool Sem_ModBus_Free;  //bandera de Modbus libre
extern bool Sem_Free_App;   //bandera de APP regis
extern bool Sem_Free_App1;  //bandera de APP1 regis


extern uint16_t Id_GW;      //Id de GW
extern uint16_t Id_GW_AU;   //Id Recibida
extern uint8_t GW_Secuencia;//Secuencia de transmicion actual 
extern DATE_t GW_RTCC;      //variable de intercambio de RTCC 