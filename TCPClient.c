#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/TCPIPLibrary/tcpv4.h"
#include "mcc_generated_files/TCPIPLibrary/ipv4.h"
#include "mcc_generated_files/TCPIPLibrary/tcpip_config.h"

#include "App.h"
#include "TCP-IP_APPs.h"
#include "TCPClient.h"

sockaddr_in_t remoteSocket;

State_tcp_ip_Def State_tcp_ip=SM_SOCKET_OBTAINED;


char rxdataPort60[length_buffer_RX]={"\0"};
char txdataPort60[length_buffer_TX]={"\0"};

State_tcp_Client_Def TCP_Client(void)
{
    // create the socket for the TCP Client
    static tcpTCB_t port60TCB;
    
    // create the TX and RX Client's buffers
    //static char rxdataPort60[length_buffer_RX]={"\0"};
    //static char txdataPort60[length_buffer_TX]={"\0"};


    static time_t t_client;
    static time_t socketTimeout;
    
    uint16_t rx_len;
    socketState_t socketState;
    //uint16_t pot;
    uint8_t i=0,St_G=0;
    //char strTmp[17];
    //char Swap[100];
    socketState = TCP_SocketPoll(&port60TCB);

    time(&t_client);

    switch(socketState)
    {
        case NOT_A_SOCKET:
            // Inserting and initializing the socket
            TCP_SocketInit(&port60TCB);
            break;
        case SOCKET_CLOSED:
            // if the socket is closed we will try to connect again
            // try to connect once at 2 seconds
            socketTimeout = t_client + 2;
            /*--------------Inserta el buffer Rx en el socket-----------------*/
            TCP_InsertRxBuffer(&port60TCB, (uint8_t*)rxdataPort60,length_buffer_RX); // sizeof(rxdataPort60));
            TCP_Connect(&port60TCB, &remoteSocket);
            break;
        case SOCKET_IN_PROGRESS:
            // if the socket is closed we will try to connect again
            if(t_client >= socketTimeout)
            {
                TCP_Close(&port60TCB);
            }
            break;
        case SOCKET_CONNECTED:
            // implement an echo client over TCP
            // check if the previous buffer was sent
            
            if (State_tcp_ip==SM_SOCKET_OBTAINED){
                
                if(t_client >= socketTimeout)
                {   
                    rxdataPort60[0]='\0';
                    strcat(txdataPort60,"POST ");
                    strcat(txdataPort60, RemoteURL);
                    strcat(txdataPort60," HTTP/1.1\r\nHost: ");
                    strcat(txdataPort60, ServerName);
                    strcat(txdataPort60, "\r\nAccept: application/json\r\nContent-Type: application/json\r\nContent-Length: ");
                    //strcat(txdataPort60, (const char*) "\r\nConnection: keep-alive"); // HTTP 1.0 no persistente
                    //strcat(txdataPort60, (const char*) "\r\nContent-Type: application/json");
                    //strcat(txdataPort60, (const char*) "\r\nContent-Length: \r\n\r\n");
                    // Send the packet: T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: control
                    PutJSON();
                    // Close anuncio al servidor transmition luego de la respuesta
                    strcat(txdataPort60, "\r\nConnection: close\r\n\r\n"); //HTTP 1.1 no persistente
                    // Send the packet
                    // send board status message only once at 2 seconds
                    socketTimeout = t_client + 2;
                    //send data back to the source
                    TCP_Send(&port60TCB, (uint8_t*)txdataPort60, strlen(txdataPort60));
                    State_tcp_ip++;
                }
                
            }else if(State_tcp_ip==SM_PROCESS_RESPONSE){               
            
                if (TCP_SendDone(&port60TCB)) //consulta si envio correctamente
                {
                    rx_len =(uint16_t)TCP_GetRxLength(&port60TCB);
                    //TCP_GetReceivedData;
                    // handle the incoming data
           
                    if(rx_len >0){
                        
                        rxdataPort60[(uint8_t)port60TCB.rxBufferPtr ]='\0'; //para que la busqueda por cadenas llegue a fin
                        St_G=GetJSON(rx_len);
                            
                        if (St_G==7){ //EVOLUCION_RX);//hago que evolucione el buffer recepcion
                            while ((59+i)< (uint8_t)port60TCB.rxBufferPtr){
                                rxdataPort60[i]=rxdataPort60[59+i];
                                i++;
                            }
                            // reuse the RX buffer
                            TCP_InsertRxBuffer(&port60TCB, (uint8_t*) rxdataPort60, sizeof(rxdataPort60));
                            port60TCB.rxBufferPtr +=i; //actualizo el puntero
                        }
                    }    
                    if(St_G==4){  //DONE:Rutina terminada de recepcion datos completos de App
                      TCP_Close(&port60TCB);
                      State_tcp_ip--; //reset to SM_SOCKET_OBTAINED
                      St_G=0;
                      return(DONE_st);
                    }else
                        if(St_G==6){ //ERR: Algo ocurrio datos fuera de protocolo
                            TCP_Close(&port60TCB); //hubo error en el paquete recibido
                            State_tcp_ip--; //reset to SM_SOCKET_OBTAINED
                            St_G=0;
                            return(ERR);
                        }                    
                }                            
            }
            break;
        case SOCKET_CLOSING:
            TCP_SocketRemove(&port60TCB);
            break;
        default:
            break;
            // we should not end up here
    }
    return(EXIT);
}

void TCP_Client_Initialize(){
    remoteSocket.addr.s_addr = MAKE_IPV4_ADDRESS(192, 168, 0, 16); //direccion IP destino
    remoteSocket.port = 60; //puerto destino
}