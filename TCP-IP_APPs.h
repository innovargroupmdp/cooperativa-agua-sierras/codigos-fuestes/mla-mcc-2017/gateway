/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

#include <xc.h>
#include <stdint.h>


/* TODO Application specific user parameters used in user.c may go here */

/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

/* TODO User level functions prototypes (i.e. InitApp) go here */

// Defines the server to be accessed for this application
const char ServerName[] = "192.168.1.20";

// Note that if HTTPS is used, the ServerName and URL 
// must change to an SSL enabled server.

//static WORD ServerPort = HTTP_PORT;


// Defines the URL to be requested by this HTTP client
const char RemoteURL[] = "/CSPA/ServZ0";

#define length_buffer_RX 200
#define length_buffer_TX 200

extern char rxdataPort60[length_buffer_RX];
extern char txdataPort60[length_buffer_TX];

void reverse(char*);

void itoa(char*,int );

void utoa(char*, uint16_t );

void PutFecha(uint8_t *, uint8_t);

void PutJSON(void);

uint16_t Get_Obj_Json_TCP_ChartoInt(uint16_t,const char*);

char* Get_Obj_Json_TCP_Array( uint16_t,const char*);

uint8_t GetJSON(uint16_t);

