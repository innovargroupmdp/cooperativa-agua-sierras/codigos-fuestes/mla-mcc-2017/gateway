/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/


#include <xc.h>            /* XC8 General Include File */
#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#include "system.h"        /* System funct/params, like osc/peripheral config */
//#include "TCP-IP_APPs.h" /* User funct/params, such as InitApp */
#include "mcc_generated_files/mcc.h"

#include "App.h"
#include "TCPClient.h"
#include "APLICACION.h"
#include "modbus.h"
#include "APP_MIWI_ModBus.h"


/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/* i.e. uint8_t <variable_name>; */
 APP_t App;           //variables de comando o de intercambio TCPIP
 APP_t App1;          //variables de comando o de intercambio MIWI
 uint8_t  AddMIWI[ADDRESS_LEN];  //intercambio     
//----Buffer y bases------------------------------
 APP_t_Cons_Complet BufferRX;  //buffer Recepcion MIWI

 uint16_t BaseSlot[BaseSlot_LEN][2];  //buffer Recepcion MIWI para cambioos de slot

 Gen_Buffer_t Buffer[Bufferlength]; //beffer recepcion-transmision tcp-ip

//----------------------------------
  char Pass[10];       //pass de GW
  char Pass_AU[10];    //Pass recibida

//----semaforos------------------------------
 
 bool Sem_ModBus_Free=false;  //bandera de Modbus libre
 
 bool Buff_Rx_MB=false;
 /* bandera de trabajo listo sobre una trama TCP Cuando llega una trama se 
  * atiende o coloca en buffer luego se puede volver a ingresar  */
 bool Sem_TCP_DONE=false;     //bandera de trabajo listo sobre una trama TCP
 bool Sem_MB_DONE=false;    //

 bool Sem_Free_App=true;
 bool Sem_Free_App1=true;

 uint16_t Id_GW;      //Id de GW
 uint16_t Id_GW_AU;   //Id Recibida
 uint8_t GW_Secuencia;//Secuencia de transmicion actual 
 DATE_t GW_RTCC;      //variable de intercambio de RTCC 

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{
    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize I/O and Peripherals for application */
    // initialize the device
    SYSTEM_Initialize();
    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();

    TCP_Client_Initialize();
    
    modbus_init();
    //InitApp();
    

    /* TODO <INSERT USER APPLICATION CODE HERE> */

    while(1)
    {   
    /******************************************************************************/
    /* Llama a administrar el socket Ethernet en busca de nuevos paquetes         */
    /******************************************************************************/
        Network_Manage();
    /******************************************************************************/
    /* Llama a administrar la apliaciones                                         */
    /******************************************************************************/        
        Serv_APP();        
    /******************************************************************************/
    /* Llama a administrar la conexion MODBUS en busca de nuevos paquetes arma el
     *  buffer                                                                    */
    /******************************************************************************/ 
        if (Sem_ModBus_Free) ModBus_Manage();
    }

}

