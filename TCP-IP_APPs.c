/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/


#include <xc.h>         /* XC8 General Include File */

#include <stdint.h>         /* For uint8_t definition */
#include <stdlib.h>
#include <stdbool.h>        /* For true/false definition */
#include <stddef.h>
#include <string.h>
#include "TCP-IP_APPs.h"
#include "App.h"
#include "APP_MIWI_ModBus.h"
/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* <Initialize variables in user.h and insert code for user algorithms.> */
enum TCPRxState_t {
        
        AUTEN=0,
        CONTROL,
        TIPO,        
        PROCESS,
        DONErx,
        EXIT,
        ERR,
        EVOLUCION_RX
        
    };
    
typedef enum TCPRxState_t TCPRxState;

//The function reverse used above is implemented two pages earlier:
/* reverse:  reverse string s in place */
 void reverse(char *s)
 {
     int i, j;
     char c;
 
     for (i = 0, j = strlen(s)-1; i<j; i++, j--) 
     {
         c = *(s+i);
         *(s+i) = *(s+j);
         *(s+j) = c;
     }
 }

/* itoa:  convert n to characters in s */
 void itoa(char *s, int n)
 {
    int i, sign;

    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;
    do {       /* generate digits in reverse order */
       *(s+(i++)) = n % 10 + '0';   /* get next digit */        
    } while ((n /= 10) > 0);     /* delete it */

    if (sign < 0)
       *(s+(i++)) = '-';
    *(s+i) = '\0';
    reverse(s);
 }
 
 /* utoa:  convert n to characters in s */
 void utoa( char *s, uint16_t n)
 {
    int i;

    i = 0;
    do {       /* generate digits in reverse order */
       *(s+(i++)) = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */

    *(s+i) = '\0';
    reverse(s);
 }
 
void PutFecha(uint8_t *p, uint8_t f) {

    uint8_t i = 0;
    
    char s[2] ={0,'\0'};
    char str[10]="\0"; 
    switch (f) {
        case 1: 
            for (i = 0; i < 3; i++) {

                s[0]=(char)(*p & 0xf0);
                s[0]=(char)(s[0] >> 4);
                s[0]= (char)(s[0]+48);            //paso de bcd a ascii

                strcat(str, (const char*)s);

                s[0] = (char)(*p & 0x0f);
                s[0]= (char)(s[0]+48);
                strcat(str, (const char*)s);                   
                
                if (i != 2){ 
                    s[0] = ':';
                    strcat(str, (const char*)s);
                }    
                //} else
                //    s[0] = '-';                                
               //strcat(str, (const char*)s);
                p++;
            }
        break;    
        case 2:
            for (i = 0; i < 3; i++) {

                s[0]=(char)(*p & 0xf0);
                s[0]=(char)(s[0] >> 4);
                s[0]= (char)(s[0]+48);            //paso de bcd a ascii

                strcat(str, (const char*)s);

                s[0] = (char)(*p & 0x0f);
                s[0]= (char)(s[0]+48);
                strcat(str, (const char*)s);
                
                if (i != 2){
                    
                    if (i == 0)
                        s[0] = '-'; 
                    else 
                        s[0] = '/';
                    
                    strcat(str, (const char*)s);
                }
                
                p++;
            }
        break;    
    }
    strcat(txdataPort60, (const char*)str);
    return;
}

/*****************************************************************************
  Function:
    void PutJSON(TCP_SOCKET)

  Summary:
    Implements JSON (over TCP).

  Description:
	

  Precondition:
	

  Parameters:
    None

  Returns:
    None
 ***************************************************************************/
void PutJSON(void) {
    /*
        uint16_t  {ID: } // identificador dispositivo asignado en la asociacion
                  {Pass:}
        uint8_t   {Tipo:}; // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
        uint8_t   Secuencia;
        uint8_t   Nro_intentos;
        uint8_t   Noise;
        uint8_t   Signal;
        uint8_t   Bat;
        uint16_t  Slot;
        DATE_t    DateTx;     
        MED_t     Medida[24];
        uint8_t   SlotDif;
     */
    #define Toma_1  (Tomas -1)
    char *s = NULL;
    uint8_t i, j;
    uint8_t *p;
    
    //lonngitud de cadena json adaptar segun tipo!!!!
    strcat(txdataPort60,"64\r\n\r\n{\"Id_GW\":"); //6  ID GateWay
    //utoa((char *) s, (unsigned) Id_GW, 5); //5
    utoa( s ,Id_GW);
    strcat(txdataPort60, (const char*)s);

    strcat(txdataPort60, ",\"Pass\":\""); //9  
    strcat(txdataPort60,Pass); //10
    
    strcat(txdataPort60,",\"Secuencia\":"); //10
    //utoa((char *) s, GW_Secuencia, 3); //3
    utoa( s,(uint16_t)GW_Secuencia );
    strcat(txdataPort60, (const char*)s);

    switch (App.Tipo) // Send the packet: T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: control
    {

        case 2://------------------ Asociacion
      
            strcat(txdataPort60,"\",\"Tipo\":2,\"Id_M\":"); //6  Aqui es la MAC del dispositivo a asociar
            //utoa((char *) s, (unsigned) App.ID, 5); //5
            utoa((char *) s, (unsigned) App.ID); //5
            strcat(txdataPort60, (const char*)s);

            strcat(txdataPort60,"}"); //1
            break;
            
        case 1://--------------------Normal
            
            strcat(txdataPort60,"\",\"Tipo\":2,\"Medidores\":["); //8 

            j = 0; 
            while (BufferRX.Buffer[j].Tipo!=0 && j<BufferRX.point ) { //BufferRX.point apunta a uno vacio o libre
                
                strcat(txdataPort60,"{\"Id_M\":"); //6 
                //utoa((char *) s, (unsigned) BufferRX.Buffer[j].ID, 5); //5
                utoa((char *) s, (unsigned) BufferRX.Buffer[j].ID); //5
                strcat(txdataPort60, (const char*)s);

                strcat(txdataPort60,",\"Secuencia_M\":"); //10
                //utoa((char *) s, BufferRX.Buffer[j].Secuencia, 3); //2
                utoa((char *) s, BufferRX.Buffer[j].Secuencia); //2
                strcat(txdataPort60, (const char*)s);

                strcat(txdataPort60,",\"Nro_intentos\":"); //10
                //utoa((char *) s, BufferRX.Buffer[j].Nro_intentos, 2); //2
                utoa((char *) s, BufferRX.Buffer[j].Nro_intentos); //2
                strcat(txdataPort60, (const char*)s);

                strcat(txdataPort60,",\"Noise\":"); //10
                //utoa((char *) s, BufferRX.Buffer[j].Noise, 2); //2
                utoa((char *) s, BufferRX.Buffer[j].Noise); //2
                strcat(txdataPort60, (const char*)s);

                strcat(txdataPort60,",\"Signal\":"); //10
                //utoa((char *) s, BufferRX.Buffer[j].Signal, 2); //2
                utoa((char *) s, BufferRX.Buffer[j].Signal); //2
                strcat(txdataPort60, (const char*)s);

                strcat(txdataPort60,",\"Bat\":"); //10
                //utoa((char *) s, BufferRX.Buffer[j].Bat, 2); //2
                utoa((char *) s, BufferRX.Buffer[j].Bat); //2
                strcat(txdataPort60, (const char*)s);

                strcat(txdataPort60,",\"Slot\":"); //10
                //utoa((char *) s, BufferRX.Buffer[j].Slot, 2); //2
                utoa((char *) s, BufferRX.Buffer[j].Slot); //2
                strcat(txdataPort60, (const char*)s);
                
                strcat(txdataPort60,",\"DateTx\":\""); //10
                p = &BufferRX.Buffer[j].DateTx.Seg;
                PutFecha(p,1);  // solo seg min hora
                
                strcat(txdataPort60,"\",\"MedidaRE\":{\"Mesure\":"); //10 MedidaRE
                //utoa((char *) s, BufferRX.Buffer[j].MedidaRE.Mesure, 5); //2  
                utoa((char *) s, BufferRX.Buffer[j].MedidaRE.Mesure); //2  
                strcat(txdataPort60, (const char*)s);
                
                strcat(txdataPort60,",\"FechaMedida\":\""); //10                
                p = &(BufferRX.Buffer[j].MedidaRE.DateMesu.Hou);
                PutFecha(p,2);       // solo hora dia mes
                    
                strcat(txdataPort60,"\"},\"Medidas\":["); //10

                for (i = 0; i < Tomas; i++) {
                    
                    strcat(txdataPort60,"{\"Medida\":"); //10
                    //utoa((char *) s, BufferRX.Buffer[j].MedidaDif[i],3); //2
                    utoa((char *) s, BufferRX.Buffer[j].MedidaDif[i]); //2
                    strcat(txdataPort60, (const char*)s);

                    //strcat(txdataPort60,",\"FechaMedida\":\""); //10
                    //p = &(BufferRX[j].Medida[i].DateMesu.Hou);
                    //PutFecha(p,2);       // solo hora dia mes

                    if (i != Toma_1) strcat(txdataPort60,"\"},");
                    else strcat(txdataPort60,"\"}]}");
                    p++;
                }
                
                BufferRX.send_point=j;
                
                
                if ((BufferRX.Buffer[j+1].Tipo) && ((j+1)<BufferRX.point)) 
                    strcat(txdataPort60, ",");
                else 
                    strcat(txdataPort60, "]}");
                
                j++;
            }
            break;
            
        case 3://----------------------TIME

            strcat(txdataPort60,"\",\"Tipo\":3}"); //8
             
            break;

        case 4:// ----------------(ASOCIACION GATEWAY)

            strcat(txdataPort60,"\",\"Tipo\":4}"); //8 

            break;
            
        case 5:// ----------------(CONTROL)

            strcat(txdataPort60,"\",\"Tipo\":5}"); //8 

            break;    
    }
    return;
}
/*****************************************************************************
  Function:
    Get_Obj_Json_TCP_ChartoInt(TCP_SOCKET TCPS,str,len)

  Summary:
    Extrae elementos Json a int.
 * 
 * OTROS:
 * Get_Obj_Json_TCP_Array(); entrega Array
   Get_Obj_Json_TCP_Int();  
   Get_Obj_Json_TCP_Char(TCPSOC,(ROM BYTE*) "\"Tipo\"",len);
 * Get_Obj_Json_TCP_ChartoInt(TCP_SOCKET TCPS,str,len);
*****************************************************************************/
uint16_t Get_Obj_Json_TCP_ChartoInt(uint16_t w , const char* str){    
    
    char D[21];                
    uint16_t l;
    uint8_t i;
    char *a;
    
    l=strlen((char*)str);
    
    a = strstr(rxdataPort60,"}]}"); // consulto fin de Json
    
    if (a==NULL) { 
        
        a = strstr(rxdataPort60, str);

        if (NULL != a)  // ubicacion de la cadena y que entre todo el objeto Json   
        {
            if((a+40-rxdataPort60)<=w){
                
                a=a+l+2; //cero + (l-1)+(:_)
                //length("Tipo":")+(datos)   {"variable":"cadena",}
                i=0;
                while((*a!=',') && (i<21))
                {
                    *(D+i)=*(a++);                        
                    i++;
                }
                D[i] = '\0';
                return( (uint16_t)atoi((char *)D));
            } 
        }    
    }else{        
        a = strstr(rxdataPort60, str);

        if (NULL != a)  //entra todo el objeto Json   
        {                       
                a=a+l+2; //cero + (l-1)+(:"_)
               
                //estoy ubicado sobre el primer caracter del dato
                //length("Tipo":")+(datos)   {"variable":"cadena"}
                i=0;
                while((*a!=',') && (i<21))
                {
                    *(D+i)=*(a++);                        
                    i++;
                }
                D[i] = '\0';
                
                return((uint16_t)atoi((char *)D));              
        }   
    }    
    return(0xFFFF);//hago que evolucione el buffer recepcion
}

char* Get_Obj_Json_TCP_Array( uint16_t w,const char* str){    
    
    static char D[21];
    uint16_t l;
    uint8_t i;
    char *a;    
       
    l=strlen((char*)str);
    
    a = strstr(rxdataPort60,"\"}]}"); // consulto fin de Json   

    if (a==NULL) { 
        
        a = strstr(rxdataPort60, str);

        if (NULL != a)  // ubicacion de la cadena y que entre todo el objeto Json   
        {
            if((a+40-rxdataPort60)<=w){
                
                a=a+l+3; //cero + (l-1)+(:"_)
               
                //estoy ubicado sobre el primer caracter del dato
                //length("Tipo":")+(datos)   {"variable":"cadena"}
                i=0;
                while((*a!='\"') && (i<21))
                {
                    *(D+i)=*(a++);                        
                    i++;
                }
                D[i] = '\0';
                
                return((char *)D);              
            }
        }
    }else{
        a = strstr(rxdataPort60, str);

        if (NULL != a)  //entra todo el objeto Json   
        {
                       
                a=a+l+3; //cero + (l-1)+(:"_)
               
                //estoy ubicado sobre el primer caracter del dato
                //length("Tipo":")+(datos)   {"variable":"cadena"}
                i=0;
                while((*a!='\"') && (i<21))
                {
                    *(D+i)=*(a++);                        
                    i++;
                }
                D[i] = '\0';
                
                return((char *)D);              
            
        }
        
    }   
    
    return(NULL);//hago que evolucione el buffer recepcion
}

/*****************************************************************************
  Function:
    void GetJSON(TCP_SOCKET TCPSOC)

  Summary:
    Retrieve data from JSON (over TCP).

  Description:
  -Tramas:     
       -General
            
            uint16_t  {IdGW: } // identificador dispositivo asignado en la asociacion
                      {Pass:}
            uint8_t    Secuencia;
            uint8_t   {Tipo:}; // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
       -T=1: Normal          
  
       -T=2: Asociacion
            uint16_t  {Id } 
            uint16_t  Slot;
            Note: el gw agrega el set point de date
       -T=3: Time 
             DATE_t   DateTx;     
 
       -T=4: Control 
            uint16_t  Slot;      

  Precondition:
	
  Parameters:
    None

  Returns:
    None
 ***************************************************************************/
uint8_t GetJSON(uint16_t w) {
     
    static TCPRxState RxState=AUTEN;   
    uint16_t a=0;
    uint8_t i;
    
    char *p;
    char D1[2];
    uint8_t *p1;
    uint8_t f;
    
    static bool st=0;

    switch (RxState) {

        case AUTEN:

            if (w >100) {
                                            
                if (!st){
                   a=Get_Obj_Json_TCP_ChartoInt(w,"\"IdGW\"");
                   if (0xffff!=a) {
                       Id_GW_AU=a;
                       st++;
                   } // restan al menos 66 elementos que retirar de tcp
                   return(EVOLUCION_RX);//hago que evolucione el buffer recepcion al retornar a TCPCLiente
                }
            }    
            if(st){                 
                p=Get_Obj_Json_TCP_Array(w,"\"Pass\"");                    
                if (p!=NULL){
                    RxState=ERR;
                    st--;
                    if ((strcmp((char*)Pass,p) == 0) && (Id_GW==Id_GW_AU))
                        RxState = CONTROL; // trama valida analizo secuencias
                }
            }      
            
        break;

        case CONTROL:
            if (w >20) {
    
                a=Get_Obj_Json_TCP_ChartoInt(w,"\"Secuencia\"");
                
                if (a!=0xffff){
                    RxState=ERR;
                    
                    if (GW_Secuencia == (uint8_t) a)
                            RxState = TIPO; // trama valida, analizo secuencias
                } 
            }    

        break;
        
        case TIPO:          
            if (w > 20) {
                
                a=Get_Obj_Json_TCP_ChartoInt(w,"\"Tipo\"");
                if (0xffff!=a){
                    RxState=ERR;
                    if ((1== (uint8_t)a) ||(2== (uint8_t)a) ||(3== (uint8_t)a) ||(4== (uint8_t)a) ||(5== (uint8_t)a))
                    {
                        App.Tipo=(uint8_t)a;
                        RxState=PROCESS ;
                    }
                }                 
            }                

        break;

        case PROCESS:
               
            p = strstr(rxdataPort60,"\"}]}"); // fin de objetos Json
            if (p!=NULL){ 
                
                switch (App.Tipo) {

                    case 2:// -----------------//ASOCIA

                        RxState=ERR;
                        a=Get_Obj_Json_TCP_ChartoInt(w,"\"Id\"");
                        if (a!=0xffff ){
                            App.ID=a;
                            
                            a=Get_Obj_Json_TCP_ChartoInt(w,"\"Slot\"");
                            if (a!=0xffff ){
                                App.Slot=a;
                                RxState=DONErx;
                            }
                        }
         
                    break;
                    
                    case 4:// -----------------//ASOCIA GW

                        RxState=ERR;
                        a=Get_Obj_Json_TCP_ChartoInt(w,"\"Id\"");
                        if (a!=0xffff ){
                            App.ID=a;
                            p=Get_Obj_Json_TCP_Array(w,"\"PassGW\"");                    
                            if (p!=NULL){
                                strcpy(Pass_AU,p);       
                                RxState = DONErx;
                            }
                        }
         
                    break;

                    case 3: //-----------------//CLOCK

                        RxState=ERR;
                        p=Get_Obj_Json_TCP_Array(w,"\"CLock\"");
                        
                        if (p!=NULL ){
                            App.Slot=a;
                            RxState=DONErx;

                            /*
                             * D=hh:mm:ss-DD/MM/AA
                                uint8_t Seg;
                                uint8_t Min;
                                uint8_t Hou;
                                uint8_t Day;
                                uint8_t Mon;
                                uint8_t Yea;
                            }DATE_t;*/

                            p1=&App.DateTx.Seg; //GW_RTCC.Seg; 
                            D1[1]='\0';

                            for(i=0;i<17;i+=3){ 
                                D1[0]=*(p+i);

                                f= (uint8_t)atoi(D1);
                                f=(uint8_t)(f<<4); 
                                D1[0]=*(p+1+i);

                                *p1=(uint8_t) (f|(atoi(D1)&0x0F)); 
                                p1++;
                            }
                        }
                        
                    break;

                    case 5: //-----------------//CONTROL

                        RxState=ERR;
                        a=Get_Obj_Json_TCP_ChartoInt(w,"\"Id\"");
                        if (a!=0xffff ){
                            App.ID=a;
                            
                            a=Get_Obj_Json_TCP_ChartoInt(w,"\"Slot\"");
                            if (a!=0xffff ){
                                App.Slot=a;
                                RxState=DONErx;
                            }
                        }
                        
                    break;

                    case 1 : //-----------------//NORMAL
                        App.Tipo=0;
                        RxState=DONErx;
                        //Algo hay que hacer, semaforo que librere recursos
                    break;

                    default : //-----------------//caso de error
                        App.Tipo=0;
                        RxState=ERR;

                    break;
                }
            }
            
        break;
         
        case DONErx:
               RxState=AUTEN; //vuelve a la analizar desde cero trama valida            
               return(AUTEN);        
        break;
        
        case EXIT:
               RxState=AUTEN;            
               return(AUTEN);        
        break;    
        
        case ERR:
               RxState =AUTEN; //vuelve a la analizar desde cero trama invalida
               return(AUTEN);
        break;                  
    }
    return(RxState);
}


